﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：ActionType
* 创建日期：2021-12-22 23:51:19
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 事件类型
    /// </summary>
	public enum ActionType  
	{
		/// <summary>
		/// 无
		/// </summary>
		None = 0,
		/// <summary>
		/// 点击
		/// </summary>
		Click,
		/// <summary>
		/// 按下
		/// </summary>
		Down,
		/// <summary>
		/// 抬起
		/// </summary>
		Up,
		/// <summary>
		/// 进入
		/// </summary>
		Enter,
		/// <summary>
		/// 离开
		/// </summary>
		Exit,
		/// <summary>
		/// 按住
		/// </summary>
		Press,
		/// <summary>
		/// 拖拽
		/// </summary>
		Drag,
		/// <summary>
		/// 按键
		/// </summary>
		Button,

	}
}

