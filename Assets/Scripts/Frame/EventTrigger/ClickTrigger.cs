﻿/************************************************************
  Copyright (C), 2007-2017,BJ Rainier Tech. Co., Ltd.
  FileName: ClickTrigger.cs
  Author:张辰       Version :1.0          Date: 2017-12-7
  Description:点击事件监听
************************************************************/
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using System.Collections;
using System;

namespace Com.Rainier.ZC_Frame
{
    public class ClickTrigger : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private MouseClickType.MouseInput mouseInput;

        public MouseClickType.MouseInput MouseInput {
            get { return mouseInput; }
            set { mouseInput = value; }
        }

        protected ClickTrigger() { }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (MouseClickType.current.MouseInputJudgment(mouseInput, eventData))
            {
                EventSystem.current.SetSelectedGameObject(this.gameObject);
                Press();
                Press(transform.name);
            }
        }

        [FormerlySerializedAs("onMouseClick")]
        [SerializeField]
        private ClickEvent onMouseClick = new ClickEvent();

        [FormerlySerializedAs("onMouseClickCall")]
        [SerializeField]
        private ClickCallEvent onMouseClickCall = new ClickCallEvent();

        public ClickEvent OnMouseClick
        {
            get { return onMouseClick; }
            set { onMouseClick = value; }
        }

        public ClickCallEvent OnMouseClickCall
        {
            get { return onMouseClickCall; }
            set { onMouseClickCall = value; }
        }

        private void Press()
        {
            onMouseClick.Invoke();
        }

        private void Press(string objName)
        {
            onMouseClickCall.Invoke(objName);
        }

        [Serializable]
        public class ClickEvent : UnityEvent { }

        [Serializable]
        public class ClickCallEvent : UnityEvent<string> { }
    }
}

