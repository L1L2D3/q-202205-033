﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： TextMeshProViewModel
* 创建日期：2021-12-20 15:32:49
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using TMPro;

namespace Com.Rainier.Buskit3D
{
    /// <summary>
    /// 
    /// </summary>
	public class TextMeshProViewModel : ViewModel 
	{
        /// <summary>
        /// 获取或设置TextMeshPro组件的值
        /// </summary>
        [FireInitEvent]
        public string Value
        {
            get
            {
                return textValue;
            }
            set
            {
                string oldValue = textValue;
                string newValue = value;
                if (oldValue.Equals(newValue))
                {
                    return;
                }
                textValue = newValue;
                this.FireEvent("Value", oldValue, newValue);
                RefreshComponent();
            }
        }
        string textValue = string.Empty;

        /// <summary>
        /// 初始化View组件
        /// </summary>
        /// <param name="uiBehaviour"></param>
        public override void InitView()
        {
            if (this.UIComponent == null)
            {
                this.UIComponent = GetComponent<TextMeshProUGUI>();
            }

            if (!(this.UIComponent is TextMeshProUGUI))
            {
                throw new Buskit3DException("TextViewModel#InitView[Incorrect component type]");
            }

            TextMeshProUGUI text = (TextMeshProUGUI)this.UIComponent;
            Value = text.text;
        }

        /// <summary>
        /// 更新组件界面
        /// </summary>
        protected override void RefreshComponent()
        {
            TextMeshProUGUI text = (TextMeshProUGUI)this.UIComponent;
            if (!text.text.Equals(Value))
            {
                text.text = Value;
            }
        }

    }
}

