﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： NamePair
* 创建日期：2021-12-22 20:50:17
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：名称对
******************************************************************************/

using UnityEngine;
using System.Collections;

namespace Com.Rainier.ZC_Frame
{
	/// <summary>
    /// 名称对
    /// </summary>
    [SerializeField]
    public struct NamePair
    {
        private static NamePair empty = new NamePair(string.Empty, string.Empty);

        private string rootName;

        private string objName;

        /// <summary>
        /// 根物体名字
        /// </summary>
        public string RootName
        {
            get { return rootName; }
            set { rootName = value; }
        }

        /// <summary>
        /// 对象名字
        /// </summary>
        public string ObjName
        {
            get { return objName; }
            set { objName = value; }
        }

        /// <summary>
        /// 空名称对
        /// </summary>
        public static NamePair Empty
        {
            get
            {
                return empty;
            }
        }

        public NamePair(string rootName, string objName)
        {
            this.rootName = rootName;
            this.objName = objName;
        }

        public static bool operator ==(NamePair a, NamePair b)
        {
            if (a.rootName == b.rootName && a.ObjName == b.ObjName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator !=(NamePair a, NamePair b)
        {
            if (a.rootName == b.rootName && a.ObjName == b.ObjName)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is NamePair tmpobj && this.rootName == tmpobj.rootName && this.ObjName == tmpobj.ObjName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

