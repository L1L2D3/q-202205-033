﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：CarControllerLogic
* 创建日期：2021-11-08 17:49:45
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：小车控制脚本
******************************************************************************/
using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Aop;
using Com.Rainier.ZC_Frame;
namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class CarControllerLogic : ObjLogicBase 
	{
        /// <summary>
        /// 左前轮
        /// </summary>
        public WheelCollider frontWheelLeft;

        /// <summary>
        /// 右前轮
        /// </summary>
        public WheelCollider frontWheelRight;

        /// <summary>
        /// 左后轮
        /// </summary>
        public WheelCollider rearWheelLeft;

        /// <summary>
        /// 右后轮
        /// </summary>
        public WheelCollider rearWheelRight;

        /// <summary>
        /// 最大马力
        /// </summary>
        public int maxMotorTorque = 100;

        private float motorTorque = 0;

        /// <summary>
        /// 最大转向角
        /// </summary>
        public int maxSteerAngle = 45;

        private float steerAngle = 0;

        /// <summary>
        /// 最大制动
        /// </summary>
        public int maxBrakeTorque = 500;

        private float brakeTorque = 0;

        /// <summary>
        /// 睡眠
        /// </summary>
        private bool isSleep = true;

        /// <summary>
        /// 移动速度
        /// </summary>
        private float speed = 0f;

        /// <summary>
        /// 车辆朝向
        /// </summary>
        private int carDirection = 1;

        /// <summary>
        /// 移动速度
        /// </summary>
        public float Speed {
            get { return speed; }
        }

        /// <summary>
        /// 车辆朝向
        /// </summary>
        public int CarDirection {
            get { return carDirection; }
        }

        private Rigidbody carRigidbody;

        private void Start()
        {
            AddEntity<CarControllerEntity>();
            carRigidbody = GetComponent<Rigidbody>();
        }

        private void OnDestroy()
        {
            RemoveEntity<CarControllerEntity>();
        }

        private void FixedUpdate()
        {
            speed = carRigidbody.velocity.magnitude * 3.6f;
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                motorTorque = maxMotorTorque * Input.GetAxis("Vertical");
                steerAngle = maxSteerAngle * Input.GetAxis("Horizontal");
                if (Input.GetAxis("Vertical")==1)
                {
                    carDirection = 1;
                    PeopleWalk();
                }
                else if (Input.GetAxis("Vertical") == -1)
                {
                    carDirection = 0;
                    PeopleBackWalk();
                }
                isSleep = false;
            }
            else
            {
                PeopleNomal();
                motorTorque = 0;
                steerAngle = 0;
            }
            if (isSleep)
            {
                return;
            }
            if (motorTorque != 0)
            {
                SetMotorTorque(motorTorque);
                SetSteerAngle(steerAngle);
                SetBrakeTorque(0);
            }
            else
            {
                SetBrakeTorque(maxBrakeTorque);
                if (rearWheelLeft.rpm == 0)
                {
                    SetMotorTorque(0);
                    SetSteerAngle(0);
                    isSleep = true;
                }
            }


        }

        private void Update()
        {
            
            if (speed == 0)
            {
                
            }
            else
            {
                //Debug.Log(rearWheelLeft.rpm);
                //if (rearWheelLeft.rpm>=0)
                //{
                //    PeopleWalk();
                //}
                //else
                //{
                //    PeopleBackWalk();
                //}
                
            }
        }

        /// <summary>
        /// 人物静止
        /// </summary>
        protected virtual void PeopleNomal() { Debug.Log("站立状态"); }

        /// <summary>
        /// 人物前进
        /// </summary>
        protected virtual void PeopleWalk() { Debug.Log("前进状态"); }

        /// <summary>
        /// 人物倒退
        /// </summary>
        protected virtual void PeopleBackWalk() { Debug.Log("后退状态"); }

        /// <summary>
        /// 设置马力
        /// </summary>
        /// <param name="value"></param>
        private void SetMotorTorque(float value) {
            GetEntity<CarControllerEntity>().MotorTorque = value;
        }

        /// <summary>
        /// 设置制动力
        /// </summary>
        /// <param name="value"></param>
        private void SetBrakeTorque(float value)
        {
            GetEntity<CarControllerEntity>().BrakeTorque = value;
        }

        /// <summary>
        /// 设置转向
        /// </summary>
        /// <param name="value"></param>
        private void SetSteerAngle(float value)
        {
            GetEntity<CarControllerEntity>().SteerAngle = value;
        }

        public override void ProcessLogic(IEvent evt)
        {
            //马力
            if (evt.EventName.Equals("MotorTorque"))
            {
                PropertyEvent propertyEvent = evt as PropertyEvent;
                float newValue = (float)propertyEvent.NewValue;
                CarMotor(newValue);
            }
            //制动
            else if (evt.EventName.Equals("BrakeTorque"))
            {
                PropertyEvent propertyEvent = evt as PropertyEvent;
                float newValue = (float)propertyEvent.NewValue;
                CarBrake(newValue);
            }
            //转向
            else if (evt.EventName.Equals("SteerAngle"))
            {
                PropertyEvent propertyEvent = evt as PropertyEvent;
                float newValue = (float)propertyEvent.NewValue;
                CarSteer(newValue);
            }
        }

        private void CarMotor(float motor) {
            frontWheelLeft.motorTorque = motor;
            frontWheelRight.motorTorque = motor;
            rearWheelLeft.motorTorque = motor;
            rearWheelRight.motorTorque = motor;
        }

        private void CarBrake(float brake) {
            frontWheelLeft.brakeTorque = brake;
            frontWheelRight.brakeTorque = brake;
            rearWheelLeft.brakeTorque = brake;
            rearWheelRight.brakeTorque = brake;
        }

        private void CarSteer(float steer) {
            frontWheelLeft.steerAngle = steer;
            frontWheelRight.steerAngle = steer;
        }
    }
}
