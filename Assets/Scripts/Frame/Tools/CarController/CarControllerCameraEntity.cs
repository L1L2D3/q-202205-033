﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： CarControllerCameraEntity
* 创建日期：2021-11-09 15:36:46
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using Com.Rainier.Buskit3D;

namespace Com.Rainier.ZC_Frame
{
	/// <summary>
    ///
    /// </summary>
	public class CarControllerCameraEntity : Entity
	{
        /// <summary>
        /// 相机旋转
        /// </summary>
        /// <param name="rotateX"></param>
        /// <param name="rotateY"></param>
        public void CameraRotate(float rotateX, float rotateY)
        {
            FireEvent("CameraRotate", new object[] { rotateX, rotateY });
        }

        /// <summary>
        /// 回转视角
        /// </summary>
        public void ReturnCamera()
        {
            FireEvent("ReturnCamera", new object[] {  });
        }
    }
}

