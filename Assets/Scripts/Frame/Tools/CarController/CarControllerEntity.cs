﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： CarControllerEntity
* 创建日期：2021-11-08 17:50:04
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using Com.Rainier.Buskit3D;

namespace Com.Rainier.ZC_Frame
{
	/// <summary>
    ///
    /// </summary>
	public class CarControllerEntity : Entity
	{
        /// <summary>
        /// 马力
        /// </summary>
        private float motorTorque = 0f;

        /// <summary>
        /// 马力
        /// </summary>
        public float MotorTorque
        {
            get { return motorTorque; }
            set
            {
                var oldvalue = motorTorque;
                motorTorque = value;
                FireEvent("MotorTorque", oldvalue, value);
            }
        }

        /// <summary>
        /// 制动
        /// </summary>
        private float brakeTorque = 0f;

        /// <summary>
        /// 制动
        /// </summary>
        public float BrakeTorque {
            get { return brakeTorque; }
            set
            {
                var oldvalue = brakeTorque;
                brakeTorque = value;
                FireEvent("BrakeTorque", oldvalue, value);
            }
        }

        /// <summary>
        /// 转向
        /// </summary>
        private float steerAngle = 0f;

        /// <summary>
        /// 转向
        /// </summary>
        public float SteerAngle
        {
            get { return steerAngle; }
            set
            {
                var oldvalue = steerAngle;
                steerAngle = value;
                FireEvent("SteerAngle", oldvalue, value);
            }
        }
    }
}

