﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：CarControllerCameraLogic
* 创建日期：2021-11-09 09:48:04
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：车辆控制相机跟随脚本
******************************************************************************/
using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Aop;
using Com.Rainier.ZC_Frame;
using UnityEngine.EventSystems;

namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class CarControllerCameraLogic : ObjLogicBase 
	{
		/// <summary>
		/// 车朝向
		/// </summary>
		private int carDirection = 1;
		/// <summary>
		/// 目标车
		/// </summary>
		public Transform car;
		/// <summary>
		/// 是否使用相机控制
		/// </summary>
		public bool isUseCameraContorl = false;
		/// <summary>
		/// 相机与物体间距
		/// </summary>
		public float distance = 2.5f;
		/// <summary>
		/// 人物高度
		/// </summary>
		public float playerHeight = 1f;
		/// <summary>
		/// 相机跟随高度阻尼
		/// </summary>
		public float cameraHeightDamping = 10f;
		/// <summary>
		/// 相机跟随宽度阻尼
		/// </summary>
		public float cameraRotationDamping = 5f;
		/// <summary>
		/// 相机水平倾斜角
		/// </summary>
		public float TPSYawAngle = 0f;
		/// <summary>
		/// 相机竖直倾斜角
		/// </summary>
		public float TPSPitchAngle = 15f;
		/// <summary>
		/// 移动视角最低值
		/// </summary>
		public float yMinLimit = -30f;
		/// <summary>
		/// 移动视角最高值
		/// </summary>
		public float yMaxLimit = 50f;
		/// <summary>
		/// 视角自动归正速度
		/// </summary>
		public float autoResetSpeed = 5f;

		private Rigidbody carRigidbody;
		private Camera carCamera;
		private GameObject pivot;
		private float playerSpeed = 0f;
		private Vector3 playerVelocity = Vector3.zero;
		/// <summary>
		/// 与刚体局部速度相关的最大倾斜角。
		/// </summary>
		private float TPSTiltMaximum = 15f;
		/// <summary>
		/// 倾角倍增器
		/// </summary>
		private float TPSTiltMultiplier = 2f;
		/// <summary>
		/// 当前倾角
		/// </summary>
		private float TPSTiltAngle = 0f; 
										
		private Quaternion currentRotation = Quaternion.identity;
		private Quaternion wantedRotation = Quaternion.identity;
		private float currentHeight = 0f;
		private float wantedHeight = 0f;

		private Vector3 collisionPos = Vector3.zero;   
		private Quaternion collisionRot = Quaternion.identity;  

		private int direction = 1;
		private int lastDirection = 1;

		private float orbitX = 0f;
		private float orbitY = 0f;

		private Vector3 targetPosition;

		private float orbitResetTimer = 0f;

		private float orbitXSpeed = 7.5f;
		private float orbitYSpeed = 5f;

		private float tempDistance;
		private float tempDistance1;
		private float tempDistance2;
		private float tempDistance3;
		private float tempDistance4;
		private float tempDistance5;
		private float tempPos = 0.01f;

		private void OnEnable()
        {
			ResetCamera();
		}

        private void OnDestroy()
        {
			RemoveEntity<CarControllerCameraEntity>();
        }

        private void Start()
        {
			AddEntity<CarControllerCameraEntity>();
			carRigidbody = car.GetComponent<Rigidbody>();
			carCamera = transform.GetChild(0).GetChild(0).GetComponent<Camera>();
			pivot = transform.GetChild(0).gameObject;
		}

        private void Update()
        {
			playerSpeed = Mathf.Lerp(playerSpeed, car.GetComponent<CarControllerLogic>().Speed, Time.deltaTime * 5f);
			carDirection = car.GetComponent<CarControllerLogic>().CarDirection;
		}

        private void LateUpdate()
        {
			CameraMove();
			if (isUseCameraContorl) {
				if (Input.GetMouseButton(1))
				{
					Rotate(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
				}
				AutoResetCamera();
			}
		}

		/// <summary>
		/// 相机移动
		/// </summary>
        private void CameraMove()
		{
			if (lastDirection != carDirection)
			{
				direction = carDirection;
				orbitX = 0f;
				orbitY = 0f;
			}
			lastDirection = carDirection;

			wantedRotation = car.transform.rotation * Quaternion.AngleAxis((direction == 1 ? 0 : 180) + (isUseCameraContorl ? orbitX : 0), Vector3.up);
			wantedRotation = wantedRotation * Quaternion.AngleAxis((isUseCameraContorl ? orbitY : 0), Vector3.right);

			if (Input.GetKey(KeyCode.B)) {
				ReturnCamera();
				
			}

			wantedHeight = car.transform.position.y + playerHeight;

			currentHeight = Mathf.Lerp(currentHeight, wantedHeight, cameraHeightDamping * Time.fixedDeltaTime);

			if (Time.time > 1)
			{
				currentRotation = Quaternion.Lerp(currentRotation, wantedRotation, cameraRotationDamping * Time.deltaTime);
			}
			else 
			{
				currentRotation = wantedRotation;
			}
			
			TPSTiltAngle = Mathf.Lerp(0f, TPSTiltMaximum * Mathf.Clamp(-playerVelocity.x, -1f, 1f), Mathf.Abs(playerVelocity.x) / 50f);
			TPSTiltAngle *= TPSTiltMultiplier;

			targetPosition = car.transform.position;
			targetPosition -= (currentRotation) * Vector3.forward * (distance * Mathf.Lerp(1f, .75f, (carRigidbody.velocity.magnitude * 3.6f) / 100f));
			targetPosition += Vector3.up * (playerHeight * Mathf.Lerp(1f, .75f, (carRigidbody.velocity.magnitude * 3.6f) / 100f));

			transform.position = targetPosition;

			carCamera.transform.localPosition = Vector3.Lerp(carCamera.transform.localPosition, new Vector3(TPSTiltAngle / 10f, 0f, 0f), Time.deltaTime * 3f);

			transform.LookAt(car.transform);
			transform.eulerAngles = new Vector3(currentRotation.eulerAngles.x + (TPSPitchAngle * Mathf.Lerp(1f, .75f, (carRigidbody.velocity.magnitude * 3.6f) / 100f)), transform.eulerAngles.y, -Mathf.Clamp(TPSTiltAngle, -TPSTiltMaximum, TPSTiltMaximum) + TPSYawAngle);

			collisionPos = Vector3.Lerp(new Vector3(collisionPos.x, collisionPos.y, collisionPos.z), Vector3.zero, Time.unscaledDeltaTime * 5f);

			if (Time.deltaTime != 0) {
				collisionRot = Quaternion.Lerp(collisionRot, Quaternion.identity, Time.deltaTime * 5f);
			}
			
			pivot.transform.localPosition = Vector3.Lerp(pivot.transform.localPosition, collisionPos, Time.deltaTime * 10f);
			pivot.transform.localRotation = Quaternion.Lerp(pivot.transform.localRotation, collisionRot, Time.deltaTime * 10f);

			OccludeRay(car.transform.position);
		}

		/// <summary>
		/// 射线检测相机遮挡
		/// </summary>
		/// <param name="targetFollow"></param>
		private void OccludeRay(Vector3 targetFollow)
		{
			RaycastHit hit1 = new RaycastHit();
			RaycastHit hit2 = new RaycastHit();
			RaycastHit hit3 = new RaycastHit();
			RaycastHit hit4 = new RaycastHit();
			RaycastHit hit5 = new RaycastHit();
			Vector3 trueTargetPosition = targetFollow + Vector3.up * playerHeight;
			if (Physics.Linecast(trueTargetPosition, transform.position, out hit1) ||
				Physics.Linecast(trueTargetPosition + (transform.right * tempPos), transform.position + (transform.right * tempPos), out hit2) ||
				Physics.Linecast(trueTargetPosition - (transform.right * tempPos), transform.position - (transform.right * tempPos), out hit3) ||
				Physics.Linecast(trueTargetPosition + (transform.up * tempPos), transform.position + (transform.up * tempPos), out hit4) ||
				Physics.Linecast(trueTargetPosition - (transform.up * tempPos), transform.position - (transform.up * tempPos), out hit5))
			{
				RaycastHit tmpHit = HitCheck(trueTargetPosition,hit1, hit2, hit3, hit4, hit5);
                if (tmpHit.point!=Vector3.zero)
                {
					Vector3 occludedPosition = tmpHit.point + (tmpHit.normal * tempPos);
					transform.position = occludedPosition;
				}
			}
		}

		/// <summary>
		/// 射线判断
		/// </summary>
		/// <param name="trueTargetPosition"></param>
		/// <param name="hit1"></param>
		/// <param name="hit2"></param>
		/// <param name="hit3"></param>
		/// <param name="hit4"></param>
		/// <param name="hit5"></param>
		/// <returns></returns>
		private RaycastHit HitCheck(Vector3 trueTargetPosition, RaycastHit hit1, RaycastHit hit2, RaycastHit hit3, RaycastHit hit4, RaycastHit hit5) {
			tempDistance1 = distance;
			tempDistance2 = distance;
			tempDistance3 = distance;
			tempDistance4 = distance;
			tempDistance5 = distance;
			if (Physics.Linecast(trueTargetPosition, transform.position, out hit1))
			{
				tempDistance1 = Vector3.Distance(trueTargetPosition, hit1.point);
			}
			if (Physics.Linecast(trueTargetPosition + (transform.right * tempPos), transform.position + (transform.right * tempPos), out hit2))
			{
				tempDistance2 = Vector3.Distance(trueTargetPosition + (transform.right * tempPos), hit2.point);
			}
			if (Physics.Linecast(trueTargetPosition - (transform.right * tempPos), transform.position - (transform.right * tempPos), out hit3))
			{
				tempDistance3 = Vector3.Distance(trueTargetPosition - (transform.right * tempPos), hit3.point);
			}
			if (Physics.Linecast(trueTargetPosition + (transform.up * tempPos), transform.position + (transform.up * tempPos), out hit4))
			{
				tempDistance4 = Vector3.Distance(trueTargetPosition + (transform.up * tempPos), hit4.point);
			}
			if (Physics.Linecast(trueTargetPosition - (transform.up * tempPos), transform.position - (transform.up * tempPos), out hit5))
			{
				tempDistance5 = Vector3.Distance(trueTargetPosition - (transform.up * tempPos), hit5.point);
			}
			tempDistance = Mathf.Min(tempDistance1, tempDistance2, tempDistance3, tempDistance4, tempDistance5, distance);
			if (tempDistance == tempDistance1)
			{
				return hit1;
			}
			else if (tempDistance == tempDistance2)
			{
				return hit2;
			}
			else if (tempDistance == tempDistance3)
			{
				return hit3;
			}
			else if (tempDistance == tempDistance4)
			{
				return hit4;
			}
			else if (tempDistance == tempDistance5)
			{
				return hit5;
			}
			else {
				return hit1;
			}
		}

		/// <summary>
		/// 速度超过定值相机视角自动归正
		/// </summary>
		private void AutoResetCamera()
		{
			orbitY = Mathf.Clamp(orbitY, yMinLimit, yMaxLimit);
			if (playerSpeed > autoResetSpeed && Mathf.Abs(orbitX) > 1f) {
				orbitResetTimer += Time.deltaTime;
			}
			if (playerSpeed > autoResetSpeed && orbitResetTimer >= 2f)
			{
				orbitX = 0f;
				orbitY = 0f;
				orbitResetTimer = 0f;
			}
		}

		/// <summary>
		/// 相机控制
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		private void MouseControl(float x,float y)
		{
			orbitX += x * orbitXSpeed * 0.2f;
			orbitY -= y * orbitYSpeed * 0.2f;
			orbitResetTimer = 0f;
		}

		/// <summary>
		/// 重置相机
		/// </summary>
		private void ResetCamera()
		{
			TPSTiltAngle = 0f;
			collisionPos = Vector3.zero;
			collisionRot = Quaternion.identity;
			orbitX = 0f;
			orbitY = 0f;
			transform.SetParent(null);
		}

		public override void ProcessLogic(IEvent evt)
        {
			if (evt is MethodEvent)
			{
				//相机旋转
				if (evt.EventName.Equals("CameraRotate"))
				{
					MethodEvent mthodEvent = evt as MethodEvent;
					object[] data = mthodEvent.Argments;
					float rotateX = (float)data[0];
					float rotateZ = (float)data[1];
					MouseControl(rotateX, rotateZ);
				}
				else if (evt.EventName.Equals("ReturnCamera")) {
					wantedRotation = wantedRotation * Quaternion.AngleAxis((180), Vector3.up);
				}
			}
		}

		private void Rotate(float x,float y) {
			GetEntity<CarControllerCameraEntity>().CameraRotate(x,y);
		}

		private void ReturnCamera() {
			GetEntity<CarControllerCameraEntity>().ReturnCamera();
		}
    }
}
