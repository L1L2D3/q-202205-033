﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： UITriggerLogic
* 创建日期：2020-06-02 08:50:50
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using Com.Rainier.Buskit3D;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class UITriggerLogic : MVVMLogicBase 
	{
        private ActionData actionData;

        private UnityAction buttonTriggerAction;

        protected override void Awake()
        {
            base.Awake();
            AddEntity<UITriggerEntity>();
            if (transform.GetComponent<ActionData>())
            {
                actionData = transform.GetComponent<ActionData>();
            }
            else
            {
                actionData = gameObject.AddComponent<ActionData>();
            }
        }

        #region Button相关
        /// <summary>
        /// 添加Button点击监听（自动高亮，点击后自动取消高亮并移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        /// <returns></returns>
        public UnityAction AddButtonListenerATLight(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                SetUIHighlight(objName, true);
                UnityAction onButtonClick = null;
                onButtonClick = new UnityAction(() => {
                    action.Invoke();
                    SetUIHighlight(objName, false);
                    actionData.RemoveTriggerAction(ActionType.Button, objName, onButtonClick);
                    UIRemoveButtonListener(objName);
                });
                UIAddButtonListener(objName);
                actionData.AddTriggerAction(ActionType.Button, objName, onButtonClick);
                return onButtonClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Button点击监听（UI框自动高亮，点击后自动取消高亮并移除监听）
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public UnityAction AddButtonListenerFrameATLight(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                SetUIFrameHighlight(objName, true);
                UnityAction onButtonClick = null;
                onButtonClick = new UnityAction(() => {
                    action.Invoke();
                    SetUIFrameHighlight(objName, false);
                    actionData.RemoveTriggerAction(ActionType.Button,objName, onButtonClick);
                    UIRemoveButtonListener(objName);
                });
                UIAddButtonListener(objName);
                actionData.AddTriggerAction(ActionType.Button, objName, onButtonClick);
                return onButtonClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Button点击监听（点击后自动移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public UnityAction AddButtonListenerAT(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                UnityAction onButtonClick = null;
                onButtonClick = new UnityAction(() => {
                    action.Invoke();
                    actionData.RemoveTriggerAction(ActionType.Button, objName, onButtonClick);
                    UIRemoveButtonListener(objName);
                });
                UIAddButtonListener(objName);
                actionData.AddTriggerAction(ActionType.Button, objName, onButtonClick);
                return onButtonClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Button点击监听
        /// </summary>
        /// <param name="objName">需要加监听的对象名</param>
        /// <param name="action">执行的方法</param>
        public UnityAction AddButtonListener(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                UIAddButtonListener(objName);
                actionData.AddTriggerAction(ActionType.Button, objName, action);
                return action;
            }
            return null;
        }

        /// <summary>
        /// 移除Button指定点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public void RemoveButtonListener(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                actionData.RemoveTriggerAction(ActionType.Button, objName, action);
                UIRemoveButtonListener(objName);
            }
        }

        /// <summary>
        /// 移除Button所有点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        public void RemoveButtonListener(string objName)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                actionData.RemoveTriggerAction(ActionType.Button, objName);
                UIRemoveButtonListener(objName);
            }
        }

        #endregion

        #region Click相关

        /// <summary>
        /// 添加Click点击监听（自动高亮，点击后自动取消高亮并移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        /// <returns></returns>
        public UnityAction AddClickListenerATLight(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                SetUIHighlight(objName, true);
                UnityAction onClick = null;
                onClick = new UnityAction(() => {
                    action.Invoke();
                    SetUIHighlight(objName, false);
                    actionData.RemoveTriggerAction(ActionType.Click, objName, onClick);
                    UIRemoveClickListener(objName);
                });
                UIAddClickListener(objName);
                actionData.AddTriggerAction(ActionType.Click, objName, onClick);
                return onClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Click点击监听（UI框自动高亮，点击后自动取消高亮并移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        /// <returns></returns>
        public UnityAction AddClickListenerFrameATLight(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                SetUIFrameHighlight(objName, true);
                UnityAction onClick = null;
                onClick = new UnityAction(() => {
                    action.Invoke();
                    SetUIFrameHighlight(objName, false);
                    actionData.RemoveTriggerAction(ActionType.Click, objName, onClick);
                    UIRemoveClickListener(objName);
                });
                UIAddClickListener(objName);
                actionData.AddTriggerAction(ActionType.Click, objName, onClick);
                return onClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Click点击监听（点击后自动移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public UnityAction AddClickListenerAT(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                UnityAction onClick = null;
                onClick = new UnityAction(() => {
                    action.Invoke();
                    actionData.RemoveTriggerAction(ActionType.Click, objName, onClick);
                    UIRemoveClickListener(objName);
                });
                UIAddClickListener(objName);
                actionData.AddTriggerAction(ActionType.Click, objName, onClick);
                return onClick;
            }
            return null;
        }

        /// <summary>
        /// 添加Click点击监听
        /// </summary>
        /// <param name="objName">需要加监听的对象名</param>
        /// <param name="action">执行的方法</param>
        public UnityAction AddClickListener(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                UIAddClickListener(objName);
                actionData.AddTriggerAction(ActionType.Click, objName, action);
                return action;
            }
            return null;
        }

        /// <summary>
        /// 移除Click指定点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public void RemoveClickListener(string objName, UnityAction action)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                actionData.RemoveTriggerAction(ActionType.Click, objName, action);
                UIRemoveClickListener(objName);
            }
        }

        /// <summary>
        /// 移除Button所有点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        public void RemoveClickListener(string objName)
        {
            UIBehaviours tmpBehaviour = GetUIBehaviours(objName);
            if (tmpBehaviour != null)
            {
                actionData.RemoveTriggerAction(ActionType.Click, objName);
                UIRemoveClickListener(objName);
            }
        }

        #endregion


        /// <summary>
        /// 添加点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void UIAddButtonListener(string triggerObjName)
        {
            if (!actionData.IsHaveAction(ActionType.Button, triggerObjName))
            {
                buttonTriggerAction = GetUIBehaviours(triggerObjName).AddButtonListener(OnTriggerButtonClick);
            }
            else if (actionData.IsHaveAction(ActionType.Button, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Button, triggerObjName) == 0)
                {
                    buttonTriggerAction = GetUIBehaviours(triggerObjName).AddButtonListener(OnTriggerButtonClick);
                }
            }
        }

        /// <summary>
        /// 移除点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void UIRemoveButtonListener(string triggerObjName)
        {
            if (actionData.IsHaveAction(ActionType.Button, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Button, triggerObjName) == 0)
                {
                    GetUIBehaviours(triggerObjName).RemoveButtonListener(buttonTriggerAction);
                }
            }
        }

        /// <summary>
        /// 添加点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void UIAddClickListener(string triggerObjName)
        {
            if (!actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                GetUIBehaviours(triggerObjName).AddClickListener(OnTriggerClick);
            }
            else if (actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Click, triggerObjName) == 0)
                {
                    GetUIBehaviours(triggerObjName).AddClickListener(OnTriggerClick);
                }
            }
        }

        /// <summary>
        /// 移除点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void UIRemoveClickListener(string triggerObjName)
        {
            if (actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Click, triggerObjName) == 0)
                {
                    GetUIBehaviours(triggerObjName).RemoveClickListener(OnTriggerClick);
                }
            }
        }

        /// <summary>
        /// 触发了点击事件
        /// </summary>
        private void OnTriggerClick(string objName) {
            GetEntity<UITriggerEntity>().OnTriggerClick(objName);
        }

        /// <summary>
        /// 出发了按键点击事件
        /// </summary>
        /// <param name="objName">对象名</param>
        private void OnTriggerButtonClick(string objName)
        {
            GetEntity<UITriggerEntity>().OnTriggerButtonClick(objName);
        }

        /// <summary>
        /// 设置UI高亮
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="isLight">是否高亮</param>
        public void SetUIHighlight(string objName, bool isLight)
        {
            GetEntity<UITriggerEntity>().SetUIHighlight(objName, isLight);
        }

        /// <summary>
        /// 设置UI框高亮
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="isLight">是否高亮</param>
        public void SetUIFrameHighlight(string objName, bool isLight)
        {
            GetEntity<UITriggerEntity>().SetUIFrameHighlight(objName, isLight);
        }


        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、Diable事件、有Destroy事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable") || evt.EventName.Equals("OnDestroy")) return;

            if (evt is MethodEvent)
            {
                //检测到高亮逻辑
                if (evt.EventName.Equals("SetUIHighlight"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    bool isLight = (bool)data[1];
                    GetUIBehaviours(objName).SetUIHighlight(isLight);
                }
                //检测到高亮逻辑
                else if (evt.EventName.Equals("SetUIFrameHighlight"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    bool isLight = (bool)data[1];
                    GetUIBehaviours(objName).SetUIFrameHighlight(isLight);
                }
                //检测到Click触发事件
                else if (evt.EventName.Equals("OnTriggerClick"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    actionData.InvokeTriggerAction(ActionType.Click, objName);
                }
                //检测到Button触发事件
                else if (evt.EventName.Equals("OnTriggerButtonClick"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    actionData.InvokeTriggerAction(ActionType.Button, objName);
                }
            }
        }

    }
}

