﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：TriggerLogic
* 创建日期：2020-05-27 18:33:38
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：3D物体点击逻辑脚本（此脚本需动态挂载）
******************************************************************************/
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Aop;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using System.Collections.Generic;

namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class TriggerLogic : ObjLogicBase 
	{
        private ActionData actionData;
        private void Awake()
        {
            AddEntity<TriggerEntity>();
            if (transform.GetComponent<ActionData>())
            {
                actionData = transform.GetComponent<ActionData>();
            }
            else
            {
                actionData = gameObject.AddComponent<ActionData>();
            }
        }

        private void OnDestroy()
        {
            RemoveEntity<TriggerEntity>();
        }

        /// <summary>
        /// 添加物体点击监听（自动高亮，点击后自动移除高亮、监听）
        /// </summary>
        /// <param name="objName">需要加监听的对象名</param>
        /// <param name="action">执行的方法</param>
        public UnityAction AddObjClickListenerATLight(string objName, UnityAction action)
        {
            ObjBehaviours tmpBehaviour = GetObjBehaviour(objName);
            if (tmpBehaviour != null)
            {
                SetObjHighlight(objName, true);
                UnityAction onTriggerClick = null;
                onTriggerClick = new UnityAction(() => {
                    SetObjHighlight(objName, false);
                    action.Invoke();
                    RemoveTriggerAction(objName, onTriggerClick);
                    ObjRemoveListener(objName);
                });
                ObjAddListener(objName);
                AddTriggerAction(objName, onTriggerClick);
                return onTriggerClick;
            }
            return null;
        }

        /// <summary>
        /// 添加物体点击监听（点击后自动移除监听）
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public UnityAction AddObjClickListenerAT(string objName, UnityAction action)
        {
            ObjBehaviours tmpBehaviour = GetObjBehaviour(objName);
            if (tmpBehaviour != null)
            {
                UnityAction onTriggerClick = null;
                onTriggerClick = new UnityAction(() => {
                    action.Invoke();
                    RemoveTriggerAction(objName, onTriggerClick);
                    ObjRemoveListener(objName);
                });
                ObjAddListener(objName);
                AddTriggerAction(objName, onTriggerClick);
                return onTriggerClick;
            }
            return null;
        }

        /// <summary>
        /// 添加物体点击监听
        /// </summary>
        /// <param name="objName">需要加监听的对象名</param>
        /// <param name="action">执行的方法</param>
        public UnityAction AddObjClickListener(string objName, UnityAction action)
        {
            ObjBehaviours tmpBehaviour = GetObjBehaviour(objName);
            if (tmpBehaviour != null)
            {
                ObjAddListener(objName);
                AddTriggerAction(objName, action);
                return action;
            }
            return null;
        }

        /// <summary>
        /// 移除物体指定点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        /// <param name="action">方法</param>
        public void RemoveObjClickListener(string objName, UnityAction action)
        {
            ObjBehaviours tmpBehaviour = GetObjBehaviour(objName);
            if (tmpBehaviour != null)
            {
                RemoveTriggerAction(objName, action);
                ObjRemoveListener(objName);
            }
        }

        /// <summary>
        /// 移除物体所有点击监听
        /// </summary>
        /// <param name="objName">对象名</param>
        public void RemoveObjClickListener(string objName)
        {
            ObjBehaviours tmpBehaviour = GetObjBehaviour(objName);
            if (tmpBehaviour != null)
            {
                RemoveTriggerAction(objName);
                ObjRemoveListener(objName);
            }
        }

        /// <summary>
        /// 存储事件
        /// </summary>
        /// <param name="triggerObjName">索引</param>
        /// <param name="triggerAction">事件</param>
        private void AddTriggerAction(string triggerObjName, UnityAction triggerAction)
        {
            actionData.AddTriggerAction(ActionType.Click, triggerObjName, triggerAction);
        }

        /// <summary>
        /// 删除事件
        /// </summary>
        /// <param name="triggerObjName">索引</param>
        /// <param name="triggerAction">事件</param>
        private void RemoveTriggerAction(string triggerObjName, UnityAction triggerAction)
        {
            actionData.RemoveTriggerAction(ActionType.Click, triggerObjName, triggerAction);
        }

        /// <summary>
        /// 删除所有事件
        /// </summary>
        /// <param name="triggerObjName">索引</param>
        private void RemoveTriggerAction(string triggerObjName)
        {
            actionData.RemoveTriggerAction(ActionType.Click, triggerObjName);
        }

        /// <summary>
        /// 添加点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void ObjAddListener(string triggerObjName)
        {
            if (!actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                GetObjBehaviour(triggerObjName).AddObjClickListener(OnTriggerClick);
            }
            else if (actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Click, triggerObjName) == 0)
                {
                    GetObjBehaviour(triggerObjName).AddObjClickListener(OnTriggerClick);
                }
            }
        }

        /// <summary>
        /// 移除点击监听
        /// </summary>
        /// <param name="triggerObjName">对象名</param>
        private void ObjRemoveListener(string triggerObjName)
        {
            if (actionData.IsHaveAction(ActionType.Click, triggerObjName))
            {
                if (actionData.GetActionCount(ActionType.Click, triggerObjName) == 0)
                {
                    GetObjBehaviour(triggerObjName).RemoveObjClickListener(OnTriggerClick);
                }
            }
        }

        /// <summary>
        /// 设置物体高亮
        /// </summary>
        /// <param name="isLight">是否高亮</param>
        public void SetObjHighlight(string objName, bool isLight)
        {
            GetEntity<TriggerEntity>().SetObjHighlight(objName, isLight);
        }

        /// <summary>
        /// 触发了点击事件
        /// </summary>
        private void OnTriggerClick(string objName)
        {
            GetEntity<TriggerEntity>().OnTriggerClick(objName);
        }

        public override void ProcessLogic(IEvent evt)
        {
            if (evt is MethodEvent)
            {
                //检测到高亮逻辑
                if (evt.EventName.Equals("SetObjHighlight"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    bool isLight = (bool)data[1];
                    GetObjBehaviour(objName).SetObjHighlight(isLight);
                }
                //检测到点击触发事件
                else if (evt.EventName.Equals("OnTriggerClick"))
                {
                    MethodEvent mthodEvent = evt as MethodEvent;
                    object[] data = mthodEvent.Argments;
                    string objName = (string)data[0];
                    actionData.InvokeTriggerAction(ActionType.Click, objName);
                }
            }
        }

    }
}
