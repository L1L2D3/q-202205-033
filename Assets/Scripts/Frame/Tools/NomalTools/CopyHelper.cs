﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： CopyHelper
* 创建日期：2021-10-11 14:17:12
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：数据结构复制帮助类
******************************************************************************/

using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class CopyHelper  
	{

        /// <summary>
        /// 深复制（转二进制流）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(T obj)
        {
            object copyValue;
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(stream, obj);
                stream.Seek(0, SeekOrigin.Begin);
                copyValue = bf.Deserialize(stream);
                stream.Close();
            }

            return (T)copyValue;
        }

    }
}

