﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：ToolTipLogic
* 创建日期：2020-07-03 16:48:45
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：ToolTip物体逻辑脚本
******************************************************************************/
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Com.Rainier.Buskit3D;
using UnityEngine.Serialization;
using System;

namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class ToolTipLogic : ObjLogicBase
    {
        private NamePair thisName;
        private void Start()
        {
            thisName = new NamePair(this.name, this.name);
            GetObjBehaviour(this.name).AddObjEnterListener(OnPointerEnter);
            GetObjBehaviour(this.name).AddObjExitListener(OnPointerExit);
            AddEntity<ToolTipEntity>();
            onMouseChoose.AddListener(GetUILogic<ToolTipPanelLogic>("ToolTip").OnMouseChoose);
        }

        private void OnDestroy()
        {
            onMouseChoose.RemoveAllListeners();
            RemoveEntity<ToolTipEntity>();
        }

        public override void ProcessLogic(IEvent evt)
        {
            if (evt.EventName.Equals("OnPointerEnter"))
            {
                isEnter = true;
            }
            if (evt.EventName.Equals("OnPointerExit"))
            {
                isEnter = false;
                onMouseChoose.Invoke(thisName, false);
            }
            if (evt.EventName.Equals("IsChildOfThis"))
            {
                PropertyEvent propertyEvent = evt as PropertyEvent;
                bool newValue = (bool)propertyEvent.NewValue;
                if (newValue)
                {
                    GetEntity<ToolTipEntity>().OnPointerExit();
                }
            }

        }

        private bool isEnter = false;

        private void OnPointerEnter()
        {
            GetEntity<ToolTipEntity>().OnPointerEnter();
        }

        private void OnPointerExit()
        {
            GetEntity<ToolTipEntity>().OnPointerExit();
        }


        private void Update()
        {
            if (isEnter)
            {
                onMouseChoose.Invoke(thisName, true);
                GetEntity<ToolTipEntity>().IsChildOfThis = !IsChildOfThis(IPointerOver.CurrentPointerOverGameObject());
                
            }
        }

        /// <summary>
        /// 此物体是否是目标物体的子物体
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsChildOfThis(GameObject obj) {
            if (obj == null)
            {
                return false;
            }
            return obj.transform.IsChildOf(transform);
        }

        [Serializable]
        public class ToolTipEvent : UnityEvent<NamePair, bool> { }

        [FormerlySerializedAs("onMouseChoose")]
        [SerializeField]
        private ToolTipEvent onMouseChoose = new ToolTipEvent();

        public ToolTipEvent OnMouseChoose
        {
            get { return onMouseChoose; }
            set { onMouseChoose = value; }
        }
    }
}
