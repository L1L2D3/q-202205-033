﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： ToolTipEntity
* 创建日期：2020-07-03 16:48:27
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using Com.Rainier.Buskit3D;

namespace Com.Rainier.ZC_Frame
{
	/// <summary>
    ///
    /// </summary>
	public class ToolTipEntity : Entity
	{

        private bool isChildOfThis = false;

        /// <summary>
        /// 是否是当前物体的子物体
        /// </summary>
        public bool IsChildOfThis {
            get { return isChildOfThis; }
            set {
                var oldvalue = isChildOfThis;
                isChildOfThis = value;
                FireEvent("IsChildOfThis", oldvalue, value);
            }
        }

        /// <summary>
        /// 当鼠标进入
        /// </summary>
        public void OnPointerEnter() {
            FireEvent("OnPointerEnter", new object[] { });
        }

        /// <summary>
        /// 当鼠标离开
        /// </summary>
        public void OnPointerExit() {
            FireEvent("OnPointerExit", new object[] { });
        }

        

    }
}

