﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：ToolTipGroupLogic
* 创建日期：2020-07-03 16:48:45
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：ToolTip物体逻辑脚本(适用于需要多个子物体显示ToolTip情况)
******************************************************************************/
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Com.Rainier.Buskit3D;
using UnityEngine.Serialization;
using System;

namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class ToolTipGroupLogic : ObjLogicBase
    {
        private string currentName = string.Empty;
        private void Start()
        {
            Transform[] childTrans = transform.GetComponentsInChildren<Transform>();
            for (int i = 0; i < childTrans.Length; i++)
            {
                if (!childTrans[i].name.Equals(this.name)&& childTrans[i].GetComponent<ObjBehaviours>())
                {
                    GetObjBehaviour(childTrans[i].name).AddObjEnterListener(OnPointerEnter);
                    GetObjBehaviour(childTrans[i].name).AddObjExitListener(OnPointerExit);
                }
            }
            AddEntity<ToolTipEntity>();
            onMouseChoose.AddListener(GetUILogic<ToolTipPanelLogic>("ToolTip").OnMouseChoose);
        }

        private void OnDestroy()
        {
            onMouseChoose.RemoveAllListeners();
            RemoveEntity<ToolTipEntity>();
        }

        public override void ProcessLogic(IEvent evt)
        {
            if (evt.EventName.Equals("OnPointerEnter"))
            {
                currentName = IPointerOver.CurrentPointerOverGameObject().name;
                GetObjBehaviour(currentName).SetObjHighlight(true);
                isEnter = true;
            }
            if (evt.EventName.Equals("OnPointerExit"))
            {
                isEnter = false;
                GetObjBehaviour(currentName).SetObjHighlight(false);
                onMouseChoose.Invoke(new NamePair(this.name, currentName), false);
            }
            if (evt.EventName.Equals("IsChildOfThis"))
            {
                PropertyEvent propertyEvent = evt as PropertyEvent;
                bool newValue = (bool)propertyEvent.NewValue;
                if (newValue)
                {
                    GetEntity<ToolTipEntity>().OnPointerExit();
                }
            }

        }

        private bool isEnter = false;

        private void OnPointerEnter()
        {
            GetEntity<ToolTipEntity>().OnPointerEnter();

        }

        private void OnPointerExit()
        {
            GetEntity<ToolTipEntity>().OnPointerExit();
        }


        private void Update()
        {
            if (isEnter)
            {
                onMouseChoose.Invoke(new NamePair(this.name, currentName), true);
                GetEntity<ToolTipEntity>().IsChildOfThis = !IsChildOfThis(IPointerOver.CurrentPointerOverGameObject());
            }
        }

        /// <summary>
        /// 此物体是否是目标物体的子物体
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsChildOfThis(GameObject obj) {
            if (obj == null)
            {
                return false;
            }
            return obj.transform.IsChildOf(transform);
        }

        [Serializable]
        public class ToolTipEvent : UnityEvent<NamePair, bool> { }

        [FormerlySerializedAs("onMouseChoose")]
        [SerializeField]
        private ToolTipEvent onMouseChoose = new ToolTipEvent();

        public ToolTipEvent OnMouseChoose
        {
            get { return onMouseChoose; }
            set { onMouseChoose = value; }
        }
    }
}
