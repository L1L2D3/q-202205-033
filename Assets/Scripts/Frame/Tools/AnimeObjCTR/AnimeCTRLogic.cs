﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：AnimeCTRLogic
* 创建日期：2020-11-17 11:20:04
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：生成和销毁动画物体管理
******************************************************************************/
using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Aop;

namespace Com.Rainier.ZC_Frame
{   
	/// <summary>
    /// 
    /// </summary>
	public class AnimeCTRLogic : ObjLogicBase 
	{

        /// <summary>
        /// 加载动画
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="gameObjectName">名字</param>
        public void LoadAnime(string path, string gameObjectName)
        {
            LoadGameObject(path, gameObjectName, gameObject);
        }

        /// <summary>
        /// 销毁所有动画
        /// </summary>
        public void DestroyAllAnime()
        {
            if (transform.childCount != 0)
            {
                for (int i = transform.childCount - 1; i >= 0; i--)
                {
                    DestroyImmediate(transform.GetChild(i).gameObject);
                }
            }
        }

        public override void ProcessLogic(IEvent evt)
        {
            
        }
    }
}
