﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： ActionDataManager
* 创建日期：2021-12-22 10:26:22
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：事件存储脚本
******************************************************************************/

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class ActionData  :MonoBehaviour
	{
		/// <summary>
		/// 事件存储器
		/// </summary>
		private Dictionary<ActionType, Dictionary<string, List<UnityAction>>> triggerActions;

        private void Awake()
        {
			triggerActions = new Dictionary<ActionType, Dictionary<string, List<UnityAction>>>();
		}

		/// <summary>
		/// 存储事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		/// <param name="triggerAction">事件</param>
        public void AddTriggerAction(ActionType actionType, string objName, UnityAction triggerAction) {
            if (IsHaveAction(actionType, objName))
            {
				triggerActions[actionType][objName].Add(triggerAction);
			}
            else
            {
				List<UnityAction> tmpActions = new List<UnityAction>();
				tmpActions.Add(triggerAction);
                if (triggerActions.ContainsKey(actionType))
                {
					triggerActions[actionType].Add(objName, tmpActions);
				}
                else
                {
					Dictionary<string, List<UnityAction>> tmpobjAction = new Dictionary<string, List<UnityAction>>();
					tmpobjAction.Add(objName, tmpActions);
					triggerActions.Add(actionType, tmpobjAction);
				}
				
			}
		}

		/// <summary>
		/// 删除事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		/// <param name="triggerAction">事件</param>
		public void RemoveTriggerAction(ActionType actionType, string objName, UnityAction triggerAction)
		{
			if (IsHaveAction(actionType, objName))
			{
				triggerActions[actionType][objName].Remove(triggerAction);
			}
		}

		/// <summary>
		/// 获取事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		/// <returns></returns>
		public List<UnityAction> GetTriggerAction(ActionType actionType, string objName)
		{
			if (IsHaveAction(actionType, objName))
			{
				return triggerActions[actionType][objName];
			}
			else
			{
				Debug.Log(string.Format("未找到物体：[{0}] 中的事件", objName));
				return null;
			}
		}

		/// <summary>
		/// 执行事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		public void InvokeTriggerAction(ActionType actionType, string objName) {
			if (IsHaveAction(actionType, objName))
			{
				List<UnityAction> unityActions = triggerActions[actionType][objName];
                for (int i = 0; i < unityActions.Count; i++)
                {
					unityActions[i].Invoke();
				}
			}
			else
			{
				Debug.Log(string.Format("未找到物体：[{0}] 中的 [{1}] 事件", objName, actionType));
				
			}
		}

		/// <summary>
		/// 删除指定对象的所有事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		public void RemoveTriggerAction(ActionType actionType, string objName)
		{
			if (IsHaveAction(actionType, objName))
			{
				triggerActions[actionType].Remove(objName);
			}
		}

		/// <summary>
		/// 删除指定类型的所有事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		public void RemoveRootTriggerAction(ActionType actionType) {
            if (triggerActions.ContainsKey(actionType))
            {
				triggerActions.Remove(actionType);
			}
		}

		/// <summary>
		/// 是否存在事件
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		/// <returns></returns>
		public bool IsHaveAction(ActionType actionType, string objName) {
            foreach (var item in triggerActions)
            {
                if (item.Key == actionType)
                {
                    foreach (var childItem in item.Value)
                    {
                        if (childItem.Key == objName)
                        {
							return true;
						}
                    }
                }
            }
			return false;
		}

		/// <summary>
		/// 获取事件数量
		/// </summary>
		/// <param name="actionType">事件类型</param>
		/// <param name="objName">对象名</param>
		/// <returns></returns>
		public int GetActionCount(ActionType actionType, string objName) {
            if (IsHaveAction(actionType, objName))
            {
				return triggerActions[actionType][objName].Count;

			}
            else
            {
				Debug.LogError(string.Format("未发现物体：[{0}] 的[{1}]类型事件", objName, actionType));
				return -1;
            }
		}

	}
}

