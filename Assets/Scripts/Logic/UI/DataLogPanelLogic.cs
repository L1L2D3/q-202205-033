﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：DataLogPanelLogic
* 创建日期：2022-08-15 17:19:08
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/
using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using Com.Rainier.ZC_Frame;
using UnityEngine.Events;

namespace Com.Rainier.yh
{   
	/// <summary>
    /// 
    /// </summary>
	public class DataLogPanelLogic : MVVMLogicBase
	{
		private string _lastLog;
        /// <summary>
        /// 处理业务逻辑
        /// </summary>
        /// <param name="evt"></param>
        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、OnDiable事件、有OnDestroy事件、有OnEnable事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable")|| evt.EventName.Equals("OnDestroy") || evt.EventName.Equals("OnEnable"))	return;

            //忽略除ViewModel外其他Entity发出的事件
            if (!(evt.EventSource is ViewModel))	return;

			//获取事件源
            ViewModel vm = evt.EventSource as ViewModel;

            if (vm.name.Equals("NoButton"))
            {
	            if (GetUIBehaviours("Type").Text.Contains("Control")||FindObjectOfType<EmbryoManager>().Rate <= 0)
	            {
		            GetUIBehaviours("NoButton").Scale=Vector3.zero;
		            InvokeCallBackAction();
	            }
	            else
	            {
		            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("在仔细观察一下吧!","提示","好的");
	            }
            }
            
            if (vm.name.Equals("NoHatchButton"))
            {
	            if (FindObjectOfType<EmbryoManager>().Rate <= 0)
	            {
		            GetUIBehaviours("NoHatchButton").Scale=Vector3.zero;
		            InvokeCallBackAction();
	            }
	            else
	            {
		            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("在仔细观察一下吧!","提示","好的");
	            }
            }

            if (vm.name.Equals("BasePanelButton"))
            {
	            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("完成镜检和表格，才能进行下一步哟!","提示","确定");
            }
        }

        public bool Result()
        {
	        EmbryoManager embryoManager = FindObjectOfType<EmbryoManager>();
	        int count = 0;
	        for (int i = 0; i < 5; i++)
	        {
		        if ((int) embryoManager.DeathRate[embryoManager.CurrentChoice][i] * 20 ==
		            int.Parse(GetUIBehaviours("Log" + (i + 1)).Text))
			        count += 1;
	        }

	        if (count >= 5)
		        return true;
	        else
		        return false;
        }
        public void SetCallBack(UnityAction action)
        {
	        SetCallBackAction(action);
        }

        public void SetUnderWayTextStyle(string name)
        {
	        if (_lastLog != null)
	        {
		        GetUIBehaviours(_lastLog).textMeshPro.color=Color.black;
	        }
	        _lastLog = name;
	        GetUIBehaviours(_lastLog).textMeshPro.color=Color.cyan;
        }

        public void Show(string type,string path)
        {
	        GetUIBehaviours(this.name).Scale=Vector3.one;
	        GetUIBehaviours("Table").ImageSprite =
		        Resources.Load<Sprite>("Image/表格/" + path);
	        GetUIBehaviours("Type").Text = type;
	        if(GetUIBehaviours("NoButton"))
		        GetUIBehaviours("NoButton").Scale=Vector3.one;
	        else
	        {
		        if(GetUIBehaviours("NoHatchButton"))
			        GetUIBehaviours("NoHatchButton").Scale=Vector3.one;
	        }

        }

        public void Hide()
        {
	        GetUIBehaviours(this.name).Scale=Vector3.zero;
        }
	}
}
