﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：ExperimentProgressPanelLogic
* 创建日期：2022-08-03 15:05:28
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using Com.Rainier.ZC_Frame;
using UnityEngine.UI;

namespace Com.Rainier.yh
{   
	/// <summary>
    /// 
    /// </summary>
	public class ExperimentProgressPanelLogic : MVVMLogicBase
	{
		public Sprite acute;
		public Sprite subchronic;
		public Sprite test;

		private UIBehaviours[] _objects; 
		private void Start()
		{
			_objects = GetComponentsInChildren<UIBehaviours>().Where(x=>x.name!=this.name).ToArray();
			Hide();
		}

		/// <summary>
        /// 处理业务逻辑
        /// </summary>
        /// <param name="evt"></param>
        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、OnDiable事件、有OnDestroy事件、有OnEnable事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable")|| evt.EventName.Equals("OnDestroy") || evt.EventName.Equals("OnEnable"))	return;

            //忽略除ViewModel外其他Entity发出的事件
            if (!(evt.EventSource is ViewModel))	return;

			//获取事件源
            ViewModel vm = evt.EventSource as ViewModel;

            if (!vm.name.Contains("子"))
            {
	            for (int i = 0; i < _objects.Length; i++)
	            {
		            if (_objects[i].name.Contains(vm.name + "子"))
			            _objects[i].SetActive(!_objects[i].IsActive());
	            }
            }
        }

		public void Show()
		{
			GetUIBehaviours(this.name).Scale=Vector3.one;
		}

		public void Hide()
		{
			GetUIBehaviours(this.name).Scale=Vector3.zero;
		}

		public void ChangeState(string root,string experi)
		{
			switch (root)
			{
				case "三氯生急性毒性":
					GetUIBehaviours(root).ImageSprite = acute;
					break;
				case "三氯生亚慢性毒性":
					GetUIBehaviours(root).ImageSprite = subchronic;
					break;
				case "转录组测序":
					GetUIBehaviours(root).ImageSprite = test;
					return;
					break;
			}

			var ex = root + "子-" + experi;
			GetUIBehaviours(ex).ImageSprite = GetUIBehaviours(ex).Button().spriteState.selectedSprite;
		}
    }
}
