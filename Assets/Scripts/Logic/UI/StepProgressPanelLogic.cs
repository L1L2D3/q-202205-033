﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：StepProgressPanelLogic
* 创建日期：2022-08-02 16:17:52
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using Com.Rainier.ZC_Frame;
using TMPro;
using UnityEngine.UI;

namespace Com.Rainier.yh
{   
	/// <summary>
    /// 步骤进度
    /// </summary>
	public class StepProgressPanelLogic : MVVMLogicBase
	{

		public Sprite ShrinkD;
		public Sprite ShrinkC;
		public Sprite ExpandD;
		public Sprite ExpandC;

		private bool isExpand;
		
		public Sprite unfinishedSprite;
		public Sprite underwaySprite;
		public Sprite completeSprite;

		private void Start()
		{
			Hide();
		}

		/// <summary>
        /// 处理业务逻辑
        /// </summary>
        /// <param name="evt"></param>
        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、OnDiable事件、有OnDestroy事件、有OnEnable事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable")|| evt.EventName.Equals("OnDestroy") || evt.EventName.Equals("OnEnable"))	return;

            //忽略除ViewModel外其他Entity发出的事件
            if (!(evt.EventSource is ViewModel))	return;

			//获取事件源
            ViewModel vm = evt.EventSource as ViewModel;

            if (vm.name.Equals("StepProgressSwitch"))
            {
	            if(isExpand)
		            Shrink();
	            else
		            Expand();
            }
        }

		public void Show()
		{
			GetUIBehaviours(this.name).Scale=Vector3.one;
		}

		public void Hide()
		{
			GetUIBehaviours(this.name).Scale=Vector3.zero;
		}

        private void Shrink()
        {
	        isExpand = false;
	        GetUIBehaviours("StepProgressPanelBG").LocalPosition=new Vector3(-1200,230);
	        GetUIBehaviours("StepProgressSwitch").ImageSprite = ExpandD;
	        GetUIBehaviours("StepProgressSwitch").GetComponent<Image>().SetNativeSize();
	        GetUIBehaviours("StepProgressSwitch").ChangeButtonClickSprite(ExpandC,ExpandC,null);
        }

        private void Expand()
        {
	        isExpand = true;
	        GetUIBehaviours("StepProgressPanelBG").LocalPosition=new Vector3(-758,230);
	        GetUIBehaviours("StepProgressSwitch").ImageSprite = ShrinkD;
	        GetUIBehaviours("StepProgressSwitch").GetComponent<Image>().SetNativeSize();
	        GetUIBehaviours("StepProgressSwitch").ChangeButtonClickSprite(ShrinkC,ShrinkC,null);
        }

        public void Initialize(string[] texts)
        {
	        while (GetUIBehaviours("Progress").transform.childCount>0)
	        {
		        DestroyImmediate(GetUIBehaviours("Progress").transform.GetChild(0).gameObject);
	        }
	        Show();
	        Expand();
	        for (int i = 0; i < texts.Length; i++)
	        {
		        LoadItem(i);
		        GetUIBehaviours("StepName" + i).Text = texts[i];
	        }
	        GetUIBehaviours("Progress").ReFreshLayout();
	        GetUIBehaviours("Progress").ImageFillAmount=0;
	        GetUIBehaviours("ProgressBase").GetComponent<RectTransform>().sizeDelta=new Vector2(6,GetUIBehaviours("Progress").GetComponent<RectTransform>().rect.height);
        }

        private void LoadItem(int number)
        {
	        GameObject option = Instantiate(Resources.Load<GameObject>("Prefabs/Step"),GetGameObject("Progress").transform,false);
	        option.name = option.name.Remove(option.name.Length - 7);
	        Transform[] childTransforms = option.transform.GetComponentsInChildren<Transform>();
	        for (int i = 0; i < childTransforms.Length; i++)
	        {
		        childTransforms[i].gameObject.name += number.ToString();
		        childTransforms[i].gameObject.AddComponent<UIBehaviours>();
		        GetComponent<MvvmContext>().Initialize(childTransforms[i]);
	        }
        }

        public void ChangeState(int number,StepState state)
        {
	        UIBehaviours behaviours = GetUIBehaviours("StepState" + number);
	        if (behaviours)
	        {
		        switch (state)
		        {
			        case StepState.Underway:
				        InjectService.Get<ObjManager>().GetObjBehaviours("交互物体",GetUIBehaviours("StepName"+number).Text).SetActive(true);
				        InjectService.Get<ObjManager>().GetObjBehaviours("目标位置",GetUIBehaviours("StepName"+number).Text).SetActive(true);
				        GetUIBehaviours("Progress").ImageFillAmount = number * ((float)1/(GetUIBehaviours("Progress").transform.childCount-1));
				        behaviours.ImageSprite = underwaySprite;
				        break;
			        case  StepState.Complete:
				        InjectService.Get<ObjManager>().GetObjBehaviours("交互物体",GetUIBehaviours("StepName"+number).Text).SetActive(false);
				        InjectService.Get<ObjManager>().GetObjBehaviours("目标位置",GetUIBehaviours("StepName"+number).Text).SetActive(false);
				        behaviours.ImageSprite = completeSprite;
				        break;
		        }
	        }
	        else
	        {
		        Debug.LogError("没有指定步骤");
	        }
        }
        
        public enum StepState
        {
	        Unfinished
	        ,Underway
	        ,Complete
        }
    }
}
