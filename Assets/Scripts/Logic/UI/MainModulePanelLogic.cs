﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：MainModulePanelLogic
* 创建日期：2022-07-30 21:02:49
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

namespace Com.Rainier.yh
{   
	/// <summary>
    /// 
    /// </summary>
	public class MainModulePanelLogic : MVVMLogicBase
	{

		public Sprite unfinishedSprite;
		public Sprite underwaySprite;
		public Sprite completeSprite;

		private string _currentKnowledge=null;

		private RectMask2D _contentMask;

		private int _currentPage;

		private string[] _knowledgeStrings;


		private static Dictionary<string, Experiment[]> _experiments=new Dictionary<string, Experiment[]>();

		public class Experiment
		{
			public string eName;
			public ExperimentState eState;
		}
		


		/// <summary>
		/// 初始化实验
		/// </summary>
		private void InitializedExperiment()
		{
			if(_experiments.Count>0)
				return;
			Experiment[] item1=new Experiment[2];
			item1[0]=new Experiment(); 
			item1[1]=new Experiment(); 
			item1[0].eName = "三氯生对斑马鱼的毒性测定";
			item1[0].eState = ExperimentState.Unfinished;
			item1[1].eName = "三氯生对斑马鱼胚胎发育的影响";
			item1[1].eState = ExperimentState.Unfinished;
			Experiment[] item2=new Experiment[2];
			item2[0]=new Experiment(); 
			item2[1]=new Experiment(); 
			item2[0].eName = "TCS亚慢性暴毒的行为学实验";
			item2[0].eState = ExperimentState.Unfinished;
			item2[1].eName = "斑马鱼成鱼(脑组织酶活性)的测定";
			item2[1].eState = ExperimentState.Unfinished;
			_experiments.Add("三氯生急性毒性",item1);
			_experiments.Add("三氯生亚慢性毒性",item2);
			_experiments.Add("转录组测序",null);
			foreach (var item in _experiments)
			{
				GetUIBehaviours(item.Key + "State").ImageSprite = unfinishedSprite;
				if(item.Value!=null)
				foreach (var experiment in item.Value)
				{
					GetUIBehaviours(experiment.eName + "State").ImageSprite = unfinishedSprite;
				}
			}
		}

		private void InitializedObj()
		{
			Transform gameObject1=GameObject.Find("交互物体").transform;
			Transform gameObject2=GameObject.Find("目标位置").transform;
			for (int i = 0; i < gameObject1.childCount; i++)
			{
				var child= gameObject1.GetChild(i).gameObject;
				for (int j = 0; j <child.transform.childCount ; j++)
				{
					child.transform.GetChild(j).gameObject.SetActive(false);
				}
				child.SetActive(false);
			}
			for (int i = 0; i < gameObject2.childCount; i++)
			{
				var child= gameObject2.GetChild(i).gameObject;
				for (int j = 0; j <child.transform.childCount ; j++)
				{
					child.transform.GetChild(j).gameObject.SetActive(false);
				}
				child.SetActive(false);
			}
		}
		private void Start()
		{
			InitializedExperiment();
			InitializedObj();
			ChangeExperimentState("三氯生对斑马鱼的毒性测定",ExperimentState.Underway);
			//GetUIBehaviours("转录组测序State").ImageSprite=underwaySprite;
			_contentMask = GetUIBehaviours("ContentMask").GetComponent<RectMask2D>();
			_currentPage = 0;
			GetUIBehaviours("NextPage").Scale=Vector3.zero;
			Show();
		}

		public void Show()
		{
			UpdateExperimentState();
			GetUIBehaviours(this.name).Scale=Vector3.one;
		}

		public void Hide()
		{
			GetUIBehaviours(this.name).Scale=Vector3.zero;
		}

		/// <summary>
        /// 处理业务逻辑
        /// </summary>
        /// <param name="evt"></param>
        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、OnDiable事件、有OnDestroy事件、有OnEnable事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable")|| evt.EventName.Equals("OnDestroy") || evt.EventName.Equals("OnEnable"))	return;

            //忽略除ViewModel外其他Entity发出的事件
            if (!(evt.EventSource is ViewModel))	return;

			//获取事件源
            ViewModel vm = evt.EventSource as ViewModel;

            #region 一级按钮
            switch (vm.name)
            {
	            case "Knowledge":
		            GetUIBehaviours("KnowledgePanel").Scale=Vector3.one;
		            GetUIBehaviours("Content").Text = Resources.Load("Txt/知识学习/急性毒性/0").ToString();
		            _currentKnowledge = "急性毒性";
		            GetUIBehaviours("B-急性毒性").Button().interactable = false;
		            break;
	            case "Purpose":
		            GetUIBehaviours("PurposePanel").Scale=Vector3.one;
		            break;
	            case "PurposeCloseButton":
		            GetUIBehaviours("MainModuleButton").Scale=Vector3.zero;
		            GetUIBehaviours("ExperimentChoicePanel").Scale=Vector3.one;
		            break;
	            case "Into":
		            GetUIBehaviours("MainModuleButton").Scale=Vector3.zero;
		            GetUIBehaviours("ExperimentChoicePanel").Scale=Vector3.one;
		            break;
            }
            #endregion

            #region 知识学习

            if (vm.name.Contains("B-"))
            {
	            GetUIBehaviours(vm.name).Button().interactable = false;
	            GetUIBehaviours("B-" + _currentKnowledge).Button().interactable = true;
	            _currentKnowledge = vm.name.Remove(0, 2);
	            _knowledgeStrings = Resources.LoadAll<TextAsset>("Txt/知识学习/" + _currentKnowledge).Select(x => x.text).ToArray();
	            if(_knowledgeStrings.Length>1)
		            GetUIBehaviours("NextPage").Scale=Vector3.one;
	            else 
		            GetUIBehaviours("NextPage").Scale=Vector3.zero;
	            _currentPage = 0;
	            ShowKnowledge(_currentPage);
            }
            //下一页
            if (vm.name.Equals("NextPage"))
            {
	            _currentPage += 1;
	            if(_currentPage==_knowledgeStrings.Length-1)
		            GetUIBehaviours("NextPage").Scale=Vector3.zero;
	            if (_currentPage > _knowledgeStrings.Length-1)
	            {
		            _currentPage -= 1;
	            }
	            else
		            ShowKnowledge(_currentPage);

            }
            #endregion

            #region 实验选择

            if (vm.name.Contains("E-"))
            {
	            if (vm.name.Equals("E-转录组测序"))
	            {
		            if (GetUIBehaviours(vm.name.Remove(0, 2) + "State").ImageSprite == underwaySprite)
		            {
			            FindObjectOfType<IOC>().gameObject.AddComponent<RNASeqStep>();
			            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel")
				            .ChangeState(vm.name.Remove(0, 2), String.Empty);
			            GetUIBehaviours(this.name).Scale=Vector3.zero;
			            return;
		            }
		            else
		            {
			            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("无法进入实验，您还没有完成前置实验","提示","确定");
			            return;
		            }

	            }
	            GetUIBehaviours(vm.name.Remove(0,2)).Scale=Vector3.one;
            }

            if (vm.name.Contains("I-"))
            {
	            var experimentName = vm.name.Remove(0, 2);
	            if (GetExperimentState(experimentName) == ExperimentState.Underway||GetExperimentState(experimentName)==ExperimentState.Complete)
	            {
		            switch (GetUIBehaviours(vm.name).Parent.name)
		            {
			            case "三氯生急性毒性":
				            if (!FindObjectOfType<IOC>().gameObject.GetComponent<AcuteToxicityStep>())
				            {
					            var step1 = FindObjectOfType<IOC>().gameObject.AddComponent<AcuteToxicityStep>();
					            step1.CurrentExperiment = experimentName;
				            }
				            else
				            {
					            var step1 = FindObjectOfType<IOC>().gameObject.GetComponent<AcuteToxicityStep>();
					            step1.CurrentExperiment = experimentName;
					            step1.Init();
				            }
				            GetUIBehaviours(this.name).Scale=Vector3.zero;

				            break;
			            case "三氯生亚慢性毒性":
				            if (!FindObjectOfType<IOC>().gameObject.GetComponent<SubchronicToxicityStep>())
				            {
					            Debug.Log(1);
					            var step1 = FindObjectOfType<IOC>().gameObject.AddComponent<SubchronicToxicityStep>();
					            step1.CurrentExperiment = experimentName;
				            }
				            else
				            {
					            Debug.Log(2);
					            var step1 = FindObjectOfType<IOC>().gameObject.GetComponent<SubchronicToxicityStep>();
					            step1.CurrentExperiment = experimentName;
					            step1.Init();
				            }
				            GetUIBehaviours(this.name).Scale=Vector3.zero;
				            break;
		            }
	            }
	            else
	            {
		            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("无法进入实验，您还没有完成前置实验","提示","确定");
	            }
	            //ChangeExperimentState(experimentName,ExperimentState.Underway);

            }


            #endregion
            

            //关闭按钮
            if (vm.name.Contains("CloseButton"))
            {
	            GetUIBehaviours(vm.name.Replace("CloseButton","Panel")).Scale=Vector3.zero;
            }
        }

		//知识学习文字
		private void ShowKnowledge(int page)
		{
			GetUIBehaviours("Content").Text = _knowledgeStrings[page];
			float currentValue=520;
			Vector4 newPadding;
			DOTween.To(() => currentValue, x => currentValue = x, 0, 4).OnUpdate(() =>
			{
				newPadding = new Vector4(0, currentValue, 0, 0);
				_contentMask.padding = newPadding;
			}).SetAutoKill(true);
		}

		/// <summary>
		/// 切换实验状态
		/// </summary>
		/// <param name="experimentName"></param>
		/// <param name="state"></param>
		public void ChangeExperimentState(string experimentName,ExperimentState state)
		{
			for (int i = 0; i < _experiments.Count; i++)
			{
				var temp = _experiments.ElementAt(i);
				foreach (var experiment in temp.Value)
				{
					if (experiment.eName.Equals(experimentName))
					{
						experiment.eState = state;
						GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel")
							.ChangeState(temp.Key, experiment.eName);
						UpdateExperimentState();
						return;
					}
					else
						continue;
				}
			}
		}

		/// <summary>
		/// 更新实验状态
		/// </summary>
		private void UpdateExperimentState()
		{
			foreach (var item in _experiments)
			{
				int underway = 0;
				int complete = 0;
				if(item.Value!=null)
					foreach (var experiment in item.Value)
					{
						if (experiment.eState == ExperimentState.Underway)
						{
							GetUIBehaviours(experiment.eName + "State").ImageSprite = underwaySprite;
							underway += 1;
						}

						if (experiment.eState == ExperimentState.Complete)
						{
							GetUIBehaviours(experiment.eName + "State").ImageSprite = completeSprite;
							complete += 1;
						}

						if (underway == 1)
							GetUIBehaviours(item.Key + "State").ImageSprite = underwaySprite;
						if (complete == 1)
							GetUIBehaviours(item.Key + "State").ImageSprite = underwaySprite;
						if (complete == 2)
							GetUIBehaviours(item.Key + "State").ImageSprite = completeSprite;
					}
			}
		}

		public ExperimentState GetExperimentState(string name)
		{
			foreach (var item in _experiments)
			{
				if (item.Value != null)
					foreach (var experiment in item.Value)
					{
						if (experiment.eName == name)
						{
							return experiment.eState;
						}
					}
			}

			return ExperimentState.Unfinished;
		}

		public enum ExperimentState
		{
			Unfinished
			,Underway
			,Complete
		}
	}
}
