﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：LargeViewPanelLoic
* 创建日期：2022-08-15 19:34:45
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Com.Rainier.Buskit3D;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Com.Rainier.yh
{   
	/// <summary>
    /// 
    /// </summary>
	public class LargeViewPanelLoic : MVVMLogicBase
	{

		private ObjBehaviours _obj;

		private Vector3[] _path;

		private Camera _myCamera;
		
		private void Start()
		{
			Init();
		}


		private void Init()
		{
			_obj = InjectService.Get<ObjManager>().GetObjBehaviours("LargeView", "胚胎");
			_path = InjectService.Get<ObjManager>().GetObjBehaviours("LargeView", "FishPath").transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray();
			GetUIBehaviours("RawImage").AddDragListener(OnRawImageDrag);
			_myCamera = InjectService.Get<ObjManager>().GetObjBehaviours("LargeView", "Camera").GetComponent<Camera>();
			EventTrigger trigger = GetComponent<EventTrigger>();
			EventTrigger.Entry entry=new EventTrigger.Entry();
			entry.eventID = EventTriggerType.Scroll;
			entry.callback.AddListener(OnScroll);
			trigger.triggers.Add(entry);
		}
		/// <summary>
        /// 处理业务逻辑
        /// </summary>
        /// <param name="evt"></param>
        public override void ProcessLogic(IEvent evt)
        {
            //忽略所有属性初始化事件、OnDiable事件、有OnDestroy事件、有OnEnable事件
            if ((evt is PropertyInitEvent) || evt.EventName.Equals("OnDisable")|| evt.EventName.Equals("OnDestroy") || evt.EventName.Equals("OnEnable"))	return;

            //忽略除ViewModel外其他Entity发出的事件
            if (!(evt.EventSource is ViewModel))	return;

			//获取事件源
            ViewModel vm = evt.EventSource as ViewModel;
            if (vm.name.Equals("Close"))
            {
	            Hide();
            }

            if (vm.name.Equals("Select"))
            {
	            Hide();
	            InvokeCallBackAction();
            }
            
        }

		public void Hide()
		{
			GetUIBehaviours(this.name).Scale=Vector3.zero;
			DOTween.Kill("胚胎活");
			_obj.LocalRotation=Vector3.zero;
			_obj.LocalPosition=Vector3.zero;
			_obj.Scale=Vector3.one;
			_myCamera.fieldOfView = 60;
		}

        public void Show(string name,UnityAction action)
        {
	        GetUIBehaviours(this.name).Scale=Vector3.one;
	        SetCallBackAction(action);
	        for (int i = 0; i < _obj.transform.childCount; i++)
	        {
		        if (_obj.transform.GetChild(i).name == name)
		        {
			        _obj.transform.GetChild(i).gameObject.SetActive(true);
			        if (name.Contains("胚胎"))
			        {
				        if (name.Contains("Live"))
				        {
					        if (name.Contains("24") || name.Contains("48"))
						        _obj.transform.DOScale(1.1f, 2).SetLoops(50).SetId("胚胎活");
					        else
						        _obj.transform.DOPath(_path, 20).SetLookAt(0).SetId("胚胎活").SetLoops(50);
				        }
			        }
		        }
		        else
			        _obj.transform.GetChild(i).gameObject.SetActive(false);
	        }
        }
        
        void OnRawImageDrag()
        {
	        float h = Input.GetAxis("Mouse X");
	        float v = Input.GetAxis("Mouse Y");
	        Vector2 offset = new Vector2(h, v) * 5;
	        _obj.transform.Rotate(Vector3.up * -offset.x, Space.World);//左右旋转
	        _obj.transform.Rotate(Vector3.right * offset.y, Space.World);//上下旋转
        }

        void OnScroll(BaseEventData data)
        {
	        float value = -Input.GetAxis("Mouse ScrollWheel")  * 10;
	        _myCamera.fieldOfView+=value;
	        _myCamera.fieldOfView = Mathf.Clamp(_myCamera.fieldOfView, 30, 70);
        }
	}
}
