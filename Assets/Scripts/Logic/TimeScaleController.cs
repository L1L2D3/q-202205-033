﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleController : MonoBehaviour
{
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            Time.timeScale += 1;
        if (Input.GetKeyDown(KeyCode.DownArrow))
            Time.timeScale -= 1;
        Time.timeScale = Mathf.Clamp(Time.timeScale,1, 8);
    }
}
