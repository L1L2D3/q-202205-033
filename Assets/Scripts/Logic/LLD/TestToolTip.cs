﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： TestToolTip
* 创建日期：2020-07-03 19:50:21
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class TestToolTip : MonoBehaviour 
	{
        private Dictionary<NamePair, string> tips;

        private void Start()
        {
            tips = new Dictionary<NamePair, string>();
            tips.Add(new NamePair("交互物体", "心脏"), "心脏");
            tips.Add(new NamePair("交互物体", "肠"), "肠");
            tips.Add(new NamePair("交互物体", "胰腺"), "胰腺");
            tips.Add(new NamePair("交互物体", "肾脏"), "肾脏");
            tips.Add(new NamePair("交互物体", "脾"), "脾");
            tips.Add(new NamePair("交互物体", "肝脏"), "肝脏");
            tips.Add(new NamePair("交互物体", "肌肉"), "肌肉");
            transform.GetComponent<ToolTipPanelLogic>().SetToolTip(tips);
        }

    }
}

