﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：AcuteToxicityStep
* 创建日期：2022-08-02 15:39:56
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Com.Rainier.yh
{
    /// <summary>
    /// 三氯生急性毒性
    /// </summary>
    public class AcuteToxicityStep : StepLogicBase
    {
        protected override void Start()
        {
            base.Start();
            StartStepLogic();
        }


        public void Init()
        {
            base.Start();
            step.RemoveAllStep();
            StartStepLogic();
        }

        /// <summary>
        /// 学习模式步骤存储
        /// </summary>
        protected override void StudyStepInput()
        {
            if (CurrentExperiment == "三氯生对斑马鱼的毒性测定")
            {
                #region 胚胎收集
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, AStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "按照雌雄比为3：4将斑马鱼成鱼放入杂交缸中", new NamePair("交互物体", "杂交缸"),
                    Step.TriggerType.ObjLight, AStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "第二天早上，将隔板取出，开启日光灯，使雌雄成鱼自由交配产卵,0.5h后收集胚胎", new NamePair("交互物体", "隔板"),
                    Step.TriggerType.ObjLight, AStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "提起杂交缸内胆,用巴氏吸管将受精卵转移到结晶皿内", new NamePair("交互物体", "内胆"),
                    Step.TriggerType.ObjLight, AStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, new NamePair("交互物体", "恒温培养箱"), Step.TriggerType.ObjLight, AStep_5, null);
                
                #endregion
                
                #region 暴露液配置
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, BStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将在冰箱内低温保存的三氯生母液（100mg/L）取出", new NamePair("交互物体", "冰箱1"),
                    Step.TriggerType.ObjLight, BStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用移液枪分别向容量瓶内加入2μL、3μL、4μL、5μL、6μL、7μL、8μL的三氯生母液", new NamePair("交互物体", "容量瓶"),
                    Step.TriggerType.ObjLight, BStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在容量瓶内加入斑马鱼饲养水定容到10ml", new NamePair("交互物体", "烧杯"),
                    Step.TriggerType.ObjLight, BStep_4, null);
                
                #endregion
                
                #region 三氯生暴露处理
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, CStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "每个培养皿中随机选取（20 粒发育良好的受精卵）加至6孔细胞培养板", new NamePair("交互物体", "培养皿1"),
                    Step.TriggerType.ObjLight, CStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "吸干水分", new NamePair("交互物体", "移液枪6"),
                    Step.TriggerType.ObjLight, CStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "每孔依次加入2ml梯度浓度三氯生溶液", new NamePair("交互物体", "离心管处理"),
                    Step.TriggerType.ObjLight, CStep_4, null);
                
                #endregion
                
                #region 镜检统计死亡率
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "准备镜检", NamePair.Empty, Step.TriggerType.none, DStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "除对照外，请在设置的7个三氯生浓度处理钟任意选中一个浓度进行镜检", NamePair.Empty, Step.TriggerType.none, DStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "每 24 h 观察并记录死亡的胚胎数", new NamePair("交互物体", "恒温培养箱2"), Step.TriggerType.ObjLight, DStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜"), Step.TriggerType.ObjLight, DStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_10, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_11, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_12, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_13, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "记录死亡数，统计死亡率", NamePair.Empty, Step.TriggerType.none, DStep_14, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "点击数据填充，查看对照和三氯生各处理组24/48/72/96/120hpf 的死亡率", NamePair.Empty, Step.TriggerType.none, DStep_15, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, DStep_16, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, DStep_17, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "通过镜检，我们得到了120hpf对照组和三氯生处理的死亡率（表中为3次技术重复数据）", NamePair.Empty, Step.TriggerType.none, DStep_18, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "以斑马鱼120hpf死亡率为纵坐标，以三氯生浓度为横坐标，可以得到剂量-效应曲线和三氯生对斑马鱼的LC50。", NamePair.Empty, Step.TriggerType.none, DStep_19, null);
                
                 #endregion
            }
            else
            {

                #region 三氯生暴露处理
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, EStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从28℃恒温培养箱内将收集的胚胎取出，放到体视显微镜上，结晶皿以黑底相衬，侧面加以灯光观察", new NamePair("交互物体", "恒温培养箱3"), 
                    Step.TriggerType.ObjLight, EStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "每个结晶皿中随机选取（20 粒发育良好的受精卵）加至6孔细胞培养板", new NamePair("交互物体", "培养皿3"), 
                    Step.TriggerType.ObjLight, EStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从冰箱内取出配制好的三氯生溶液（262μg/L，52.5μg/L），依次向加入六孔板内加入2ml（进行3次技术重复）", new NamePair("交互物体", "试剂瓶"), 
                    Step.TriggerType.ObjLight, EStep_4, null);
                
                 #endregion
                
                #region 对胚胎发育的影响
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, FStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理24h 后进行1次镜检观察", new NamePair("交互物体", "恒温培养箱4"), Step.TriggerType.ObjLight, FStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜2"), Step.TriggerType.ObjLight, FStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, FStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理48h 后进行第2次镜检观察", new NamePair("交互物体", "恒温培养箱4"), Step.TriggerType.ObjLight, FStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜2"), Step.TriggerType.ObjLight, FStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_10, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, FStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理72h 后进行第2次镜检观察", new NamePair("交互物体", "恒温培养箱4"), Step.TriggerType.ObjLight, FStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜2"), Step.TriggerType.ObjLight, FStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_11, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_12, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_13, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, FStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理96h 后进行第2次镜检观察", new NamePair("交互物体", "恒温培养箱4"), Step.TriggerType.ObjLight, FStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜2"), Step.TriggerType.ObjLight, FStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_14, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_15, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_16, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, FStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理120h 后进行第2次镜检观察", new NamePair("交互物体", "恒温培养箱4"), Step.TriggerType.ObjLight, FStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜2"), Step.TriggerType.ObjLight, FStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_17, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_18, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育畸形的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, FStep_19, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, FStep_20, null);
                
                #endregion
                
                #region 胚胎孵化影响
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, GStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理48h 后进行1次镜检观察", new NamePair("交互物体", "恒温培养箱5"), Step.TriggerType.ObjLight, GStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜3"), Step.TriggerType.ObjLight, GStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, GStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理53h 后进行2次镜检观察", new NamePair("交互物体", "恒温培养箱5"), Step.TriggerType.ObjLight, GStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜3"), Step.TriggerType.ObjLight, GStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_10, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, GStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "三氯生暴露处理53h 后进行2次镜检观察", new NamePair("交互物体", "恒温培养箱5"), Step.TriggerType.ObjLight, GStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜3"), Step.TriggerType.ObjLight, GStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_11, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_12, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在当前视野中点击选择发育迟缓的胚胎，观察并记录", NamePair.Empty, Step.TriggerType.none, GStep_13, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, GStep_14, null);
                
                #endregion

                #region 对心跳的影响
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, HStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "72h 后镜检观察，记录幼鱼10s内的心跳次数", new NamePair("交互物体", "恒温培养箱6"), Step.TriggerType.ObjLight, HStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜4"), Step.TriggerType.ObjLight, HStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty, Step.TriggerType.none, HStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "96h 后镜检观察，记录幼鱼10s内的心跳次数", new NamePair("交互物体", "恒温培养箱6"), Step.TriggerType.ObjLight, HStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "镜检", new NamePair("交互物体", "显微镜4"), Step.TriggerType.ObjLight, HStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_10, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请在显微镜下视野中选择幼鱼进行观察！", NamePair.Empty, Step.TriggerType.none, HStep_11, null);

                #endregion
                

            }
        }

        #region 胚胎收集

        /// <summary>
        /// 初始配置
        /// </summary>
        private void AStep_1()
        {
            string[] names = {"斑马鱼的胚胎收集", "斑马鱼暴露液的配置", "三氯生暴露处理", "镜检统计死亡率", "剂量-效应曲线"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "AStep_1相机目标位置");
            GetObjBehaviour("交互物体", "雌鱼组1").Scale = Vector3.zero;
            GetObjBehaviour("交互物体", "雄鱼组1").Scale = Vector3.zero;
            NextStep();
        }

        private void AStep_2()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "杂交缸").GetChildObj("盖子").DOLocalMoveX(-1, 2));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "雌鱼组1").Scale = Vector3.one);
            sequence.Append(GetObjBehaviour("交互物体", "雌鱼组1").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.1f, 0), 2));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "雄鱼组1").Scale = Vector3.one);
            sequence.Append(GetObjBehaviour("交互物体", "雄鱼组1").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.1f, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "钟").GetChildObj("时针")
                .DOLocalRotate(new Vector3(0, 0, -360), 3, RotateMode.FastBeyond360));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "杂交缸").Collider().enabled = false);
            sequence.OnComplete(NextStep);
        }

        private void AStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "隔板").transform.DOPath(GetObjBehaviour("目标位置", "隔板路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "雄鱼组1").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼组路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).SetLookAt(0).SetLoops(2));
            sequence.Join(GetObjBehaviour("交互物体", "雌鱼组1").transform.DOPath(GetObjBehaviour("目标位置", "雌鱼组路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).SetLookAt(0).SetLoops(2));
            sequence.Join(GetObjBehaviour("交互物体", "钟").GetChildObj("时针").transform
                .DOLocalRotate(new Vector3(0, 0, -20), 5, RotateMode.FastBeyond360));
            sequence.OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "内胆").Collider().enabled = true;
                GetObjBehaviour("交互物体", "雌鱼组1").Parent = GetObjBehaviour("交互物体", "内胆").transform;
                GetObjBehaviour("交互物体", "雄鱼组1").Parent = GetObjBehaviour("交互物体", "内胆").transform;
                NextStep();
            });
        }

        private void AStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "内胆").transform.DOPath(GetObjBehaviour("目标位置", "内胆路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "受精卵组").SetPos("目标位置", "受精卵组目标位置");
                GetObjBehaviour("交互物体", "巴氏吸管").SetObjHighlight(true);
                GetObjBehaviour("交互物体", "巴氏吸管").AddObjClickListener(() =>
                {
                    GetObjBehaviour("交互物体", "巴氏吸管").SetObjHighlight(false);
                    GetObjBehaviour("交互物体", "巴氏吸管").RemoveObjClickListener();
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "AStep_4相机目标位置", 2, () =>
                    {
                        GetObjBehaviour("交互物体", "受精卵组").SetObjHighlight(true);
                        GetObjBehaviour("交互物体", "受精卵组").AddObjClickListener(() =>
                        {
                            GetObjBehaviour("交互物体", "受精卵组").RemoveObjClickListener();
                            GetObjBehaviour("交互物体", "受精卵组").SetObjHighlight(false);
                            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "AStep_1相机目标位置", 2, () =>
                            {
                                Absorb(1);
                                WaitTimeDoFunc(16,
                                    () =>
                                    {
                                        GetObjLogic<LookCameraLogic>("Main Camera")
                                            .MoveLookCamera("目标位置", "AStep_5目标位置", 1, NextStep);
                                    });
                            });
                        });
                    });
                });
            });
        }

        private void AStep_5()
        {
            for (int i = 1; i < 6; i++)
            {
                GetObjBehaviour("交互物体", "受精卵" + i).Parent = GetObjBehaviour("交互物体", "培养皿").transform;
            }

            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("培养箱的温度设置为28摄氏度","提示","确定");
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "培养皿").transform.DOPath(GetObjBehaviour("目标位置", "培养皿路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱").GetChildObj("门")
                .DOBlendableLocalRotateBy(new Vector3(0, -90, 0), 2));
            sequence.OnComplete(() =>
            {
                Sprite sprite = Resources.Load<Sprite>("Image/受精卵");
                GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(sprite, "受精卵", "确认", () =>
                {
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                    NextStep();
                });
            });
        }

        /// <summary>
        /// 吸取受精卵
        /// </summary>
        /// <param name="index"></param>
        private void Absorb(int index)
        {
            GetObjBehaviour("目标位置", "巴氏吸管位置3").Position =
                (GetObjBehaviour("交互物体", "受精卵" + index).Position + new Vector3(0, 0.0518f, 0));
            Vector3[] path =
            {
                GetObjBehaviour("目标位置", "巴氏吸管位置1").Position, GetObjBehaviour("目标位置", "巴氏吸管位置2").Position,
                GetObjBehaviour("目标位置", "巴氏吸管位置3").Position
            };
            GetObjBehaviour("交互物体", "巴氏吸管").transform.DOPath(path, 1.5f).OnComplete(
                () =>
                {
                    GetObjBehaviour("交互物体", "受精卵" + index).Parent = GetObjBehaviour("交互物体", "巴氏吸管").transform;
                    GetObjBehaviour("交互物体", "巴氏吸管").transform
                        .DOPath(path.Reverse().ToArray(), 1.5f).OnComplete(() =>
                        {
                            GetObjBehaviour("交互物体", "受精卵" + index).Parent = GetObjBehaviour("交互物体", "受精卵组").transform;
                            GetObjBehaviour("交互物体", "受精卵" + index).Position =
                                GetObjBehaviour("目标位置", "受精卵" + index).Position;
                            index += 1;
                            if (index > 5)
                                return;
                            Absorb(index);
                        });
                });
        }

        #endregion

        #region 暴露液的配置

        private void BStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(" ", "提示", "本实验采用静水式法", () =>
            {
                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "B相机位置1");
                NextStep();
            });
            GetUIBehaviour("WarningPanel", "WarningPanelText")
                .TweenTMPText(Resources.Load("Txt/文字介绍/斑马鱼暴露液的配置").ToString(), 4, null);
        }

        private void BStep_2()
        {
            GetObjBehaviour("交互物体", "冰箱1").GetChildObj("门").DOLocalRotate(new Vector3(0, -90, 0), 1.5f).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "冰箱1").Collider().enabled = false;
                GetObjBehaviour("交互物体", "三氯生母液100").SetObjHighlight(true);
                GetObjBehaviour("交互物体", "三氯生母液100").AddObjClickListener(() =>
                {
                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "B相机位置2");
                    GetObjBehaviour("交互物体", "三氯生母液100").SetPos("目标位置", "三氯生母液100目标位置");
                    GetObjBehaviour("交互物体", "冰箱1").GetChildObj("门").localEulerAngles = Vector3.zero;
                    GetObjBehaviour("交互物体", "三氯生母液100").SetObjHighlight(false);
                    GetObjBehaviour("交互物体", "三氯生母液100").RemoveObjClickListener();
                    NextStep();
                });
            });
        }

        private void BStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            GetObjBehaviour("交互物体","三氯生母液100").GetChildObj("瓶盖").localScale=Vector3.zero;
            var origin = GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition;
            sequence.Append(GetObjBehaviour("交互物体", "移液枪2").transform.DOPath(GetObjBehaviour("目标位置", "移液枪路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 5));
            sequence.AppendInterval(1);
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "三氯生母液100").GetChildObj("瓶盖").localScale = Vector3.one;
                for (int i = 1; i < 8; i++)
                {
                    GetObjBehaviour("交互物体", "容量瓶"+i).GetChildObj("盖子").localScale=Vector3.zero;
                }
            });
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin);
            sequence.Append(Move());
            sequence.Append(Inject());
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器").localPosition = origin;
                GetObjBehaviour("交互物体", "移液枪2").SetPos("目标位置", "移液枪路径");
                NextStep();
            });
        }

        private void BStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "烧杯").transform
                .DOMove(GetObjBehaviour("目标位置", "烧杯目标位置").Position, 2f));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "烧杯").SetPos("目标位置", "烧杯目标位置"));
            sequence.Append(GetObjBehaviour("交互物体", "玻璃棒").transform
                .DOMove(GetObjBehaviour("目标位置", "玻璃棒目标位置").Position, 2.5f));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "玻璃棒").SetPos("目标位置", "玻璃棒目标位置"));
            sequence.AppendInterval(2);
            sequence.Append(GetObjBehaviour("交互物体", "容量瓶1").GetChildObj("溶液").DOScale(Vector3.one, 1.5f));
            sequence.Join(GetObjBehaviour("交互物体", "烧杯").GetChildObj("水").DOScaleZ(0.01f, 1.5f));
            sequence.AppendCallback(() =>
            {
                for (int i = 2; i < 8; i++)
                {
                    GetObjBehaviour("交互物体", "容量瓶" + i).GetChildObj("溶液").localScale = Vector3.one;
                }

                GetObjBehaviour("交互物体", "烧杯").SetActive(false);
                GetObjBehaviour("交互物体", "玻璃棒").SetActive(false);
            });
            sequence.Append(GetObjBehaviour("交互物体", "容量瓶1").transform.DOLocalRotate(new Vector3(180, 0, 0), 2));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "容量瓶1").MoveTo("目标位置", "容量瓶1目标位置", 2,
                    () => GetObjBehaviour("交互物体", "离心管1").GetChildObj("盖子").localScale = Vector3.zero);
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "B相机位置3", 3, () => sequence.Play());
            });
            sequence.Append(GetObjBehaviour("交互物体", "离心管1").GetChildObj("溶液").DOScale(Vector3.one, 4));
            sequence.Join(GetObjBehaviour("交互物体", "容量瓶1").GetChildObj("溶液").DOScale(Vector3.zero, 4));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "离心管1").GetChildObj("盖子").localScale = Vector3.one;
                GetObjBehaviour("交互物体", "容量瓶1").transform.localEulerAngles = Vector3.zero;
                for (int i = 2; i < 8; i++)
                {
                    GetObjBehaviour("交互物体", "离心管" + i).GetChildObj("溶液").localScale = Vector3.one;
                    GetObjBehaviour("交互物体", "容量瓶" + i).GetChildObj("溶液").localScale = Vector3.zero;
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                    NextStep();
                }
            });
        }

        private Tween Inject()
        {
            return GetObjBehaviour("交互物体", "移液枪2").GetChildObj("注射器")
                .DOBlendableLocalMoveBy(new Vector3(0, -0.01f, 0), 1);
        }

        private Tween Move()
        {
            return GetObjBehaviour("交互物体", "移液枪2").transform.DOBlendableLocalMoveBy(new Vector3(0.035f, 0, 0), 1);
        }

        #endregion

        #region 三氯生暴露处理

        private void PutIn(int index)
        {
            GetObjBehaviour("目标位置", "巴氏吸管路径").GetChildObj("3").position =
                (GetObjBehaviour("交互物体", "受精卵组"+index).Position);
            var path = GetObjBehaviour("目标位置", "巴氏吸管路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray();
            GetObjBehaviour("交互物体", "巴氏吸管1").transform.DOPath(path, 2).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "受精卵组"+index).Scale = new Vector3(0.5f, 0.5f, 0.5f);
                GetObjBehaviour("交互物体", "巴氏吸管1").transform
                    .DOPath(path.Reverse().ToArray(), 1.5f).OnComplete(() =>
                    {
                        index += 1;
                        if (index > 3)
                        {
                            for (int i = 4; i < 9; i++)
                            {
                                GetObjBehaviour("交互物体", "受精卵组"+i).Scale = new Vector3(0.5f, 0.5f, 0.5f);  
                            }
                            NextStep();
                            return;
                        }
                        PutIn(index);
                    });
            });

        }

        private void CStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "C相机位置1");
            NextStep();
        }

        private void CStep_2()
        {
            GetObjBehaviour("交互物体","六孔培养皿1").GetChildObj("盖子").localScale=Vector3.zero;
            GetObjBehaviour("交互物体","六孔培养皿2").GetChildObj("盖子").localScale=Vector3.zero;
            PutIn(1);
        }

        private void CStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOPath(GetObjBehaviour("目标位置", "移液枪路径1").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("2").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("3").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("4").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("5").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("6").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("7").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("8").position, 1.5f));
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置2").Position, 2));
            sequence.AppendCallback(NextStep);
        }

        private void CStep_4()
        {
            for (int i = 8; i < 15; i++)
            {
                GetObjBehaviour("交互物体", "离心管" + i).GetChildObj("盖子").localScale = Vector3.zero;
            }
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管8").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("2").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("交互物体", "离心管9").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("3").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管10").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("4").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管11").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("5").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管12").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("6").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管13").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("7").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform.DOMove(
                GetObjBehaviour("交互物体", "离心管14").GetChildObj("盖子").position += new Vector3(0, 0.05f, 0), 1.5f));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "移液枪6").transform
                .DOMove(GetObjBehaviour("目标位置", "移液枪位置").GetChildObj("8").position, 1.5f));
            sequence.AppendInterval(1);
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "C相机位置3", 2, () =>
                    WaitTimeDoFunc(2, () => GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "C相机位置4",
                        2, () =>
                            WaitTimeDoFunc(2,
                                () => GetObjLogic<LookCameraLogic>("Main Camera")
                                    .MoveLookCamera("目标位置", "C相机位置2", 2, () => sequence.Play())))));
            });
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱1").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "六孔培养皿1").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿1路径")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "六孔培养皿2").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿2路径")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱1").GetChildObj("门").DOLocalRotate(new Vector3(0, 0, 0), 1));
            sequence.AppendCallback(() =>
            {
                GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                    .ChangeState(2, StepProgressPanelLogic.StepState.Complete);
                NextStep();
            });

        }

        #endregion

        #region 镜检统计死亡率

        private void DStep_1()
        {
            string[] names = {"斑马鱼的胚胎收集", "斑马鱼暴露液的配置", "三氯生暴露处理", "镜检统计死亡率", "剂量-效应曲线"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(3, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "DStep相机位置1", 2, NextStep);
            GetObjBehaviour("交互物体", "胚胎组").GetComponent<EmbryoManager>().InitDie();
            GetObjBehaviour("交互物体", "胚胎组").Scale=Vector3.zero;
        }

        private void DStep_2()
        {
            GetUILogic<BasePanelLogic>("镜检统计死亡率").ShowPanel(() =>
            {
                Toggle choice = GetUILogic<BasePanelLogic>("镜检统计死亡率").GetComponentsInChildren<Toggle>()
                    .First(x => x.isOn);
                FindObjectOfType<EmbryoManager>().CurrentChoice = choice.name;
                NextStep();
            });
        }

        private void DStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱2").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "六孔培养皿3").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿3路径")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.OnComplete(NextStep);
        }

        private void DStep_4()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").GetComponent<Camera>().nearClipPlane = 0.01f;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "DStep相机位置2", 2, () =>
            {
                GetObjBehaviour("交互物体", "六孔培养皿3").Scale=Vector3.zero;
                GetObjBehaviour("交互物体", "培养皿2").SetPos("目标位置", "培养皿2目标位置");
                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "DStep相机位置3");
                GetObjBehaviour("交互物体", "胚胎组").Scale=Vector3.one;
                NextStep();
            });

        }


        private void DStep_5()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 24hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener("Control",0,24));
        }

        private void DStep_6()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察"+FindObjectOfType<EmbryoManager>().CurrentChoice.Remove(3)+"微克/升 24hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener(FindObjectOfType<EmbryoManager>().CurrentChoice,0,24));
        }

        private void DStep_7()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 48hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener("Control", 1, 48));
        }

        private void DStep_8()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察" + FindObjectOfType<EmbryoManager>().CurrentChoice.Remove(3) + "微克/升 48hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表",
                "提示", "好的", () =>
                    AddEmbryoListener(FindObjectOfType<EmbryoManager>().CurrentChoice, 1, 48));
        }
        
        private void DStep_9()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 72hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener("Control", 2, 72));
        }

        private void DStep_10()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察" + FindObjectOfType<EmbryoManager>().CurrentChoice.Remove(3) + "微克/升 72hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表",
                "提示", "好的", () =>
                    AddEmbryoListener(FindObjectOfType<EmbryoManager>().CurrentChoice, 2, 72));
        }
        private void DStep_11()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 96hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener("Control", 3, 96));
        }

        private void DStep_12()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察" + FindObjectOfType<EmbryoManager>().CurrentChoice.Remove(3) + "微克/升 96hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表",
                "提示", "好的", () =>
                    AddEmbryoListener(FindObjectOfType<EmbryoManager>().CurrentChoice, 3, 96));
        }
        
        private void DStep_13()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 120hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表", "提示", "好的", () =>
                    AddEmbryoListener("Control", 4, 120));
        }

        private void DStep_14()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察" + FindObjectOfType<EmbryoManager>().CurrentChoice.Remove(3) +
                "微克/升 120hpf 镜检视野中的胚胎，选择死亡胚胎死亡个数填入右边的列表",
                "提示", "好的", () =>
                    AddEmbryoListener(FindObjectOfType<EmbryoManager>().CurrentChoice, 4, 120));
        }

        private void DStep_15()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/死亡率-空"), "提示", "数据填充", NextStep);
        }
        
        private void DStep_16()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/死亡率-不空"), "提示", "确定", NextStep);

        }
        
        private void DStep_17()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("该实验需进行3次技术重复", "注意", "我知道了", NextStep);

        }

        private void DStep_18()
        {
            GetUILogic<DataLogPanelLogic>("胚胎/幼鱼死亡数记录表").Hide();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(3, StepProgressPanelLogic.StepState.Complete);
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/表格/死亡率"), "数据", "确定", NextStep);
        }

        private void DStep_19()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(4, StepProgressPanelLogic.StepState.Underway);
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/表格/三氯生"), "数据", "确定",
                () =>
                {
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(4, StepProgressPanelLogic.StepState.Complete);
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
                    GetUILogic<StepTipLogic>("StepTip").Hide();
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("三氯生对斑马鱼的毒性测定",MainModulePanelLogic.ExperimentState.Complete);
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("三氯生对斑马鱼胚胎发育的影响",MainModulePanelLogic.ExperimentState.Underway);
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").Show();
                    GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Hide();
                });
        }

        private void AddEmbryoListener(string type,int number,int time)
        {
            GetUILogic<DataLogPanelLogic>("胚胎/幼鱼死亡数记录表").Show(type + " " + time + "hpf",FindObjectOfType<EmbryoManager>().CurrentChoice.Replace("/", ""));
            GetUILogic<DataLogPanelLogic>("胚胎/幼鱼死亡数记录表").SetCallBack(NextStep);
            if (type.Equals("Control"))
            {
                GetObjBehaviour("交互物体","胚胎组").Scale=Vector3.zero;
                GetObjBehaviour("交互物体","Control").Scale=Vector3.one;
                GetUILogic<DataLogPanelLogic>("胚胎/幼鱼死亡数记录表").SetUnderWayTextStyle("C" + (number + 1));
                for (int i = 1; i < 21; i++)
                {
                    for (int j = 0; j < GetObjBehaviour("交互物体", "对照胚胎" + i).transform.childCount; j++)
                    {
                        GetObjBehaviour("交互物体", "对照胚胎" + i).transform.GetChild(j).gameObject.SetActive(false);
                    }
                    GetObjBehaviour("交互物体", "对照胚胎" + i).GetChildObj("胚胎"+time+"Live").gameObject.SetActive(true);
                    GetObjBehaviour("交互物体", "对照胚胎" + i).RemoveObjClickListener();
                    GetObjBehaviour("交互物体", "对照胚胎" + i).AddObjClickListener(() =>
                        GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show("胚胎" + time + "Live", () => 
                            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("该胚胎是发育正常的哦！请重新选择", "提示", "好的")));
                }
            }
            else
            {
                GetObjBehaviour("交互物体","胚胎组").Scale=Vector3.one;
                GetObjBehaviour("交互物体","Control").Scale=Vector3.zero;
                GetUILogic<DataLogPanelLogic>("胚胎/幼鱼死亡数记录表").SetUnderWayTextStyle("Log" + (number + 1));
                for (int i = 1; i < 21; i++)
                {
                    GetObjBehaviour("交互物体", "胚胎" + i).RemoveObjClickListener();
                    GetObjBehaviour("交互物体", "胚胎" + i).SetObjHighlight(false);
                }
                int right = FindObjectOfType<EmbryoManager>().Config(type, number, time, EmbryoManager.EmbryoType.胚胎);
                int rightCount = 0;
                for (int i = 1; i < 21; i++)
                {
                    var index = i;
                    GetObjBehaviour("交互物体", "胚胎" + i).AddObjClickListener(() =>
                    {
                        GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show(GetObjBehaviour("交互物体", "胚胎" + index).GetComponent<EmbryoData>().ActiveObj,
                            () =>
                            {
                                if (GetObjBehaviour("交互物体", "胚胎" + index).GetComponent<EmbryoData>().embryoState == EmbryoData.EmbryoState.Die)
                                {
                                    int value = int.Parse(GetUIBehaviour("胚胎/幼鱼死亡数记录表", "Log" + (number + 1)).Text) + 1;
                                    rightCount = value;
                                    GetUIBehaviour("胚胎/幼鱼死亡数记录表", "Log" + (number + 1)).Text = value.ToString();
                                    GetObjBehaviour("交互物体", "胚胎" + index).SetObjHighlight(true);
                                    GetObjBehaviour("交互物体", "胚胎" + index).RemoveObjClickListener();
                                    if (rightCount == right)
                                        NextStep();
                                }
                                else
                                    GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("该胚胎是发育正常的哦！请重新选择", "提示", "好的");
                            });
                    });
                }
            }
        }
        #endregion

        #region 三氯生暴露处理

        private void EStep_1()
        {
            string[] names = {"三氯生暴露处理(胚胎影响)", "对胚胎发育的影响", "对胚胎孵化的影响", "对心跳频率的影响"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","EStep相机位置1");
            NextStep();
        }

        private void EStep_2()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱3").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "培养皿3").transform.DOPath(GetObjBehaviour("目标位置", "培养皿3路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱3").GetChildObj("门").DOLocalRotate(new Vector3(0, 0, 0), 2));
            sequence.Append(GetObjBehaviour("Main Camera","Main Camera").transform.DOMove(GetObjBehaviour("目标位置","EStep相机位置2").Position,2));
            sequence.Join(GetObjBehaviour("Main Camera","Main Camera").transform.DORotate(GetObjBehaviour("目标位置","EStep相机位置2").Rotation,2));
            sequence.OnComplete(NextStep);
        }

        private void EStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "巴氏吸管2").transform.DOPath( 
                GetObjBehaviour("目标位置", "巴氏吸管2路径").transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3.5f));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "六孔培养皿4").GetChildObj("受精卵组1").localScale = Vector3.one/4);
            sequence.Append(GetObjBehaviour("交互物体", "巴氏吸管2").transform.DOPath(GetObjBehaviour("目标位置", "巴氏吸管2路径2").
                transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 5));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "六孔培养皿4").GetChildObj("受精卵组2").localScale = Vector3.one/4);
            sequence.Append(GetObjBehaviour("交互物体", "巴氏吸管2").transform.DOPath(GetObjBehaviour("目标位置", "巴氏吸管2路径3").
                transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 5));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "六孔培养皿4").GetChildObj("受精卵组3").localScale = Vector3.one / 4;
                GetObjBehaviour("交互物体", "巴氏吸管2").Scale=Vector3.zero;
            });
            sequence.Append(GetObjBehaviour("交互物体", "移液枪7").transform.DOPath(GetObjBehaviour("目标位置", "移液枪7路径").
                transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 8));
            sequence.Append(GetObjBehaviour("交互物体", "移液枪7").transform.DOMove(GetObjBehaviour("目标位置", "移液枪7路径").Position, 2));
            sequence.OnComplete(()=>GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置","EStep相机位置3",2,NextStep));
        }

        private void EStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            GetObjBehaviour("交互物体","试剂瓶").GetChildObj("盖子1").transform.localScale=Vector3.zero;
            GetObjBehaviour("交互物体","试剂瓶").GetChildObj("盖子2").transform.localScale=Vector3.zero;
            sequence.Append(GetObjBehaviour("交互物体", "移液枪9").transform.DOPath( 
                GetObjBehaviour("目标位置", "移液枪9路径").transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 15));
            sequence.Append(GetObjBehaviour("交互物体", "移液枪9").transform.DOMove(GetObjBehaviour("目标位置", "移液枪9路径").Position, 2));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "试剂瓶").GetChildObj("盖子1").transform.localScale = Vector3.one;
                GetObjBehaviour("交互物体", "试剂瓶").GetChildObj("盖子2").transform.localScale = Vector3.one;
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "EStep相机位置4", 2, () => sequence.Play());
            });
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱3").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "六孔培养皿4").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿4路径").transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 4));
            sequence.Append(GetObjBehaviour("交互物体", "恒温培养箱3").GetChildObj("门").DOLocalRotate(new Vector3(0, 0, 0), 2));
            sequence.Append(GetObjBehaviour("交互物体", "钟2").GetChildObj("时针").DOLocalRotate(new Vector3(0, 0, -360), 3, RotateMode.FastBeyond360));
            sequence.AppendCallback(() => GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Complete));
            sequence.OnComplete(NextStep);
        }

        #endregion

        #region 对胚胎发育的影响

        private void FStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().nearClipPlane = 0.01f;
            GetObjBehaviour("交互物体","52μg/L").GetComponent<EmbryoManager>().InitGrowth();
            GetObjBehaviour("交互物体","262μg/L").GetComponent<EmbryoManager>().InitGrowth();
            GetObjBehaviour("交互物体", "52μg/L").SetActive(false);
            GetObjBehaviour("交互物体","262μg/L").SetActive(false);
            NextStep();
        }

        private void FStep_2()
        {
            GetObjBehaviour("交互物体", "钟3").GetChildObj("时针")
                .DOLocalRotate(new Vector3(0, 0, -360), 3, RotateMode.FastBeyond360).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "恒温培养箱4").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2)
                        .OnComplete(() =>
                            GetObjBehaviour("交互物体", "六孔培养皿5").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿5路径")
                                    .transform
                                    .GetComponentsInChildren<Transform>().Select(x => x.position).Reverse().ToArray(),
                                3)
                                .OnComplete(() =>
                                {
                                    GetObjBehaviour("交互物体", "恒温培养箱4").GetChildObj("门").localEulerAngles = Vector3.zero;
                                    GetObjLogic<LookCameraLogic>("Main Camera")
                                        .MoveLookCamera("目标位置", "FStep相机位置1", 2, NextStep);
                                }));
                });
        }

        private void FStep_3()
        {
            GetObjBehaviour("交互物体","培养皿4").SetPos("目标位置","培养皿4初始位置");
            GetObjBehaviour("交互物体", "六孔培养皿5").Scale=Vector3.zero;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "FStep相机位置2", 2, NextStep);
        }

        private void FStep_4()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","FStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 24hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("Control", 0, 24));
        }
        
        private void FStep_5()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 24hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("52μg/L", 0, 24));
        }

        private void FStep_6()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 24hpf 镜检视野中的胚胎，选择死亡胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("262μg/L", 0, 24));
        }

        private void FStep_7()
        {
            GetUILogic<DataLogPanelLogic>("胚胎畸形数记录表").Hide();
            GetObjBehaviour("交互物体","六孔培养皿5").Scale=Vector3.one;
            GetObjBehaviour("交互物体","六孔培养皿5").SetPos("目标位置","六孔培养皿5初始位置");
            GetObjBehaviour("交互物体","培养皿4").Position=Vector3.zero;
            GetUIBehaviour("胚胎畸形数记录表", "Control").Text = 0.ToString();
            GetUIBehaviour("胚胎畸形数记录表", "52μg/L").Text = 0.ToString();
            GetUIBehaviour("胚胎畸形数记录表", "262μg/L").Text = 0.ToString();
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置","FStep相机位置0",2,NextStep);
        }

        private void FStep_8()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","FStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 48hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("Control", 1, 48));
        }

        private void FStep_9()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 48hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("52μg/L", 1, 48));
        }
        
        private void FStep_10()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 48hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("262μg/L", 1, 48));
        }
        
        private void FStep_11()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","FStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 72hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("Control", 2, 72));
        }
        
        private void FStep_12()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 72hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
            AddGrowthListener("52μg/L",2,72));
        }

        private void FStep_13()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 72hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
            AddGrowthListener("262μg/L",2,72));
        }
        
        private void FStep_14()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","FStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 96hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("Control", 3, 96));
        }
        
        private void FStep_15()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 96hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("52μg/L", 3, 96));
        }

        private void FStep_16()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 96hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("262μg/L", 3, 96));
        }
        
        private void FStep_17()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","FStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 120hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
            AddGrowthListener("Control",4,120));
        }
        
        private void FStep_18()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 120hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("52μg/L", 4, 120));
        }

        private void FStep_19()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 120hpf 镜检视野中的胚胎，选择胚胎畸形个数填入右边的列表", "提示", "好的", () =>
                    AddGrowthListener("262μg/L", 4, 120));
        }

        private void FStep_20()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("请继续观察其它吧","提示","好的", () =>
            {
                GetUILogic<DataLogPanelLogic>("胚胎畸形数记录表").Hide();
                GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                    .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                NextStep();
            });
        }
        
         private void AddGrowthListener(string type,int number,int time)
        {
            GetUILogic<DataLogPanelLogic>("胚胎畸形数记录表").Show(type,time+"畸形");
            GetUILogic<DataLogPanelLogic>("胚胎畸形数记录表").SetCallBack(()=>
            {
                GetObjBehaviour("交互物体", "52μg/L").SetActive(false);
                GetObjBehaviour("交互物体","262μg/L").SetActive(false);
                NextStep();
            });
            GetUILogic<DataLogPanelLogic>("胚胎畸形数记录表").SetUnderWayTextStyle(type);
            if (type.Equals("Control"))
            {
                GetObjBehaviour("交互物体","52μg/L").SetActive(false);
                GetObjBehaviour("交互物体","262μg/L").SetActive(false);
                GetObjBehaviour("交互物体","Control1").Scale=Vector3.one;
                for (int i = 1; i < 21; i++)
                {
                    for (int j = 0; j < GetObjBehaviour("交互物体", "畸形对照胚胎" + i).transform.childCount; j++)
                    {
                        GetObjBehaviour("交互物体", "畸形对照胚胎" + i).transform.GetChild(j).gameObject.SetActive(false);
                    }
                    GetObjBehaviour("交互物体", "畸形对照胚胎" + i).GetChildObj("畸形"+time+"Normal").gameObject.SetActive(true);
                    GetObjBehaviour("交互物体", "畸形对照胚胎" + i).RemoveObjClickListener();
                    GetObjBehaviour("交互物体", "畸形对照胚胎" + i).AddObjClickListener(()=>
                        GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show("畸形" + time + "Normal", () => 
                            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("该胚胎是发育正常的哦！请重新选择", "提示", "好的")));
                }
            }
            else
            {
                GetObjBehaviour("交互物体","Control1").Scale=Vector3.zero;
                if (type.Equals("52μg/L"))
                {
                    GetObjBehaviour("交互物体","262μg/L").SetActive(false);
                    GetObjBehaviour("交互物体","52μg/L").SetActive(true);
                }
                else
                {
                    GetObjBehaviour("交互物体","262μg/L").SetActive(true);
                    GetObjBehaviour("交互物体","52μg/L").SetActive(false);
                }
                for (int i = 1; i < 21; i++)
                {
                    GetObjBehaviour("交互物体", type+"畸形胚胎" + i).RemoveObjClickListener();
                    GetObjBehaviour("交互物体", type+"畸形胚胎" + i).RemoveObjClickListener();
                    GetObjBehaviour("交互物体", type+"畸形胚胎" + i).SetObjHighlight(false);
                }
                int right = FindObjectOfType<EmbryoManager>().ConfigGrowth(type, number, time, EmbryoManager.EmbryoType.畸形);
                int rightCount = 0;
                for (int i = 1; i < 21; i++)
                {
                     var index = i;
                     GetObjBehaviour("交互物体", type+"畸形胚胎" + i).AddObjClickListener(() =>
                    {
                        GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show(
                            GetObjBehaviour("交互物体", type + "畸形胚胎" + index).GetComponent<EmbryoData>().ActiveObj,
                            () =>
                            {
                                if (GetObjBehaviour("交互物体", type + "畸形胚胎" + index).GetComponent<EmbryoData>()
                                        .embryoState ==
                                    EmbryoData.EmbryoState.Malformation)
                                {
                                    int value = int.Parse(GetUIBehaviour("胚胎畸形数记录表", type).Text) + 1;
                                    rightCount = value;
                                    GetUIBehaviour("胚胎畸形数记录表", type).Text = value.ToString();
                                    GetObjBehaviour("交互物体", type + "畸形胚胎" + index).SetObjHighlight(true);
                                    GetObjBehaviour("交互物体", type + "畸形胚胎" + index).RemoveObjClickListener();
                                    if (rightCount == right)
                                    {
                                        GetObjBehaviour("交互物体", "262μg/L").SetActive(false);
                                        GetObjBehaviour("交互物体", "52μg/L").SetActive(false);
                                        NextStep();
                                    }
                                }
                                else
                                    GetUILogic<WarningPanelLogic>("WarningPanel")
                                        .SetWarning("该胚胎是发育正常的哦！请重新选择", "提示", "好的");
                            });
                    });
                }
            }
        }
        #endregion

        #region 胚胎孵化

        private void AddHatchListener(string type,int number,int time)
        {
            GetUILogic<DataLogPanelLogic>("胚胎孵化数记录表").Show(type,time+"孵化");
            GetUILogic<DataLogPanelLogic>("胚胎孵化数记录表").SetCallBack(()=>
            {
                GetObjBehaviour("交互物体","孵化Control").SetActive(false);
                GetObjBehaviour("交互物体", "孵化52μg/L").SetActive(false);
                GetObjBehaviour("交互物体","孵化262μg/L").SetActive(false);
                GetUIBehaviour("胚胎孵化数记录表", type).Text = "20";
                NextStep();
            });
            GetUILogic<DataLogPanelLogic>("胚胎孵化数记录表").SetUnderWayTextStyle(type);
            if (type.Equals("Control"))
            {
                GetObjBehaviour("交互物体","孵化Control").SetActive(true);
                GetObjBehaviour("交互物体","孵化262μg/L").SetActive(false); 
                GetObjBehaviour("交互物体","孵化52μg/L").SetActive(false);
            }
            if (type.Equals("52μg/L"))
            {
                GetObjBehaviour("交互物体","孵化52μg/L").SetActive(true);
                GetObjBehaviour("交互物体","孵化Control").SetActive(false);
                GetObjBehaviour("交互物体","孵化262μg/L").SetActive(false);

            }
            if(type.Equals("262μg/L"))
            {
                GetObjBehaviour("交互物体","孵化262μg/L").SetActive(true);
                GetObjBehaviour("交互物体","孵化Control").SetActive(false);
                GetObjBehaviour("交互物体","孵化52μg/L").SetActive(false);
            }
            for (int i = 1; i < 21; i++)
            {
                GetObjBehaviour("交互物体", type+"孵化胚胎" + i).RemoveObjClickListener();
                GetObjBehaviour("交互物体", type+"孵化胚胎" + i).SetObjHighlight(false);
            }
            int right = FindObjectOfType<EmbryoManager>().ConfigHatch(type, number, time, EmbryoManager.EmbryoType.孵化);
            int rightCount = 0;
            for (int i = 1; i < 21; i++)
            {
                var index = i;
                GetObjBehaviour("交互物体", type+"孵化胚胎" + i).AddObjClickListener(() =>
                {
                    GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show(
                        GetObjBehaviour("交互物体", type + "孵化胚胎" + index).GetComponent<EmbryoData>().ActiveObj,
                        () =>
                        {
                            if (GetObjBehaviour("交互物体", type + "孵化胚胎" + index).GetComponent<EmbryoData>().embryoState ==
                                EmbryoData.EmbryoState.Hatch)
                            {
                                int value = int.Parse(GetUIBehaviour("胚胎孵化数记录表", type).Text) + 1;
                                rightCount = value;
                                GetUIBehaviour("胚胎孵化数记录表", type).Text = value.ToString();
                                GetObjBehaviour("交互物体", type + "孵化胚胎" + index).SetObjHighlight(true);
                                GetObjBehaviour("交互物体", type + "孵化胚胎" + index).RemoveObjClickListener();
                                if (rightCount == right)
                                {
                                    GetObjBehaviour("交互物体", "孵化262μg/L").SetActive(false);
                                    GetObjBehaviour("交互物体", "孵化52μg/L").SetActive(false);
                                    NextStep();
                                }
                            }
                            else
                                GetUILogic<WarningPanelLogic>("WarningPanel").SetWarning("该胚胎是发育迟缓请重新选择", "提示", "好的");
                        });
                });
            }
        }
        private void GStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
            GetObjBehaviour("交互物体","孵化52μg/L").GetComponent<EmbryoManager>().InitHatch();
            GetObjBehaviour("交互物体","孵化262μg/L").GetComponent<EmbryoManager>().InitHatch();
            GetObjBehaviour("交互物体","孵化Control").GetComponent<EmbryoManager>().InitHatch();
            GetObjBehaviour("交互物体", "孵化52μg/L").SetActive(false);
            GetObjBehaviour("交互物体", "孵化262μg/L").SetActive(false);
            GetObjBehaviour("交互物体","孵化Control").SetActive(false);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","GStep相机位置0");
            NextStep();
        }

        private void GStep_2()
        {
            GetObjBehaviour("交互物体", "钟4").GetChildObj("时针")
                .DOLocalRotate(new Vector3(0, 0, -360), 3, RotateMode.FastBeyond360).SetLoops(2).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "恒温培养箱5").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2)
                        .OnComplete(() =>
                            GetObjBehaviour("交互物体", "六孔培养皿6").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿6路径")
                                    .transform
                                    .GetComponentsInChildren<Transform>().Select(x => x.position).Reverse().ToArray(),
                                3)
                                .OnComplete(() =>
                                {
                                    GetObjBehaviour("交互物体", "恒温培养箱5").GetChildObj("门").localEulerAngles = Vector3.zero;
                                    GetObjLogic<LookCameraLogic>("Main Camera")
                                        .MoveLookCamera("目标位置", "GStep相机位置1", 2, NextStep);
                                }));
                });
        }

        private void GStep_3()
        {
            GetObjBehaviour("交互物体","六孔培养皿6").Scale=Vector3.zero;
            GetObjBehaviour("交互物体", "培养皿6").SetPos("目标位置", "培养皿6目标位置");
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "GStep相机位置2", 2, NextStep);
        }

        private void GStep_4()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","GStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
            "仔细观察对照组 48hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
            AddHatchListener("Control",0,48));
        }

        private void GStep_5()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 48hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
            AddHatchListener("52μg/L",0,48));
        }

        private void GStep_6()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 48hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("262μg/L", 0, 48));
        }
        
        private void GStep_7()
        {
            GetUILogic<DataLogPanelLogic>("胚胎孵化数记录表").Hide();
            GetObjBehaviour("交互物体","六孔培养皿6").Scale=Vector3.one;
            GetObjBehaviour("交互物体","六孔培养皿6").SetPos("目标位置","六孔培养皿6初始位置");
            GetObjBehaviour("交互物体","培养皿6").Position=Vector3.zero;
            GetUIBehaviour("胚胎孵化数记录表", "Control").Text = 0.ToString();
            GetUIBehaviour("胚胎孵化数记录表", "52μg/L").Text = 0.ToString();
            GetUIBehaviour("胚胎孵化数记录表", "262μg/L").Text = 0.ToString();
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置","GStep相机位置0",2,NextStep);
        }
        
        private void GStep_8()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","GStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 53hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("Control", 1, 53));
        }

        private void GStep_9()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 53hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("52μg/L", 1, 53));
        }

        private void GStep_10()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 53hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("262μg/L", 1, 53));
        }
        
        private void GStep_11()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","GStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 72hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("Control", 2, 72));
        }

        private void GStep_12()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 72hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("52μg/L", 2, 72));
        }

        private void GStep_13()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 72hpf 镜检视野中的胚胎，选择胚胎孵化个数填入右边的列表", "提示", "好的", () =>
                    AddHatchListener("262μg/L", 2, 72));
        }

        private void GStep_14()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/孵化"), "三氯生可导致斑马鱼幼鱼孵化延迟", "确定",
                () =>
                {
                    GetUILogic<DataLogPanelLogic>("胚胎孵化数记录表").Hide();
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(2, StepProgressPanelLogic.StepState.Complete);
                    NextStep();
                });
        }
        
        #endregion

        #region 对心跳频率的影响

        private int[] GetNum(int min,int max,int mean)
        {
            int[] numbers = { };
            do
            {
                numbers = Enumerable.Range(1, 20)
                    .Select(num => new System.Random(Guid.NewGuid().GetHashCode()).Next(min, max))
                    .ToArray();
            } while (numbers.Sum()!=mean);

            return numbers;
        }

        public void WaitDoAction(float time,UnityAction action)
        {
            float currentValue =0 ;
            DOTween.To(() => currentValue, x => currentValue = x,1, time).OnComplete(action.Invoke);
        }
        
        //心跳
        private void AddHeartbeatListener(string type,int time,int min,int max,int mean)
        {
            int[] nums = GetNum(min, max, mean);
            var heart=GetObjBehaviour("LargeView","胚胎").GetChildObj("心脏");
            heart.gameObject.SetActive(true);
            GetUILogic<DataLogPanelLogic>("胚胎心跳记录表").Show(type,time+"心跳");
            GetUILogic<DataLogPanelLogic>("胚胎心跳记录表").SetUnderWayTextStyle(type);
            GetUIBehaviour("LargeViewPanel","Select").Scale=Vector3.zero;
            for (int i = 1; i < 21; i++)
            {
                GetUIBehaviour("心跳次数", "心跳" + i).Text = nums[i-1].ToString();
                GetObjBehaviour("交互物体","心跳鱼"+i).GetChildObj("心跳").gameObject.SetActive(true);
                var index = i;
                GetObjBehaviour("交互物体","心跳鱼"+i).RemoveObjClickListener();
                GetObjBehaviour("交互物体","心跳鱼"+i).AddObjClickListener(() =>
                {
                    heart.DOPunchScale(new Vector3(1.0001f, 1.0001f, 1.0001f), 0.5f,1,0.1f)
                        .SetLoops(nums[index]).SetId("胚胎活").OnComplete(() =>
                        {
                            GetObjBehaviour("交互物体","心跳鱼"+index).RemoveObjClickListener();
                            GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Hide();
                            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                                "当前幼鱼心跳数：" + nums[index - 1] + "点击继续查看其它", "结果", "继续", () =>
                                {
                                    GetUIBehaviour("心跳次数", "心跳次数").Scale = Vector3.one;
                                    GetUIBehaviour("胚胎心跳记录表", type).Text = (mean/20).ToString();
                                    Debug.Log(type);
                                    Debug.Log(GetUIBehaviour("胚胎心跳记录表", type).Text);
                                    WaitDoAction(4, () =>
                                    {
                                        heart.localScale=Vector3.one;
                                        GetUIBehaviour("心跳次数", "心跳次数").Scale = Vector3.zero;
                                        NextStep();
                                    });
                                });
                        });
                    GetUILogic<LargeViewPanelLoic>("LargeViewPanel").Show("心跳", null);
                });
            }
        }
        
        private void HStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(3, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","HStep相机位置0");
            NextStep();
        }

        private void HStep_2()
        {
            GetObjBehaviour("交互物体", "钟5").GetChildObj("时针")
                .DOLocalRotate(new Vector3(0, 0, -360), 3, RotateMode.FastBeyond360).SetLoops(3).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "恒温培养箱6").GetChildObj("门").DOLocalRotate(new Vector3(0, 90, 0), 2)
                        .OnComplete(() =>
                            GetObjBehaviour("交互物体", "六孔培养皿7").transform.DOPath(GetObjBehaviour("目标位置", "六孔培养皿7路径")
                                    .transform
                                    .GetComponentsInChildren<Transform>().Select(x => x.position).Reverse().ToArray(),
                                3)
                                .OnComplete(() =>
                                {
                                    GetObjBehaviour("交互物体", "恒温培养箱6").GetChildObj("门").localEulerAngles = Vector3.zero;
                                    GetObjLogic<LookCameraLogic>("Main Camera")
                                        .MoveLookCamera("目标位置", "HStep相机位置1", 2, NextStep);
                                }));
                });
        }

        private void HStep_3()
        {
            GetObjBehaviour("交互物体", "培养皿8").SetPos("目标位置", "培养皿8目标位置");
            GetObjBehaviour("交互物体","六孔培养皿7").Scale=Vector3.zero;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "HStep相机位置2", 2, NextStep);

        }

        private void HStep_4()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","HStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 72hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("Control", 72, 24,27,500));
        }

        private void HStep_5()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 72hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("52μg/L", 72, 25,28,520));
        }

        private void HStep_6()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 72hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("262μg/L", 72, 25,28,520));
        }
        
        private void HStep_7()
        {
            GetUILogic<DataLogPanelLogic>("胚胎心跳记录表").Hide();
            GetObjBehaviour("交互物体","六孔培养皿7").SetPos("目标位置","六孔培养皿7初始位置");
            GetObjBehaviour("交互物体","六孔培养皿7").Scale=Vector3.one;
            GetObjBehaviour("交互物体","培养皿8").Position=Vector3.zero;
            GetUIBehaviour("胚胎心跳记录表", "Control").Text = 0.ToString();
            GetUIBehaviour("胚胎心跳记录表", "52μg/L").Text = 0.ToString();
            GetUIBehaviour("胚胎心跳记录表", "262μg/L").Text = 0.ToString();
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置","HStep相机位置0",2,NextStep);
        }

        private void HStep_8()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置","HStep相机位置3");
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察对照组 96hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("Control", 96, 24,27,500));
        }

        private void HStep_9()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察52微克/L 96hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("52μg/L", 96, 24,27,500));
        }

        private void HStep_10()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "仔细观察262微克/L 96hpf 镜检视野中的胚胎，统计心跳数填入右边的列表", "提示", "好的", () =>
                    AddHeartbeatListener("262μg/L", 72, 29,32,600));
        }

        private void HStep_11()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(3, StepProgressPanelLogic.StepState.Complete);
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
            GetUILogic<StepTipLogic>("StepTip").Hide();
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Hide();
            GetUILogic<DataLogPanelLogic>("胚胎心跳记录表").Hide();
            GetUILogic<MainModulePanelLogic>("MainModulePanel").Show();
            GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("三氯生对斑马鱼胚胎发育的影响",MainModulePanelLogic.ExperimentState.Complete);
            GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("TCS亚慢性暴毒的行为学实验",MainModulePanelLogic.ExperimentState.Underway);
            DestroyImmediate(GetComponent<AcuteToxicityStep>());
        }
        #endregion
    }
}

