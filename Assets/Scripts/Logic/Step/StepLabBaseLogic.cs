﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：StepLabBaseLogic
* 创建日期：2022-08-13 11:33:17
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;
using Com.Rainier.yh;
using Com.Rainier.ZC_Frame;

namespace Com.Rainier.yh
{
    /// <summary>
    /// 
    /// </summary>
	public class StepLabBaseLogic : StepLogicBase
	{
        protected void StepStart(int index)
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(index,StepProgressPanelLogic.StepState.Underway);
        }
        
        protected void StepEnd(int index)
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(index,StepProgressPanelLogic.StepState.Complete);
        }

        protected void ChangeStepState(int index,StepProgressPanelLogic.StepState state)
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(index,state);
        }
    }
}

