﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：RNA-SeqStep
* 创建日期：2022-08-13 17:35:51
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using System.Linq;
using UnityEngine.UI;

namespace Com.Rainier.yh
{
    /// <summary>
    /// 
    /// </summary>
    public class RNASeqStep : StepLogicBase
    {

        private void Start()
        {
            StartStepLogic();
        }

        /// <summary>
        /// 学习模式步骤存储
        /// </summary>
        protected override void StudyStepInput()
        {
            //步骤顺序，画外音，步骤开始时执行的方法，步骤提示，步骤触发物体，触发类型，触发后执行的方法，离开步骤前最后执行的方法
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                string.Empty, NamePair.Empty, Step.TriggerType.none, RStep_1, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_2, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_3, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_4, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_5, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_6, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_7, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_8, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_9, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_10, null);
            // step.AddStep(StepManager.StepID.orderStep, string.Empty, null, 
            //     "用酶标仪测定412 nm处的最终吸光度", new NamePair("RNAPanel","Button"), Step.TriggerType.UILight, RStep_11, null);

            #region 总RNA提取

            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "总RNA提取", NamePair.Empty, Step.TriggerType.none, SStep_1, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "在120hpf时，将0、25、125和225μg/L各组培养孔板（六孔板）中的50条幼鱼，分别加入1.5mL去酶EP管中",
                NamePair.Empty, Step.TriggerType.none, SStep_2, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "用DEPC水清洗5次，每次3min，最后一次洗净后点离20s，并置冰上吸干所有水分",
                new NamePair("交互物体", "DEPC水"), Step.TriggerType.ObjLight, SStep_3, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "用DEPC水清洗5次，每次3min，最后一次洗净后点离20s，并置冰上吸干所有水分",
                NamePair.Empty, Step.TriggerType.none, SStep_4, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "加入1000μL的Trizol", new NamePair("交互物体", "Trizol试剂"), Step.TriggerType.ObjLight, SStep_5, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "在冰上用手提式匀浆机匀浆（每次匀浆前需用DEPC水清洗两遍以避免交叉污染）", new NamePair("交互物体", "匀浆机"), Step.TriggerType.ObjLight,
                SStep_6, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "加入200μL的BCP（优化氯仿），剧烈涡旋15s，室温静置5min", new NamePair("交互物体", "氯仿试剂"), Step.TriggerType.ObjLight,
                SStep_7, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "4℃，12000g，离心15 min，液体分成上中下三层", new NamePair("交互物体", "氯仿离心机"), Step.TriggerType.ObjLight,
                SStep_8, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "移液枪转移上层清液于新1.5mL去酶EP管中", new NamePair("交互物体", "底座"), Step.TriggerType.ObjLight,
                SStep_9, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "加入等体积异丙醇，上下颠倒，轻轻混匀，室温静置10min；", new NamePair("交互物体", "异丙醇试剂"), Step.TriggerType.ObjLight,
                SStep_10, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "4℃，12000g，离心15 min，液体分成上中下三层", new NamePair("交互物体", "异丙醇离心机"), Step.TriggerType.ObjLight,
                SStep_11, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "加入预冷的DEPC水配置的75%乙醇洗涤悬浮沉淀，轻轻颠倒摇晃并静置", new NamePair("交互物体", "乙醇试剂"), Step.TriggerType.ObjLight,
                SStep_12, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "4℃，7500g，离心5min", new NamePair("交互物体", "乙醇离心机"), Step.TriggerType.ObjLight,
                SStep_13, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "弃上清留取白色沉淀，室温干燥至半透明（不能完全干燥）", new NamePair("交互物体", "乙醇移液枪"), Step.TriggerType.ObjLight,
                SStep_14, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "溶于22μL DEPC水中，置于冰上", new NamePair("交互物体", "乙醇移液枪"), Step.TriggerType.ObjLight,
                SStep_15, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "得到结果", NamePair.Empty, Step.TriggerType.none,
                SStep_16, null);

            #endregion

            #region mRNA文库构建

            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "", NamePair.Empty, Step.TriggerType.none, MStep_1, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "", NamePair.Empty, Step.TriggerType.none, MStep_2, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "", NamePair.Empty, Step.TriggerType.none, MStep_3, null);

            #endregion

            #region Illumina测序

            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "点击仪器屏幕上的Sequence按钮，开始测序", new NamePair("CeXuPanel", "Button"), Step.TriggerType.UILight, IStep_1,
                null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "放入流动槽、缓冲液盒、试剂托盘（已加入文库）", new NamePair("交互物体", "I流动槽"), Step.TriggerType.ObjLight, IStep_2, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "放入流动槽、缓冲液盒、试剂托盘（已加入文库）", new NamePair("交互物体", "I缓冲液盒"), Step.TriggerType.ObjLight, IStep_3, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "放入流动槽、缓冲液盒、试剂托盘（已加入文库）", new NamePair("交互物体", "I试剂托盘"), Step.TriggerType.ObjLight, IStep_4, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "点击屏幕上的加载按钮", new NamePair("CeXuPanel", "Button"), Step.TriggerType.UILight, IStep_5, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "加载完成点击开始按钮", new NamePair("CeXuPanel", "Button"), Step.TriggerType.UILight, IStep_6, null);
            step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                "点击屏幕上的RNA-seq，选择点击启动", new NamePair("CeXuPanel", "Button"), Step.TriggerType.UILight, IStep_7, null);

            #endregion
        }

        /// <summary>
        /// 考核模式步骤存储
        /// </summary>
        protected override void ExamStepInput()
        {

        }

        /// <summary>
        /// 考核模式显示考核提示
        /// </summary>
        /// <param name="triggerType">触发类型</param>
        /// <param name="namePair">触发物体</param>
        protected override void ShowExamTip(Step.TriggerType triggerType, NamePair namePair)
        {

        }

        /// <summary>
        /// 考核模式隐藏考核提示
        /// </summary>
        /// <param name="triggerType">触发类型</param>
        /// <param name="namePair">触发物体</param>
        protected override void HideExamTip(Step.TriggerType triggerType, NamePair namePair)
        {

        }


        private void RStep_1()
        {
            GetObjBehaviour("目标位置", "转录组测序").SetActive(true);
            GetObjBehaviour("交互物体", "转录组测序").SetActive(true);
            GetObjBehaviour("目标位置", "原理介绍").SetActive(true);
            GetObjBehaviour("交互物体", "原理介绍").SetActive(true);
            ChangeButtonImage("板布局", new Vector3(-207, 90, 0), new Vector2(106, 10));
            GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/1");
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "R相机位置1", 2, () =>
            {
                GetUILogic<VideoPanelLogic>("VideoPanel").LoadVideoAndPlay("转录组测序原理");
                GetUILogic<VideoPanelLogic>("VideoPanel").SetEndAction(NextStep);
            });
        }

        // private void RStep_2()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/2");
        //     ChangeButtonImage("操作步骤",new Vector3(-207,80,0),new Vector2(106,10));
        //     NextStep();
        // }
        //
        // private void RStep_3()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/3");
        //     ChangeButtonImage("吸光度",new Vector3(-269,141,0),new Vector2(20,29));
        //     NextStep();
        // }
        //
        // private void RStep_4()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/4");
        //     ChangeButtonImage("吸光度输入框",new Vector3(-70,70,0),new Vector2(66,12));
        //     NextStep();
        // }
        //
        // private void RStep_5()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/5");
        //     ChangeButtonImage("吸光度1",new Vector3(-201,52,0),new Vector2(99,8));
        //     NextStep();
        // }
        //
        // private void RStep_6()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/6");
        //     ChangeButtonImage("按钮2",new Vector3(-252,-159,0),new Vector2(28,13));
        //     NextStep();
        // }
        //
        // private void RStep_7()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/8");
        //     ChangeButtonImage("温度",new Vector3(-249,-128,0),new Vector2(31,9));
        //     NextStep();
        // }
        //
        // private void RStep_8()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/10");
        //     ChangeButtonImage("确定",new Vector3(-214,-152,0),new Vector2(20,9));
        //     NextStep();
        // }
        //
        // private void RStep_9()
        // {
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/11");
        //     ChangeButtonImage("按钮1",new Vector3(-209,-143,0),new Vector2(111,19));
        //     NextStep();
        // }
        //
        // private void RStep_10()
        // {
        //     GetUIBehaviour("RNAPanel", "Button").Scale=Vector3.zero;
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/7");
        //     WaitTimeDoFunc(3, () =>
        //     {
        //         GetUIBehaviour("RNAPanel", "Button").Scale=Vector3.one;
        //         GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/12");
        //         ChangeButtonImage("保存",new Vector3(-73,0,0),new Vector2(38,10));
        //         NextStep();
        //     });
        // }

        // private void RStep_11()
        // {
        //     GetUIBehaviour("RNAPanel", "Button").Scale=Vector3.zero;
        //     GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/13");
        //     WaitTimeDoFunc(3,()=> GetUIBehaviour("RNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/14"));
        //     NextStep();
        // }
        private void ChangeButtonImage(string path, Vector3 pos, Vector2 size)
        {
            GetUIBehaviour("RNAPanel", "Button").SetButtonActive(false);
            GetUIBehaviour("RNAPanel", "Button").GetComponent<RectTransform>().sizeDelta = size;
            GetUIBehaviour("RNAPanel", "Button").LocalPosition = pos;
            GetUIBehaviour("RNAPanel", "Button").ChangeImageSprite(Resources.Load<Sprite>("Image/酶标仪切图/" + path));
            Sprite sprite = Resources.Load<Sprite>("Image/酶标仪切图/" + path + "点击");
            GetUIBehaviour("RNAPanel", "Button").ChangeButtonClickSprite(sprite,
                sprite, sprite, null);
            GetUIBehaviour("RNAPanel", "Button").SetButtonActive(true);
            GetUIBehaviour("RNAPanel", "Button").RemoveButtonListener();
        }

        #region 总RNA提取

        private void SStep_1()
        {
            string[] names = {"Trizol", "氯仿", "异丙醇", "乙醇", "测定"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "SStep相机位置1");
            NextStep();
        }

        private void SStep_2()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SStep相机位置2", 1.5f, () =>
            {
                for (int i = 1; i < 4; i++)
                {
                    GetObjBehaviour("交互物体", "EP" + i).GetChildObj("幼鱼").localScale = Vector3.one;
                    GetUILogic<SmallTipsInfoLogic>("SmallTips").SetContent("各浓度EP管都已加入50条幼鱼");
                    WaitTimeDoFunc(3, () =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "SStep相机位置3");
                        GetUILogic<SmallTipsInfoLogic>("SmallTips").Switch(false);
                        NextStep();
                    });
                }
            });
        }

        private void SStep_3()
        {
            GetObjBehaviour("交互物体", "DEPC水").MoveToPos("目标位置", "DEPC水位置", 1.5f, () =>
            {
                WaitTimeDoFunc(2, () =>
                {
                    GetObjBehaviour("交互物体", "DEPC水").SetPos("目标位置", "DEPC水初始位置");
                    GetObjBehaviour("交互物体", "EP1").MoveToPos("目标位置", "EP管倒水位置", 1.5f, () =>
                    {
                        WaitTimeDoFunc(2, () =>
                        {
                            GetObjBehaviour("交互物体", "EP1").SetPos("目标位置", "EP1初始位置");
                            NextStep();
                        });
                    });
                });
            });
        }

        private void SStep_4()
        {
            GetObjBehaviour("交互物体", "DEPC水").MoveToPos("目标位置", "DEPC水位置1", 1.5f, () =>
            {
                WaitTimeDoFunc(2, () =>
                {
                    GetObjBehaviour("交互物体", "DEPC水").SetPos("目标位置", "DEPC水初始位置");
                    GetObjBehaviour("交互物体", "EP2").MoveToPos("目标位置", "EP管倒水位置", 1.5f, () =>
                    {
                        WaitTimeDoFunc(2, () =>
                        {
                            GetObjBehaviour("交互物体", "EP2").SetPos("目标位置", "EP2初始位置");
                            NextStep();
                        });
                    });
                });
            });
        }

        private void SStep_5()
        {
            GetObjBehaviour("交互物体", "Trizol试剂").GetChildObj("盖子").localScale = Vector3.zero;
            GetUILogic<SmallTipsInfoLogic>("SmallTips").SetContent("Trizol的作用：裂解细胞");
            GetObjBehaviour("交互物体", "移液枪12").transform.DOPath(GetObjBehaviour("目标位置", "RNA移液枪路径")
                        .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10,
                    PathType.Linear, PathMode.Full3D, 50)
                .OnComplete(NextStep);
        }

        private void SStep_6()
        {
            GetUILogic<SmallTipsInfoLogic>("SmallTips").Switch(false);
            GetObjBehaviour("交互物体", "移液枪12").SetPos("目标位置", "移液枪12清洗");
            GetObjBehaviour("交互物体", "匀浆机").SetPos("目标位置", "匀浆机清洗");
            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(2);
            sequence.Append(GetObjBehaviour("Main Camera", "Main Camera").transform
                .DOMove(GetObjBehaviour("目标位置", "SStep相机位置4").Position, 2));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "匀浆机").SetPos("目标位置", "匀浆机冰箱"));
            sequence.AppendInterval(1);
            sequence.Append(GetObjBehaviour("交互物体", "匀浆机").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.01f, 0), 1));
            sequence.AppendInterval(2);
            sequence.Append(
                GetObjBehaviour("交互物体", "匀浆机").transform.DOBlendableLocalMoveBy(new Vector3(0, 0.01f, 0), 1));
            sequence.Append(GetObjBehaviour("交互物体", "匀浆机").transform
                .DOBlendableLocalMoveBy(new Vector3(0.016f, 0, 0), 1));
            sequence.Append(GetObjBehaviour("交互物体", "匀浆机").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.01f, 0), 1));
            sequence.AppendInterval(2);
            sequence.Append(
                GetObjBehaviour("交互物体", "匀浆机").transform.DOBlendableLocalMoveBy(new Vector3(0, 0.01f, 0), 1));
            sequence.Append(GetObjBehaviour("交互物体", "匀浆机").transform
                .DOBlendableLocalMoveBy(new Vector3(0.016f, 0, 0), 1));
            sequence.Append(GetObjBehaviour("交互物体", "匀浆机").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.01f, 0), 1));
            sequence.AppendInterval(2);
            sequence.OnComplete(() =>
            {
                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "SStep相机位置2");
                for (int i = 1; i < 4; i++)
                {
                    GetObjBehaviour("交互物体", "EP" + i).GetChildObj("溶液1").localScale = Vector3.zero;
                    GetObjBehaviour("交互物体", "EP" + i).GetChildObj("溶液2").localScale = Vector3.one;
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
                    WaitTimeDoFunc(3, () =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "氯仿相机位置1");
                        NextStep();
                    });
                }
            });
        }

        private void SStep_7()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "氯仿相机位置1");
            GetObjBehaviour("交互物体", "氯仿试剂").GetChildObj("盖子").localScale = Vector3.zero;
            GetObjBehaviour("交互物体", "氯仿移液枪").transform.DOPath(GetObjBehaviour("目标位置", "氯仿移液枪路径")
                .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "氯仿移液枪").MoveToPos("目标位置", "氯仿移液枪初始位置", 1, () =>
                {
                    for (int i = 1; i < 4; i++)
                    {
                        GetObjBehaviour("交互物体", "氯仿EP" + i).MoveToPos("目标位置", "氯仿EP位置" + i, 2, null);
                    }

                    WaitTimeDoFunc(6, () =>
                    {
                        GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("室温静置5min", "提示", "确定", () =>
                        {
                            for (int i = 1; i < 4; i++)
                            {
                                GetObjBehaviour("交互物体", "氯仿EP" + i).MoveToPos("目标位置", "氯仿EP" + i + "位置", 2, null);
                                WaitTimeDoFunc(5, () =>
                                {
                                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "氯仿相机位置2");
                                    NextStep();
                                });
                            }
                        });
                    });
                });
            });
        }

        private void SStep_8()
        {
            GetUILogic<SmallTipsInfoLogic>("SmallTips").SetContent("4℃，12000g，离心15 min");
            GetObjBehaviour("交互物体", "氯仿EP4").Scale = Vector3.one;
            for (int i = 1; i < 5; i++)
            {
                GetObjBehaviour("交互物体", "氯仿EP" + i).transform.DOMove(GetObjBehaviour("目标位置", "离心EP" + i).Position, 2);
            }

            WaitTimeDoFunc(3, () => GetObjBehaviour("交互物体", "氯仿离心机").GetChildObj("核").DOLocalMoveZ(0.16f, 1.5f)
                .OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "氯仿离心机").GetChildObj("盖子1").gameObject.SetActive(false);
                    GetObjBehaviour("交互物体", "氯仿离心机").GetChildObj("盖子2").gameObject.SetActive(true);
                    WaitTimeDoFunc(4, () =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "氯仿相机位置3");
                        for (int i = 1; i < 4; i++)
                        {
                            GetObjBehaviour("交互物体", "氯仿EP" + i).SetPos("目标位置", "氯仿EP" + i + "位置");
                            GetObjBehaviour("交互物体", "氯仿EP" + i).GetChildObj("溶液2").localScale = Vector3.zero;
                            GetObjBehaviour("交互物体", "氯仿EP" + i).GetChildObj("溶液3").localScale = Vector3.one;
                            GetUILogic<SmallTipsInfoLogic>("SmallTips").Switch(false);
                            WaitTimeDoFunc(3, () =>
                            {
                                GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                                    .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                                GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                                    .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
                                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "异丙醇相机位置1");
                                NextStep();
                            });
                        }
                    });
                })
            );
        }

        private void SStep_9()
        {
            GetObjBehaviour("交互物体", "异丙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "异丙醇移液枪路径")
                    .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10)
                .OnComplete(NextStep);
        }

        private void SStep_10()
        {
            GetObjBehaviour("交互物体", "异丙醇试剂").GetChildObj("盖子").localScale=Vector3.zero;
            GetObjBehaviour("交互物体", "异丙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "异丙醇移液枪路径1")
                    .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 13)
                .OnComplete(() =>
                {
                    for (int i = 4; i < 7; i++)
                    {
                        GetObjBehaviour("交互物体", "异丙醇EP" + i).transform.DOLocalMoveY(0.7f,1);
                    }
                    WaitTimeDoFunc(1, () =>
                    {
                        for (int i = 4; i < 7; i++)
                        {
                            GetObjBehaviour("交互物体", "异丙醇EP" + i).transform.DOLocalRotate(new Vector3(360,0,0), 2);
                        }
                        WaitTimeDoFunc(2, () =>
                        {
                            for (int i = 4; i < 7; i++)
                            {
                                GetObjBehaviour("交互物体", "异丙醇EP" + i).transform.localEulerAngles=new Vector3(-90,0,0);
                                GetObjBehaviour("交互物体", "异丙醇EP" + i).transform.DOLocalMoveY(0.633f,1);
                            }
                            WaitTimeDoFunc(3, () =>
                            {
                                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "异丙醇相机位置2");
                                NextStep();
                            });
                        });
                    });
                });
        }

        private void SStep_11()
        {
            GetUILogic<SmallTipsInfoLogic>("SmallTips").SetContent("异丙醇的作用：洗涤RNA");
            GetObjBehaviour("交互物体", "异丙醇离心机").GetChildObj("核").DOLocalMoveZ(0.16f, 1.5f).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "异丙醇离心机").GetChildObj("盖子1").gameObject.SetActive(false);
                GetObjBehaviour("交互物体", "异丙醇离心机").GetChildObj("盖子2").gameObject.SetActive(true);
                WaitTimeDoFunc(4, () =>
                {
                    for (int i = 4; i < 7; i++)
                    {
                        GetObjBehaviour("交互物体", "异丙醇EP" + i).GetChildObj("溶液1").localScale = Vector3.zero;
                        GetObjBehaviour("交互物体", "异丙醇EP" + i).GetChildObj("溶液4").localScale = Vector3.one;
                    }
                    WaitTimeDoFunc(3, () =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "异丙醇相机位置3");
                        GetObjBehaviour("交互物体", "异丙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "异丙醇移液枪路径2")
                                .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10)
                            .OnComplete(
                                () =>
                                {
                                    WaitTimeDoFunc(1, () =>
                                    {
                                        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                                            .ChangeState(2, StepProgressPanelLogic.StepState.Complete);
                                        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                                            .ChangeState(3, StepProgressPanelLogic.StepState.Underway);
                                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "乙醇相机位置1");
                                        GetUILogic<SmallTipsInfoLogic>("SmallTips").Switch(false);
                                        NextStep();
                                    });
                                });
                    });
                });
            });
        }

        private void SStep_12()
        {
            GetObjBehaviour("交互物体", "乙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "乙醇移液枪路径")
                .transform.GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 12).OnComplete(() =>
            {
                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "乙醇相机位置2");
                for (int i = 1; i < 4; i++)
                {
                    GetObjBehaviour("交互物体", "乙醇EP" + i).transform.DOLocalMoveY(0.7f, 1);
                }
                WaitTimeDoFunc(2, () =>
                {
                    for (int i = 1; i < 4; i++)
                    {
                        var index = i;
                        GetObjBehaviour("交互物体", "乙醇EP" + i).transform.DOBlendableLocalRotateBy(new Vector3(-30,0,0), 1)
                            .OnComplete(()=>GetObjBehaviour("交互物体", "乙醇EP" + index).transform.DOBlendableLocalRotateBy(new Vector3(30,0,0), 1));
                    }
                    WaitTimeDoFunc(4, () =>
                    {
                        for (int i = 1; i < 4; i++)
                        {
                            GetObjBehaviour("交互物体", "乙醇EP" + i).GetChildObj("溶液4").localScale=Vector3.zero;
                            GetObjBehaviour("交互物体", "乙醇EP" + i).GetChildObj("溶液1").localScale=Vector3.one;
                        }
                        WaitTimeDoFunc(2, () =>
                        {
                            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "乙醇相机位置3");
                            for (int i = 1; i < 4; i++)
                            {
                                GetObjBehaviour("交互物体", "乙醇EP" + i).transform.DOLocalMoveY(0.633f, 1);
                            }
                            NextStep();
                        });
                    });
                });
            });
        }

        private void SStep_13()
        {
            GetObjBehaviour("交互物体", "乙醇离心机").GetChildObj("核").DOLocalMoveZ(0.16f, 1.5f).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "乙醇离心机").GetChildObj("盖子1").gameObject.SetActive(false);
                GetObjBehaviour("交互物体", "乙醇离心机").GetChildObj("盖子2").gameObject.SetActive(true);
                WaitTimeDoFunc(3, () =>
                {
                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "乙醇相机位置1");
                    for (int i = 1; i < 4; i++)
                    {
                        GetObjBehaviour("交互物体", "乙醇EP" + i).GetChildObj("溶液4").localScale=Vector3.one;
                        GetObjBehaviour("交互物体", "乙醇EP" + i).GetChildObj("溶液1").localScale=Vector3.zero;
                    }
                    NextStep();
                });
            });

        }

        private void SStep_14()
        {
            GetObjBehaviour("交互物体", "乙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "乙醇移液枪路径2")
                .transform.GetComponentsInChildren<Transform>().Select(x => x.position)
                .ToArray(), 10).OnComplete(NextStep);
        }

        private void SStep_15()
        {
            GetObjBehaviour("交互物体", "乙醇移液枪").transform.DOPath(GetObjBehaviour("目标位置", "乙醇移液枪路径3")
                .transform.GetComponentsInChildren<Transform>().Select(x => x.position)
                .ToArray(), 10).OnComplete(() =>
            {
                WaitTimeDoFunc(2, () =>
                {
                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "乙醇相机位置4");
                    for (int i = 1; i < 4; i++)
                    {
                        GetObjBehaviour("交互物体", "乙醇EP" + i).Scale=Vector3.zero;
                    }
                    GetObjBehaviour("交互物体", "乙醇EP对照").Scale=Vector3.one;
                    WaitTimeDoFunc(3, () =>
                    {
                        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                            .ChangeState(3, StepProgressPanelLogic.StepState.Complete);
                        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                            .ChangeState(4, StepProgressPanelLogic.StepState.Underway);
                        NextStep();
                    });
                });
            });
        }

        private void SStep_16()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel")
                .SetWarningThenDoFunc(Resources.Load<Sprite>("Image/结果"), "结果", "确定", NextStep);
        }
    #endregion
        #region mRNA文库构建
        private void MStep_1()
        {
            GetUILogic<StepTipLogic>("StepTip").Hide();
            string[] names = { "mRNA 片段化", "反转录", "末端修复", "末端 dA-Tailing", "接头连接", "纯化分选","文库扩增" };
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            //GetUILogic<WarningPanelLogic>("WarningPanel").GetUIBehaviours("WarningPanelBG").Scale = new Vector3(1.5f, 1.5f, 1f);
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/mRNA文库构建/1.RNA片段化"),
               "RNA片段化", "下一步", () =>
               {
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(3, StepProgressPanelLogic.StepState.Underway);
                   NextStep();
               });
        }
        private void MStep_2()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/mRNA文库构建/2.反转录-末端修复"),
               "反转录-末端修复", "下一步", () =>
               {
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(2, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(3, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(4, StepProgressPanelLogic.StepState.Underway);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(5, StepProgressPanelLogic.StepState.Underway);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(6, StepProgressPanelLogic.StepState.Underway);
                   NextStep();
               });
        }

        private void MStep_3()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/mRNA文库构建/3.接头连接-文库扩增"),
               "接头连接-文库扩增", "完成", () =>
               {
                   GetUILogic<WarningPanelLogic>("WarningPanel").GetUIBehaviours("WarningPanelBG").Scale = Vector3.one;
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(4, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(5, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(6, StepProgressPanelLogic.StepState.Complete);

                   foreach (Transform items in GetUILogic<StepProgressPanelLogic>("StepProgressPanel").GetUIBehaviours("Progress").transform)
                   {
                       Destroy(items.gameObject);
                   }

                   string[] names = { "总RNA的提取", "mRNA文库构建", "Illumina测序" };
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
                   GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                       .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
                   GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
                   GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "I相机位置近");
                       NextStep();
               });
        }
        #endregion

        #region Illumina测序
        private void IStep_1()
        {
            GetUIBehaviour("CeXuPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/测序仪器界面/2");
            GetUIBehaviour("CeXuPanel", "Button").Scale = Vector3.zero;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "I相机位置远", 2, () =>
            {
                GetUIBehaviour("CeXuPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/测序仪器界面/3");
                NextStep();
            });
        }
        private void IStep_2()
        {
            GetObjBehaviour("交互物体", "I流动槽").transform
               .DOPath(GetObjBehaviour("目标位置", "I流动槽Path")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(() => {
                   GetObjBehaviour("交互物体", "I测序仪器门1").transform.DOLocalMoveX(0.058f, 1).OnComplete(NextStep);
               });
        }
        private void IStep_3()
        {
            GetObjBehaviour("交互物体", "I测序仪器门2").transform.DOLocalRotate(new Vector3(0,0,-140), 1).OnComplete(()=> { 
                 GetObjBehaviour("交互物体", "I缓冲液盒").transform
                .DOPath(GetObjBehaviour("目标位置", "I缓冲液盒Path")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(() =>
                 {
                     GetObjBehaviour("交互物体", "I测序仪器门2").transform.DOLocalRotate(Vector3.zero, 1).OnComplete(NextStep);
                 });
            });

        }
        private void IStep_4()
        {
            GetObjBehaviour("交互物体", "I测序仪器门3").transform.DOLocalRotate(new Vector3(0, 0, 120), 1).OnComplete(() => {
                GetObjBehaviour("交互物体", "I试剂托盘").transform
               .DOPath(GetObjBehaviour("目标位置", "I试剂托盘Path")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(() =>
               {
                   GetObjBehaviour("交互物体", "I测序仪器门3").transform.DOLocalRotate(Vector3.zero, 1).OnComplete(()=> {
                       GetUIBehaviour("CeXuPanel", "Button").Scale = Vector3.one;
                       ChangeButtonImage1("2", new Vector3(68.6F, -31.58F, 0), new Vector2(40F, 12.288F));
                       GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "I相机位置近", 2, () =>
                       {
                           NextStep();
                       });
                   });
               });
            });
        }
        private void IStep_5()
        {
            GetUIBehaviour("CeXuPanel", "Button1").GetComponent<Image>().enabled = true;
            ChangeButtonImage1("3", new Vector3(85.86F, -53.2F, 0), new Vector2(33.54F, 12.93F));
            NextStep();
        }
        private void IStep_6()
        {
            GetUIBehaviour("CeXuPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/测序仪器界面/4");
            GetUIBehaviour("CeXuPanel", "Button1").GetComponent<Image>().enabled = false;
            ChangeButtonImage1("4", new Vector3(85.86F, -53.2F, 0), new Vector2(33.54F, 12.93F));
            NextStep();
        }
        private void IStep_7()
        {
            NextStep();
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("您已完成全部实验,点击确认返回","提示","好的",()=>GetUILogic<LoadSceneLogic>("LoadScene").LoadSceneAsync("StartScene"));
        }
        private void ChangeButtonImage1(string path, Vector3 pos, Vector2 size)
        {
            GetUIBehaviour("CeXuPanel", "Button").SetButtonActive(false);
            GetUIBehaviour("CeXuPanel", "Button").GetComponent<RectTransform>().sizeDelta = size;
            GetUIBehaviour("CeXuPanel", "Button").LocalPosition = pos;
            GetUIBehaviour("CeXuPanel", "Button").ChangeImageSprite(Resources.Load<Sprite>("Image/测序仪器切图/" + path));
            Sprite sprite = Resources.Load<Sprite>("Image/测序仪器切图/" + path + "点击");
            GetUIBehaviour("CeXuPanel", "Button").ChangeButtonClickSprite(sprite,
                sprite, sprite, null);
            GetUIBehaviour("CeXuPanel", "Button").SetButtonActive(true);
            GetUIBehaviour("CeXuPanel", "Button").RemoveButtonListener();
        }
        #endregion
    }
}

