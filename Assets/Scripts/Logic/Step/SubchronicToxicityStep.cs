﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称：SubchronicToxicityStep
* 创建日期：2022-08-08 15:31:24
* 作者名称：
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using System.Collections.Generic;

namespace Com.Rainier.yh
{
    /// <summary>
    /// 亚慢性毒性
    /// </summary>

    public class SubchronicToxicityStep : StepLogicBase
    {
        protected override void Start()
        {
            base.Start();
            StartStepLogic();
        }


        public void Init()
        {
            base.Start();
            step.RemoveAllStep();
            StartStepLogic();
        }

        /// <summary>
        /// 学习模式步骤存储
        /// </summary>
        protected override void StudyStepInput()
        {
            print(CurrentExperiment);
            if (CurrentExperiment=="TCS亚慢性暴毒的行为学实验")
            {
                #region 暴毒处理
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    string.Empty, NamePair.Empty, Step.TriggerType.none, SStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从冰箱内拿出已配制好的250微克/升的三氯生斑马鱼培养液", new NamePair("交互物体", "冰箱"),
                    Step.TriggerType.ObjLight, SStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将250250微克/升的三氯生斑马鱼培养液250倒入鱼缸，放入10条3个月的斑马鱼，标记三氯生处理。", new NamePair("交互物体", "三氯生溶液250"),
                    Step.TriggerType.ObjLight, SStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "另取一鱼缸，倒入清水，放入10条3个月的斑马鱼，标记为对照。", new NamePair("交互物体", "普通缸1"),
                    Step.TriggerType.ObjLight, SStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "暴毒一个月", NamePair.Empty,
                    Step.TriggerType.none, SStep_5, null);
                
                #endregion
                
                #region 栖底实验
                
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty,
                    Step.TriggerType.none, TStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从对照缸里取1条鱼放到梯形鱼缸里，将梯形鱼缸里的水加到最高水位。", new NamePair("交互物体", "梯型缸1"),
                    Step.TriggerType.ObjLight, TStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从三氯生处理缸里取1条鱼放到梯形鱼缸里，将梯形鱼缸里的水加到最高水位。", new NamePair("交互物体", "梯型缸2"),
                    Step.TriggerType.ObjLight, TStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "分别将对照和处理梯形鱼缸放入隔音箱，在电脑软件上设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", new NamePair("交互物体", "隔音箱"),
                    Step.TriggerType.ObjLight, TStep_4, null);
                
                #endregion

                #region 社交实验

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty,
                    Step.TriggerType.none, SJStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在社交缸的左边社交区域里放置3对雌雄斑马鱼，非社交区域无鱼", NamePair.Empty,
                    Step.TriggerType.none, SJStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从对照缸中取1条鱼，放入社交缸中间区域", NamePair.Empty,
                    Step.TriggerType.none, SJStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从处理缸中取1条鱼，放入社交缸中间区域", NamePair.Empty,
                    Step.TriggerType.none, SJStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将对照社交缸放入隔音箱，在电脑软件上设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", NamePair.Empty,
                    Step.TriggerType.none, SJStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "录像结果显示", NamePair.Empty,
                    Step.TriggerType.none, SJStep_6, null);

                #endregion

                #region 迷宫实验

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty,
                    Step.TriggerType.none, MGStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "对斑马鱼进行15天的训练，分别将对照缸里和处理缸内的鱼逐条取出，单独训练：将鱼放入T形缸s区。", NamePair.Empty,
                    Step.TriggerType.none, MGStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "撤掉T形缸“s”区的挡板，同时在r区喂食，并用小木锤在W区敲打", NamePair.Empty,
                    Step.TriggerType.none, MGStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "训练结束，开始实验：将T形缸放入隔音箱", NamePair.Empty,
                    Step.TriggerType.none, MGStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将斑马鱼放在隔音箱内T形缸的上方区域（“s”区），迅速取出“s”区的挡板", NamePair.Empty,
                    Step.TriggerType.none, MGStep_5, null);
                // step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                //     "在电脑软件上设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", NamePair.Empty,
                //     Step.TriggerType.none, MGStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "录像结果显示", NamePair.Empty,
                    Step.TriggerType.none, MGStep_7, null);

                #endregion

                #region 位置偏好实验

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在试验水缸底部放置一半为浅棕色，另一部分为白色（两个黑点放在白色中间）的纸板，将试验水缸分成相等的两部分", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从对照缸中取1条鱼，放到试验水缸里（水深3cm)", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "从处理缸中取1条鱼，放到试验水缸里（水深3cm)", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将水缸放入隔音箱，在电脑软件上设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "录像结果显示", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "录像结果显示", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "录像结果显示", NamePair.Empty,
                    Step.TriggerType.none, WZPHStep_8, null);

                #endregion
            }
            else
            {
                #region 斑马鱼成鱼结构认知

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    String.Empty, NamePair.Empty,
                    Step.TriggerType.none, XStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "请点击选择斑马鱼大脑", new NamePair("斑马鱼全身", "大脑"),
                    Step.TriggerType.ObjLight, XStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "点击选择斑马鱼小脑，查看斑马鱼中枢神经系统介绍", new NamePair("斑马鱼全身", "小脑"),
                    Step.TriggerType.ObjLight, XStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "查看完毕，进入斑马鱼脑解剖实验", NamePair.Empty,
                    Step.TriggerType.none, XStep_4, null);

                #endregion

                #region 斑马鱼脑解剖

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "点击播放按钮查看斑马鱼脑解剖过程", NamePair.Empty,
                    Step.TriggerType.none, YStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "取对照处理斑马鱼解剖出的脑组织0.1g放入EP管内，加入1 mL PBS溶液，置于冰盒中用匀浆器匀浆", new NamePair("交互物体", "Y_PBS"),
                    Step.TriggerType.ObjLight, YStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "取三氯生处理斑马鱼解剖出的脑组织0.1g放入EP管内，加入1 mL PBS溶液，置于冰盒中用匀浆器匀浆", new NamePair("交互物体", "Y_PBS"),
                    Step.TriggerType.ObjLight, YStep_2_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将EP管置于离心机中，14000 rpm 4 ℃下离心5 min，", new NamePair("交互物体", "Y离心机"),
                    Step.TriggerType.ObjLight, YStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "吸取上清液置于冰盒上待测。", new NamePair("交互物体", "Y_EP1"),
                    Step.TriggerType.ObjLight, YStep_4, null);

                #endregion

                #region 酶活性的测定
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "AChE检测试剂盒包括：检测工作液、校准液。", new NamePair("交互物体", "ZAChE检测试剂盒"),
                    Step.TriggerType.ObjLight, ZStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "加样", new NamePair("交互物体", "Z96孔板"),
                    Step.TriggerType.ObjLight, ZStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在a标记处即空白组: 每孔分别加200 μL超纯水", new NamePair("交互物体", "Z超纯水"),
                    Step.TriggerType.ObjLight, ZStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在b标记处即校准组: 每孔分别加200μL校准液", new NamePair("交互物体", "Z校准液"),
                    Step.TriggerType.ObjLight, ZStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在c标记处即实验组1: 每孔分别加10μL对照组脑组织匀浆液", new NamePair("交互物体", "Z_EP对照脑"),
                    Step.TriggerType.ObjLight, ZStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在c标记处即实验组1: 每孔分别加190μL工作液", new NamePair("交互物体", "Z工作液"),
                    Step.TriggerType.ObjLight, ZStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在d标记处即实验组2: 每孔分别加10μL三氯生组脑组织匀浆液", new NamePair("交互物体", "Z_EP三氯生脑"),
                    Step.TriggerType.ObjLight, ZStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "在d标记处即实验组2: 每孔分别加190μL工作液", new NamePair("交互物体", "Z工作液"),
                    Step.TriggerType.ObjLight, ZStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将96孔板的盖子盖上,然后在室温下孵育2分钟", new NamePair("交互物体", "Z96孔板"),
                    Step.TriggerType.ObjLight, ZStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "将96孔板放入酶标仪", new NamePair("交互物体", "Z酶标仪"),
                    Step.TriggerType.ObjLight, ZStep_10, null);

                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                   "将96孔板放入酶标仪", new NamePair("ZRNAPanel", "Content"),
                   Step.TriggerType.UILight, RStep_1, null);
                //step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                //   "将96孔板放入酶标仪", NamePair.Empty,
                //   Step.TriggerType.none, RStep_1, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_2, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_3, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_4, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_5, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_6, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_7, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_8, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_9, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_10, null);
                step.AddStep(StepManager.StepID.orderStep, string.Empty, null,
                    "用酶标仪测定412 nm处的最终吸光度", new NamePair("ZRNAPanel", "Button"), Step.TriggerType.UILight, RStep_11, null);

                #endregion
            }
        }


        #region 暴毒处理

        /// <summary>
        /// 初始配置
        /// </summary>
        private void SStep_1()
        {
            string[] names = {"亚慢性暴毒处理", "栖底实验", "社交实验", "T型迷宫实验", "位置偏好实验"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "SStep_1目标位置");
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().nearClipPlane = 0.3f;
            GetObjBehaviour("交互物体", "斑马鱼组").Scale = Vector3.zero;
            GetObjBehaviour("交互物体", "幼鱼组").Scale = Vector3.zero;
            GetObjBehaviour("交互物体", "幼鱼").Scale = Vector3.zero;
            GetObjBehaviour("交互物体", "雄鱼").Scale = Vector3.zero;
            NextStep();
        }

        private void SStep_2()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "冰箱").GetChildObj("门").DOLocalRotate(new Vector3(0, -90, 0), 1.5f));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "冰箱").Collider().enabled = false;
                GetObjBehaviour("交互物体", "三氯生溶液250").SetObjHighlight(true);
                GetObjBehaviour("交互物体", "三氯生溶液250").AddObjClickListener(() =>
                {
                    GetObjBehaviour("交互物体", "冰箱").GetChildObj("门").localEulerAngles = Vector3.zero;
                    GetObjBehaviour("交互物体", "三氯生溶液250").SetObjHighlight(false);
                    GetObjBehaviour("交互物体", "三氯生溶液250").SetPos("目标位置", "三氯生溶液目标位置");
                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "SStep_2目标位置");
                    GetObjBehaviour("交互物体", "三氯生溶液250").RemoveObjClickListener();
                    NextStep();
                });
            });
        }

        private void SStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "三氯生溶液250").transform
                .DOLocalMove(GetObjBehaviour("目标位置", "三氯生溶液目标位置2").Position, 1.5f));
            sequence.Join(GetObjBehaviour("交互物体", "三氯生溶液250").transform
                .DOLocalRotate(GetObjBehaviour("目标位置", "三氯生溶液目标位置2").Rotation, 1.5f));
            sequence.AppendCallback(() =>
                GetObjBehaviour("交互物体", "三氯生溶液250").GetChildObj("瓶盖").transform.localScale = Vector3.zero);
            sequence.Append(GetObjBehaviour("交互物体", "三氯生溶液250").GetChildObj("溶液").transform.DOScaleY(0.01f, 1.5f));
            sequence.Join(GetObjBehaviour("交互物体", "普通缸2").GetChildObj("水").DOScaleZ(1, 1.5f));
            sequence.Join(GetObjBehaviour("交互物体", "三氯生溶液250").GetChildObj("溶液").transform.DOLocalMoveY(0.01f, 1.5f));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "斑马鱼组").Scale = Vector3.one;
                GetObjBehaviour("交互物体", "三氯生溶液250").Scale = Vector3.zero;
            });
            sequence.Append(GetObjBehaviour("交互物体", "斑马鱼组").transform
                .DOBlendableLocalMoveBy(new Vector3(0, -0.2f, 0), 2));
            sequence.OnComplete(NextStep);
        }

        private void SStep_4()
        {
            GetObjBehaviour("交互物体", "普通缸1").GetChildObj("水").DOScaleZ(1, 2).OnComplete(() =>
            {
                GetObjBehaviour("交互物体", "幼鱼组").Scale = Vector3.one;
                GetObjBehaviour("交互物体", "幼鱼组").transform.DOBlendableLocalMoveBy(new Vector3(0, -0.2f, 0), 1)
                    .OnComplete(NextStep);
            });

        }

        private void SStep_5()
        {
            WaitTimeDoFunc(2, () => GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("一个月后", "提示",
                "完成",
                () =>
                {
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                    NextStep();
                }));
        }

        #endregion

        #region 栖底实验

        private void TStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "TStep相机位置1", 1.5f, NextStep);
        }

        private void TStep_2()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "梯型缸1").GetChildObj("水").transform.DOScaleZ(1, 2));
            sequence.Join(GetObjBehaviour("交互物体", "梯型缸1").GetChildObj("水").transform.DOScaleX(1, 2));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "幼鱼").Scale = Vector3.one);
            sequence.Append(GetObjBehaviour("交互物体", "幼鱼").transform.DOBlendableMoveBy(new Vector3(0, -0.1f, 0), 1));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "幼鱼").Parent = GetObjBehaviour("交互物体", "梯型缸1").transform;
                NextStep();
            });
        }

        private void TStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "梯型缸2").GetChildObj("水").transform.DOScaleZ(1, 2));
            sequence.Join(GetObjBehaviour("交互物体", "梯型缸2").GetChildObj("水").transform.DOScaleX(1, 2));
            sequence.AppendCallback(() => GetObjBehaviour("交互物体", "雄鱼").Scale = Vector3.one);
            sequence.Append(GetObjBehaviour("交互物体", "雄鱼").transform.DOBlendableMoveBy(new Vector3(0, -0.1f, 0), 1));
            sequence.AppendCallback(() =>
                GetObjBehaviour("交互物体", "雄鱼").Parent = GetObjBehaviour("交互物体", "梯型缸2").transform);
            sequence.OnComplete(() =>
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "TStep相机位置2", 2, NextStep));
        }

        private void TStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "隔音箱").GetChildObj("门").DOLocalRotate(new Vector3(-90, 0, 90), 2));
            sequence.Join(GetObjBehaviour("交互物体", "隔音箱1").GetChildObj("门").DOLocalRotate(new Vector3(-90, 0, 90), 2));
            sequence.Append(GetObjBehaviour("交互物体", "梯型缸1").transform.DOPath(GetObjBehaviour("目标位置", "T型缸1Path")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "梯型缸2").transform.DOPath(GetObjBehaviour("目标位置", "T型缸2Path")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Append(GetObjBehaviour("交互物体", "隔音箱").GetChildObj("门").DOLocalRotate(new Vector3(-90, 0, 0), 2));
            sequence.Join(GetObjBehaviour("交互物体", "隔音箱1").GetChildObj("门").DOLocalRotate(new Vector3(-90, 0, 0), 2));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                WaitTimeDoFunc(2, () => GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("10分钟后",
                    "提示", "确认",
                    () =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera")
                            .MoveLookCamera("目标位置", "TStep相机位置3", 2, () => sequence.Play());
                        GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 40;
                    }));
            });
            sequence.Append(GetObjBehaviour("交互物体", "幼鱼").transform.DOPath(GetObjBehaviour("目标位置", "对照鱼Path").transform
                    .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10, PathType.CatmullRom)
                .SetLookAt(0));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjLogic<LookCameraLogic>("Main Camera")
                    .MoveLookCamera("目标位置", "TStep相机位置4", 2, () => sequence.Play());
                GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 60;
            });
            sequence.Append(GetObjBehaviour("交互物体", "雄鱼").transform.DOPath(GetObjBehaviour("目标位置", "TCSPath").transform
                    .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10, PathType.CatmullRom)
                .SetLookAt(0));
            sequence.AppendCallback(() => WaitTimeDoFunc(1.5f, () =>
            {
                GetUILogic<SmallTipsInfoLogic>("SmallTips").SetContent("三氯生处理的斑马鱼更倾向在底部栖息");
                GetUILogic<WarningPanelLogic>("WarningPanel")
                    .SetWarningThenDoFunc(Resources.Load<Sprite>("Image/1.栖底实验结果热图"), "结果", "确认", () =>
                    {
                        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                            .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                        GetUILogic<SmallTipsInfoLogic>("SmallTips").Switch(false);
                        NextStep();
                    });
            }));
        }

        #endregion

        #region 社交实验

        private void SJStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SJStep_1目标位置", 1.5f, NextStep);
        }

        private void SJStep_2()
        {
            GetObjBehaviour("交互物体", "SJ社交缸对照").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "SJ社交缸对照").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "SJ社交缸对照").SetObjHighlight(false);
                // GetObjBehaviour("交互物体", "SJ鱼").transform.DOPath(GetObjBehaviour("目标位置", "鱼到社交缸1").transform
                //     .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                GetObjBehaviour("交互物体", "SJ鱼").transform.DOLocalMoveY(1, 2);
                GetObjBehaviour("交互物体", "SJ鱼1").transform.DOLocalMoveY(1, 2);
                GetObjBehaviour("交互物体", "SJ社交缸对照").RemoveObjClickListener();
                NextStep();
            });
        }

        private void SJStep_3()
        {
            GetObjBehaviour("交互物体", "SJ普通缸").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "SJ普通缸").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "SJ普通缸").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "SJ单独鱼1").transform.DOPath(GetObjBehaviour("目标位置", "鱼到社交缸中间1").transform
                    .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                GetObjBehaviour("交互物体", "SJ普通缸").RemoveObjClickListener();
                NextStep();
            });
        }

        private void SJStep_4()
        {
            GetObjBehaviour("交互物体", "SJ处理缸").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "SJ处理缸").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "SJ处理缸").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "SJ处理缸").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "SJ单独鱼2").transform.DOPath(GetObjBehaviour("目标位置", "处理鱼到社交缸中间1_1").transform
                    .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                float ii = 0;
                DOTween.To(() => ii, x => ii = x, 2, 3).OnComplete(NextStep);
                //WaitTimeDoFunc(3, NextStep);
            });
        }

        private void SJStep_5()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SJStep_5目标位置", 1.5f, null);
            GetObjBehaviour("交互物体", "SJ鱼").Parent = GetObjBehaviour("交互物体", "SJ社交缸对照").transform;
            GetObjBehaviour("交互物体", "SJ鱼1").Parent = GetObjBehaviour("交互物体", "SJ社交缸处理").transform;
            GetObjBehaviour("交互物体", "SJ单独鱼1").Parent = GetObjBehaviour("交互物体", "SJ社交缸对照").transform;
            GetObjBehaviour("交互物体", "SJ单独鱼2").Parent = GetObjBehaviour("交互物体", "SJ社交缸处理").transform;
            GetObjBehaviour("交互物体", "SJ隔音箱").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "SJ隔音箱").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "SJ隔音箱").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "SJ隔音箱").RemoveObjClickListener();
                //GetObjBehaviour("交互物体", "SJ门").transform.DOLocalRotate(new Vector3(-90, 0, 100), 2).SetAutoKill(false).SetId("开门").OnComplete(()=> 
                //{
                //    GetObjBehaviour("交互物体", "SJ社交缸").transform.DOPath(GetObjBehaviour("目标位置", "社交缸进入隔音箱并设置1").transform.
                //    GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(()=> 
                //    {
                //        DOTween.PlayBackwards("开门");
                //        WaitTimeDoFunc(3, () => SJStep_5_2());
                //    });
                //});
                //DOTween.Kill("开门");
                GetObjBehaviour("交互物体", "SJ隔音箱1").GetChildObj("SJ门").transform
                    .DOLocalRotate(new Vector3(-90, 0, 100), 2).OnComplete(
                        () =>
                        {
                            GetObjBehaviour("交互物体", "SJ社交缸处理").transform
                                .DOPath(
                                    GetObjBehaviour("目标位置", "处理缸进入隔音箱并设置1").transform
                                        .GetComponentsInChildren<Transform>()
                                        .Select(x => x.position).ToArray(), 2).OnComplete(() =>
                                    GetObjBehaviour("交互物体", "SJ隔音箱1").GetChildObj("SJ门").transform
                                        .DOLocalRotate(new Vector3(-90, 0, 0), 2));
                        });
                GetObjBehaviour("交互物体", "SJ门").transform.DOLocalRotate(new Vector3(-90, 0, 100), 2).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "SJ社交缸对照").transform
                        .DOPath(
                            GetObjBehaviour("目标位置", "社交缸进入隔音箱并设置1").transform.GetComponentsInChildren<Transform>()
                                .Select(x => x.position).ToArray(), 2).OnComplete(() =>
                        {
                            GetObjBehaviour("交互物体", "SJ门").transform.DOLocalRotate(new Vector3(-90, 0, 0), 2)
                                .OnComplete(() => { WaitTimeDoFunc(1, () => SJStep_5_2()); });
                        });
                });
            });
        }

        private void SJStep_5_2()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SJStep_5目标位置2", 1.5f, null);
            GetObjBehaviour("交互物体", "SJ电脑").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "SJ电脑").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "SJ电脑").AddObjClickListener(()=>GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", "等待","完成",()=> 
            {
                GetObjBehaviour("交互物体", "SJ电脑").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "SJ电脑").RemoveObjClickListener();
                NextStep();
            }));;
            //画面移到电脑显示屏，在电脑软件上设置测定行为参数：速度、距离、频率和停留时间，程序运行完毕，弹出查看结果的窗口
        }

        private void SJStep_6()
        {
            print("6开始");
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 40;
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().nearClipPlane = 0.3f;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SJStep_6目标位置", 0.5f, null);
            GetObjBehaviour("交互物体", "雄鱼1").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (0)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 12, PathType.CatmullRom).SetLookAt(0);
            GetObjBehaviour("交互物体", "雌鱼2").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (1)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 15, PathType.CatmullRom).SetLookAt(0);
            GetObjBehaviour("交互物体", "雄鱼3").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (2)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 12, PathType.CatmullRom).SetLookAt(0);
            GetObjBehaviour("交互物体", "雌鱼4").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (5)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 13, PathType.CatmullRom).SetLookAt(0);
            GetObjBehaviour("交互物体", "雄鱼5").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (6)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 12, PathType.CatmullRom).SetLookAt(0);
            GetObjBehaviour("交互物体", "雌鱼6").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (7)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 15, PathType.CatmullRom).SetLookAt(0);
            //GetObjBehaviour("交互物体", "SJ单独鱼1").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (3)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 14, PathType.CatmullRom).SetLookAt(0);

            GetObjBehaviour("交互物体", "SJ单独鱼1").transform.DOPath(GetObjBehaviour("目标位置", "鱼游动路径 (4)").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 13, PathType.CatmullRom).SetLookAt(0).OnComplete(()=>
            {
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "SJStep_7目标位置", 0.5f, null);
                for (int i = 7; i < 13; i++)
                {
                    float delay = (float)i /3;
                    GetObjBehaviour("交互物体", "鱼" + i).transform.DOPath(GetObjBehaviour("目标位置", "处理Path")
                        .GetComponentsInChildren<Transform>()
                        .Select(x => x.position).ToArray(), 10, PathType.CatmullRom).SetLookAt(0).SetDelay(delay);
                }

                GetObjBehaviour("交互物体", "SJ单独鱼2").transform.DOPath(GetObjBehaviour("目标位置", "处理Path")
                    .GetComponentsInChildren<Transform>()
                    .Select(x => x.position).ToArray(), 10, PathType.CatmullRom).SetLookAt(0).OnComplete(() =>
                    GetUILogic<WarningPanelLogic>("WarningPanel")
                        .SetWarningThenDoFunc(Resources.Load<Sprite>("Image/2.社交实验结果热图"), "结果", "知道了", SJStep_6_2));
            });
            //动画显示，对照和TCS处理斑马鱼游动的视频（动画制作游动趋势和过程）
        }

        private void SJStep_6_2()
        {
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 60;
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(2,StepProgressPanelLogic.StepState.Complete);
            NextStep();
        }
        #endregion

        #region 迷宫实验

        private void MGStep_1()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(3, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机MG1", 1.5f, NextStep);
        }

        private void MGStep_2()
        {
            GetObjBehaviour("交互物体", "T型缸MG").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "T型缸MG").AddObjClickListener(()=>
            {
                GetObjBehaviour("交互物体", "T型缸MG").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "T型缸MG").RemoveObjClickListener();
                GetObjBehaviour("交互物体","雄鱼MG").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼位置进入TMG1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 4);
                GetObjBehaviour("交互物体", "单独鱼MG").transform.DOPath(GetObjBehaviour("目标位置", "单独鱼位置进入TMG1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 4).OnComplete(()=> 
                {
                    GetObjBehaviour("交互物体", "雄鱼MG").transform.parent = GetObjBehaviour("交互物体", "T型缸MG").transform;
                    GetObjBehaviour("交互物体", "单独鱼MG").transform.parent = GetObjBehaviour("交互物体", "T型缸MG1").transform;
                   NextStep();
                });
            });
            
        }

        private void MGStep_3()
        {
            GetObjBehaviour("交互物体", "挡板").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "挡板").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "挡板").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "挡板").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "木锤MG").Scale = Vector3.one;
                GetObjBehaviour("交互物体", "木锤MG1").Scale = Vector3.one;
                GetObjBehaviour("交互物体", "挡板1").transform.DOPath(GetObjBehaviour("交互物体", "挡板1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                GetObjBehaviour("交互物体", "挡板").transform.DOPath(GetObjBehaviour("交互物体", "挡板").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                GetObjBehaviour("交互物体", "木锤MG").transform.DOBlendableLocalRotateBy(new Vector3(0,-10,0),0.5f).SetAutoKill(false).SetId("敲洞").OnComplete(()=>DOTween.PlayBackwards("敲洞")).SetLoops(10).OnComplete(()=> 
                {
                    GetObjBehaviour("交互物体", "木锤MG").Scale = Vector3.zero;
                });
                GetObjBehaviour("交互物体", "木锤MG1").transform.DOBlendableLocalRotateBy(new Vector3(0,-10,0),0.5f).SetAutoKill(false).SetId("敲洞1").OnComplete(()=>DOTween.PlayBackwards("敲洞1")).SetLoops(10).OnComplete(()=> 
                {
                    GetObjBehaviour("交互物体", "木锤MG1").Scale = Vector3.zero;
                });
                GetObjBehaviour("交互物体", "雄鱼MG").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼路径MG1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 20).SetLookAt(0);
                GetObjBehaviour("交互物体", "单独鱼MG").transform.DOPath(GetObjBehaviour("目标位置", "单独鱼路径MG1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 20).SetLookAt(0).OnComplete(()=> 
                {
                    NextStep();
                });
            });

        }

        private void MGStep_4()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机MG2", 1.5f, null);
            GetObjBehaviour("交互物体", "隔音箱MG").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "隔音箱MG").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "隔音箱MG").AddObjClickListener(() => 
            {
                GetObjBehaviour("交互物体", "隔音箱MG1").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "隔音箱MG1").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "MG门1").transform.DOBlendableLocalRotateBy(new Vector3(0, 100, 0), 1).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "T型缸MG1").transform.DOPath(GetObjBehaviour("目标位置", "T型缸移动到隔音箱MG1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3).OnComplete(() =>
                    {
                        GetObjBehaviour("交互物体", "MG门1").transform.DOBlendableLocalRotateBy(new Vector3(0, -100, 0), 1);
                    });
                });
                GetObjBehaviour("交互物体", "MG门").transform.DOBlendableLocalRotateBy(new Vector3(0, 100, 0), 1).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "T型缸MG").transform.DOPath(GetObjBehaviour("目标位置", "T型缸移动到隔音箱MG").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3).OnComplete(() =>
                    {
                        GetObjBehaviour("交互物体", "MG门").transform.DOBlendableLocalRotateBy(new Vector3(0, -100, 0), 1).OnComplete(()=> 
                        {
                            NextStep();
                        });
                    });
                });
            });
        }

        private void MGStep_5()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机MG3", 1.5f, null);
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 40;
            GetObjBehaviour("交互物体", "隔音箱MG").Collider().enabled = false;
            GetObjBehaviour("交互物体", "T型缸MG").Collider().enabled = false;
            GetObjBehaviour("交互物体", "挡板").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "挡板").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "挡板").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "挡板").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "挡板").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "挡板").transform.DOPath(GetObjBehaviour("交互物体", "挡板").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2);
                GetObjBehaviour("交互物体", "雄鱼MG").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼路径MG2").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 20)
                    .SetLookAt(0).OnComplete(() =>
                    {
                        GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机MG5", 0.5f, null);
                        GetObjBehaviour("交互物体", "单独鱼MG").transform.DOPath(
                                GetObjBehaviour("目标位置", "单独鱼路径MG2").GetComponentsInChildren<Transform>()
                                    .Select(x => x.position).ToArray(), 20)
                            .SetLookAt(0).OnComplete(() =>
                            {
                                GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 60;
                                NextStep();
                            });
                    });

            });
        }
        // private void MGStep_6()
        // {
        //     GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机MG4", 1.5f, null);
        //     GetObjBehaviour("交互物体", "电脑MG").SetObjHighlight(true);
        //     GetObjBehaviour("交互物体", "电脑MG").RemoveObjClickListener();
        //     GetObjBehaviour("交互物体", "电脑MG").AddObjClickListener(()=>
        //     {
        //         GetObjBehaviour("交互物体", "电脑MG").SetObjHighlight(false);
        //
        //         GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("设置测定行为参数：速度、距离、频率和停留时间，运动录像10 min", "等待", "完成", () =>
        //         {
        //             NextStep();
        //         });
        //     });
        // }

        private void MGStep_7()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/3.T迷宫实验结果热图"), "结果", "知道了", () =>
            {
                GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(3,StepProgressPanelLogic.StepState.Complete);
                NextStep();
            });
        }
        #endregion

        #region 位置偏好实验

        private void WZPHStep_1()
        {
            //摄像机WZPH1
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(4, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机WZPH1", 1.5f, NextStep);
        }

        private void WZPHStep_2()
        {
            GetObjBehaviour("交互物体", "双洞板").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "双洞板").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "双洞板").AddObjClickListener(()=> 
            {
                GetObjBehaviour("交互物体", "双洞板").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "双洞板").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "双洞板").transform.DOPath(GetObjBehaviour("目标位置", "双洞板轨迹位置").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 1);
                GetObjBehaviour("交互物体", "试验缸WZPH").transform.DOPath(GetObjBehaviour("目标位置", "试验缸轨迹WZPH1_1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(()=> 
                {
                    GetObjBehaviour("交互物体", "双洞板").transform.parent = GetObjBehaviour("交互物体", "试验缸WZPH").transform;
                    GetObjBehaviour("交互物体", "双洞板1").Scale=Vector3.one;
                    GetObjBehaviour("交互物体", "双洞板1").transform.parent = GetObjBehaviour("交互物体", "试验缸WZPH1").transform;
                    NextStep();
                });
            });
        }

        private void WZPHStep_3()
        {
            GetObjBehaviour("交互物体", "普通缸WZPH").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "普通缸WZPH").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "普通缸WZPH").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "普通缸WZPH").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "普通缸WZPH").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "雄鱼WZPH").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼轨迹WZPH 0").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "雄鱼WZPH").transform.parent = GetObjBehaviour("交互物体", "试验缸WZPH").transform;
                    NextStep();
                });
            });
        }

        private void WZPHStep_4()
        {
            GetObjBehaviour("交互物体", "处理缸WZPH").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "处理缸WZPH").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "处理缸WZPH").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "处理缸WZPH").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "处理缸WZPH").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "单独鱼WZPH").transform.DOPath(GetObjBehaviour("目标位置", "单独鱼轨迹WZPH 0").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "单独鱼WZPH").transform.parent = GetObjBehaviour("交互物体", "试验缸WZPH1").transform;
                    NextStep();
                });
            });
        }

        private void WZPHStep_5()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机WZPH2", 1.5f, null);
            GetObjBehaviour("交互物体", "隔音箱WZPH").SetObjHighlight(true);
            GetObjBehaviour("交互物体", "隔音箱WZPH").RemoveObjClickListener();
            GetObjBehaviour("交互物体", "隔音箱WZPH").AddObjClickListener(() =>
            {
                GetObjBehaviour("交互物体", "隔音箱WZPH").SetObjHighlight(false);
                GetObjBehaviour("交互物体", "隔音箱WZPH").RemoveObjClickListener();
                GetObjBehaviour("交互物体", "WZPH门1").transform.DOBlendableLocalRotateBy(new Vector3(0, 100, 0), 1).OnComplete(() => 
                {
                    GetObjBehaviour("交互物体", "试验缸WZPH1").transform.DOPath(GetObjBehaviour("目标位置", "试验缸轨迹WZPH1_3").GetComponentsInChildren<Transform>()
                        .Select(x => x.position).ToArray(), 3).OnComplete(() =>
                        GetObjBehaviour("交互物体", "WZPH门1").transform.DOBlendableLocalRotateBy(new Vector3(0, -100, 0), 1));
                });
                GetObjBehaviour("交互物体", "WZPH门").transform.DOBlendableLocalRotateBy(new Vector3(0, 100, 0), 1).OnComplete(() => 
                {
                    GetObjBehaviour("交互物体", "试验缸WZPH").transform.DOPath(GetObjBehaviour("目标位置", "试验缸轨迹WZPH1_2").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3).OnComplete(() =>
                    {
                        GetObjBehaviour("交互物体", "WZPH门").transform.DOBlendableLocalRotateBy(new Vector3(0, -100, 0), 1);
                        NextStep();
                    });
                });
            });

        }

        private void WZPHStep_6()
        {
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 40;
            GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机WZPH3", 0.5f, ()=> 
            {
                GetObjBehaviour("交互物体", "雄鱼WZPH").transform.DOPath(GetObjBehaviour("目标位置", "雄鱼轨迹WZPH 1").GetComponentsInChildren<Transform>()
                    .Select(x => x.position).ToArray(), 10).SetLookAt(0).OnComplete(() =>
                {
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "摄像机WZPH4", 1f, ()=>
                    {
                        GetObjBehaviour("交互物体", "单独鱼WZPH").transform.DOPath(GetObjBehaviour("目标位置", "单独鱼轨迹WZPH 1").GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 10).SetLookAt(0).OnComplete(() =>
                        {
                            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().fieldOfView = 60;
                            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().nearClipPlane = 0.01f;
                            NextStep();
                        });
                    });
                });

            });
        }

        private void WZPHStep_7()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/4.位置偏好实验结果热图"), "结果", "知道了", () =>
            {
                GetUILogic<StepProgressPanelLogic>("StepProgressPanel").ChangeState(4,StepProgressPanelLogic.StepState.Complete);
                NextStep();
            });
        }


        private void WZPHStep_8()
        {
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
            GetUILogic<StepTipLogic>("StepTip").Hide();
            GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("TCS亚慢性暴毒的行为学实验",MainModulePanelLogic.ExperimentState.Complete);
            GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("斑马鱼成鱼(脑组织酶活性)的测定",MainModulePanelLogic.ExperimentState.Underway);
            GetUILogic<MainModulePanelLogic>("MainModulePanel").Show();
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Hide();
            NextStep();
        }

        #endregion

        #region 斑马鱼成鱼结构认知

        /// <summary>
        /// 初始配置
        /// </summary>
        private void XStep_1()
        {
            string[] names = {"斑马鱼成鱼结构认知", "斑马鱼脑解剖", "酶活性的测定"};
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
            GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
            GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                .ChangeState(0, StepProgressPanelLogic.StepState.Underway);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "XStep_1相机位置");
            Dictionary<NamePair, string> tips = new Dictionary<NamePair, string>();
            tips.Add(new NamePair("斑马鱼全身", "心脏"), "心脏\nHeart");
            tips.Add(new NamePair("斑马鱼全身", "肠"), "肠\nIntestine");
            tips.Add(new NamePair("斑马鱼全身", "胰腺"), "胰腺\nPancreas");
            tips.Add(new NamePair("斑马鱼全身", "肾脏"), "肾脏\nKidney");
            tips.Add(new NamePair("斑马鱼全身", "脾"), "脾\nSpleen");
            tips.Add(new NamePair("斑马鱼全身", "肝脏"), "肝脏\nLiver");
            tips.Add(new NamePair("斑马鱼全身", "肌肉"), "肌肉\nMuscle");
            tips.Add(new NamePair("斑马鱼全身", "大脑"), "大脑\nBrain");
            GetObjBehaviour("Main Camera", "Main Camera").GetComponent<Camera>().nearClipPlane = 0.01f;
            GetUILogic<ToolTipPanelLogic>("ToolTip").SetToolTip(tips);
            NextStep();
        }

        private void XStep_2()
        {
            GetObjBehaviour("斑马鱼全身", "鱼身").transform.localScale = Vector3.zero;
            GetObjBehaviour("斑马鱼全身", "内脏").transform.localScale = Vector3.zero;
            GetObjBehaviour("斑马鱼全身", "大脑").GetComponent<BoxCollider>().enabled = false;
            GetObjBehaviour("斑马鱼全身", "大脑").transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "XStep_2相机位置");
            Dictionary<NamePair, string> tips = new Dictionary<NamePair, string>();
            tips.Add(new NamePair("斑马鱼全身", "嗅球"), "嗅球\nOlfactory bulb");
            tips.Add(new NamePair("斑马鱼全身", "端脑"), "端脑\nTelencephalon");
            tips.Add(new NamePair("斑马鱼全身", "缰核"), "缰核\nHabenula");
            tips.Add(new NamePair("斑马鱼全身", "视顶盖"), "视顶盖\nOptic tectum");
            tips.Add(new NamePair("斑马鱼全身", "小脑"), "小脑\nCerebellum");
            tips.Add(new NamePair("斑马鱼全身", "延髓"), "延髓\nMedulla");
            GetUILogic<ToolTipPanelLogic>("ToolTip").SetToolTip(tips);
            NextStep();
        }

        private void XStep_3()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(
                "斑马鱼中枢神经系统自前段向后依次为：\n嗅球(olfactory bulb)、端脑(telencephalon)、间脑(diencephalon)、下丘脑(hypothalamus)、视顶盖(optic tectum)、小脑(cerebellum)、后脑(rhombencephalon)和延髓(medulla)等基本结构。",
                "斑马鱼中枢神经系统介绍", "查看染色图", NextStep);
        }

        private void XStep_4()
        {
            GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc(Resources.Load<Sprite>("Image/HE染色图"),
                "斑马鱼中枢神经系统HE染色图", "查看完毕", () =>
                {
                    GetObjBehaviour("交互物体", "斑马鱼成鱼结构认知").transform.localScale = Vector3.zero;
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(0, StepProgressPanelLogic.StepState.Complete);
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(1, StepProgressPanelLogic.StepState.Underway);
                    NextStep();
                });
        }

        #endregion

        #region 斑马鱼脑解剖

        private void YStep_1()
        {
            GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "YStep_1相机位置");
            GetUILogic<AVProLogic>("AVProPanel").PlayVideoUseLocal("3.3斑马鱼脑部解剖");
            GetUILogic<AVProLogic>("AVProPanel").SetEndAction(() =>
            {
                //GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "YStep_1相机位置");
                NextStep();
            });
        }

        private void YStep_2()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.y+0.07F, 0.5F));
            sequence.AppendCallback(()=> {
                GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveX(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.x + 0.1F, 0.5F);
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
                .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path1")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y移液枪3注射器").transform.position.y + 0.005F, 0.5F).OnComplete(()=> {
                    sequence.Play();
                });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
                .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path2")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.Insert(5f, GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveX(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.x, 0.5F).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                    .DOMoveY(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.y - 0.07F, 0.5F);
                }));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y移液枪3注射器").transform.position.y - 0.005F, 0.5F).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Y_EP溶液").Scale = Vector3.one;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
               .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path3")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(NextStep);
        }
        private void YStep_2_1()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.y + 0.07F, 0.5F));
            sequence.AppendCallback(() => {
                GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveX(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.x + 0.1F, 0.5F);
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
                .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path1")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y移液枪3注射器").transform.position.y + 0.005F, 0.5F).OnComplete(() => {
                    sequence.Play();
                });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
                .DOPath(GetObjBehaviour("目标位置", "Y移液枪1Path2")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.Insert(5f, GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                .DOMoveX(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.x, 0.5F).OnComplete(() =>
                {
                    GetObjBehaviour("交互物体", "Y_PBS盖子").transform
                    .DOMoveY(GetObjBehaviour("交互物体", "Y_PBS盖子").transform.position.y - 0.07F, 0.5F);
                }));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOMoveY(GetObjBehaviour("交互物体", "Y移液枪3注射器").transform.position.y - 0.005F, 0.5F).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Y_EP2溶液").Scale = Vector3.one;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
               .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path3")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            
            sequence.Insert(7.5f, GetObjBehaviour("交互物体", "Y_EP").transform
               .DOPath(GetObjBehaviour("目标位置", "Y_EPPath1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Insert(7.5f, GetObjBehaviour("交互物体", "Y_EP2").transform
               .DOPath(GetObjBehaviour("目标位置", "Y_EP2Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));

            sequence.Append(GetObjBehaviour("交互物体", "Y匀浆器").transform
               .DOPath(GetObjBehaviour("目标位置", "Y匀浆器Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.Insert(10f, GetObjBehaviour("交互物体", "Y匀浆器").transform
               .DORotate(GetObjBehaviour("目标位置", "Y匀浆器Path1")
               .transform.eulerAngles, 1));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y匀浆器棍子").transform
               .DOLocalRotateQuaternion(GetObjBehaviour("目标位置", "Y匀浆器旋转").transform.localRotation, 1f).OnComplete(() => {
                   sequence.Play();
               });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y匀浆器").transform
               .DOPath(GetObjBehaviour("目标位置", "Y匀浆器1Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y匀浆器棍子").transform
               .DOLocalRotateQuaternion(GetObjBehaviour("目标位置", "Y匀浆器旋转").transform.localRotation, 1f).OnComplete(() => {
                   sequence.Play();
               });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y匀浆器").transform
               .DOPath(GetObjBehaviour("目标位置", "Y匀浆器Path2")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.Insert(17f, GetObjBehaviour("交互物体", "Y匀浆器").transform
               .DORotate(GetObjBehaviour("目标位置", "Y匀浆器Path2")
               .transform.eulerAngles, 1));
            sequence.AppendCallback(() =>
            {
                GetObjBehaviour("交互物体", "Y_EP盖子").transform
                   .DOLocalRotate(Vector3.zero, 1);
                GetObjBehaviour("交互物体", "Y_EP2盖子").transform
                   .DOLocalRotate(Vector3.zero, 1).OnComplete(() =>
                   {
                       GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "YStep_2相机位置");
                       NextStep();
                   });
            });

        }
        private void YStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Y_EP").transform
               .DOPath(GetObjBehaviour("目标位置", "Y_EPPath2")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Insert(0F, GetObjBehaviour("交互物体", "Y_EP2").transform
               .DOPath(GetObjBehaviour("目标位置", "Y_EP2Path2")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));

            sequence.Insert(1F,GetObjBehaviour("交互物体", "Y_EP").transform
               .DORotate(GetObjBehaviour("目标位置", "Y_EPPath2")
               .transform.eulerAngles, 1));
            sequence.Insert(1F, GetObjBehaviour("交互物体", "Y_EP2").transform
               .DORotate(GetObjBehaviour("目标位置", "Y_EP2Path2")
               .transform.eulerAngles, 1));
            sequence.Append(GetObjBehaviour("交互物体", "Y离心机小盖子").transform
               .DOMove(GetObjBehaviour("目标位置", "Y离心机小盖子关")
               .transform
               .position, 1));
            sequence.Append(GetObjBehaviour("交互物体", "Y离心机小盖子").transform
               .DOMove(GetObjBehaviour("目标位置", "Y离心机小盖子关")
               .transform
               .position, 1));
            sequence.Append(GetObjBehaviour("交互物体", "Y离心机大盖子").transform
               .DOLocalRotate(Vector3.zero, 1).SetAutoKill(false));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y离心机大盖子").transform
                   .DOLocalRotate(Vector3.zero, 2).OnComplete(()=> {
                       GetObjBehaviour("交互物体", "Y_EP上清液").Scale = Vector3.one;
                       GetObjBehaviour("交互物体", "Y_EP2上清液").Scale = Vector3.one;
                       GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("5分钟后", "提示",
                    "完成", ()=> { sequence.Play(); });
                   });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Y离心机大盖子").transform
               .DOLocalRotate(new Vector3(-90,0,0), 1).SetAutoKill(false));
            sequence.Append(GetObjBehaviour("交互物体", "Y离心机小盖子").transform
               .DOMove(GetObjBehaviour("目标位置", "Y离心机小盖子开")
               .transform
               .position, 1));
            sequence.Append(GetObjBehaviour("交互物体", "Y_EP").transform
               .DOPath(GetObjBehaviour("目标位置", "Y_EPPath3")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Insert(7F, GetObjBehaviour("交互物体", "Y_EP2").transform
                   .DOPath(GetObjBehaviour("目标位置", "Y_EP2Path3")
                   .transform
                   .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));

            sequence.Insert(6.5F, GetObjBehaviour("交互物体", "Y_EP").transform
               .DORotate(Vector3.zero, 1));
            sequence.Insert(6.5F, GetObjBehaviour("交互物体", "Y_EP2").transform
               .DORotate(Vector3.zero, 1));
            sequence.AppendCallback(()=> {
                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "YStep_1相机位置");
                NextStep();
            });
        }

        private void YStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Y_EP1").transform
              .DOPath(GetObjBehaviour("目标位置", "Y_EP1Path1")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.Insert(0, GetObjBehaviour("交互物体", "Y_EP3").transform
              .DOPath(GetObjBehaviour("目标位置", "Y_EP3Path1")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                GetObjBehaviour("交互物体", "Y_EP盖子").transform
                      .DOLocalRotate(new Vector3(0,0, -168), 1);
                GetObjBehaviour("交互物体", "Y_EP2盖子").transform
                      .DOLocalRotate(new Vector3(0, 0, -168), 1);
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
               .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path4")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOLocalMoveY(0.0821F, 0.5F));
            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path5")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(),3));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOLocalMoveY(0.0758F, 0.5F).OnKill(() =>
                {
                    GetObjBehaviour("交互物体", "Y_EP1上清液").Scale = Vector3.one;
                    sequence.Play();
                });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path7")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOLocalMoveY(0.0821F, 0.5F).OnKill(() =>
                {
                    sequence.Play();
                });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path8")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));
            sequence.AppendCallback(() =>
            {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Y移液枪3注射器").transform
                .DOLocalMoveY(0.0758F, 0.5F).OnKill(() =>
                {
                    GetObjBehaviour("交互物体", "Y_EP3上清液").Scale = Vector3.one;
                    sequence.Play();
                });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Y移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Y移液枪Path6")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 3));

            sequence.AppendCallback(() =>
            {
                WaitTimeDoFunc(2, () =>
                {
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(1, StepProgressPanelLogic.StepState.Complete);
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                        .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
                    GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "ZStep_1相机位置");
                    NextStep();
                });
            });
        }

        #endregion

        #region 酶活性的测定
        private void ZStep_1()
        {
            NextStep();
        }
        private void ZStep_2()
        {
            GetObjBehaviour("交互物体", "Z96孔板盖子").transform
               .DOPath(GetObjBehaviour("目标位置", "Z96孔板盖子Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(NextStep);
        }
        private void ZStep_3()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.
                DOMoveZ(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.position.z+0.03f,0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.position.x + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(()=>{
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep超纯水特写相机位置", 1,null);
            }));

            sequence.AppendCallback(()=> {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y+0.005f, 0.5f).OnComplete(()=> {
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                    sequence.Play(); }).SetAutoKill(false);

            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path2")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z超纯水盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.position.x - 0.03f, 0.5f).OnComplete(()=> {
                        GetObjBehaviour("交互物体", "Z超纯水盖子").transform.
                        DOMoveZ(GetObjBehaviour("交互物体", "Z超纯水盖子").transform.position.z - 0.03f, 0.5f);
                    });
                   GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y - 0.005f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Za水1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Za水2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Za水3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play(); });
            });

            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path3")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(()=> {
                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = false;
                NextStep();
            });
        }
        private void ZStep_4()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
               .DOPath(GetObjBehaviour("目标位置", "Z换枪头1Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(()=> {
                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = true;
                GetObjBehaviour("交互物体", "YYQ-T105").GetComponent<MeshRenderer>().enabled = false;
            });

            sequence.Append(GetObjBehaviour("交互物体", "Z校准液盖子").transform.
                DOMoveZ(GetObjBehaviour("交互物体", "Z校准液盖子").transform.position.z + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z校准液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z校准液盖子").transform.position.x + 0.03f, 0.5f));

            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path4")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(() => {
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep校准液特写相机位置", 1, null);
               }));

            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y + 0.005f, 0.5f).OnComplete(() => {
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                    sequence.Play(); });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path5")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z校准液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z校准液盖子").transform.position.x - 0.03f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Z校准液盖子").transform.
                    DOMoveZ(GetObjBehaviour("交互物体", "Z校准液盖子").transform.position.z - 0.03f, 0.5f);
                });
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y - 0.004f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Zb校1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zb校2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zb校3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path6")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(()=> {
                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = false;
                NextStep();
            });
        }
        private void ZStep_5()
        {
            Sequence sequence = DOTween.Sequence();
           
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path1")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(() => {
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep离心管特写相机位置", 1, null);
               }));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.position.y + 0.005f, 0.5f).OnComplete(() => {
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                    sequence.Play(); });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path2")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.position.y - 0.004f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Zc对1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zc对2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zc对3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path3")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                GetObjBehaviour("交互物体", "Z枪头3").GetComponent<MeshRenderer>().enabled = false;
                NextStep();
            });
        }
        private void ZStep_6()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
                .DOPath(GetObjBehaviour("目标位置", "Z换枪头1Path2")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = true;
                GetObjBehaviour("交互物体", "YYQ-T106").GetComponent<MeshRenderer>().enabled = false;
            });

            sequence.Append(GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveZ(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.z + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.x + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path7")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(() => {
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep工作液特写相机位置", 1, null);
               }));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y + 0.005f, 0.5f).OnComplete(() => {
                    GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                    sequence.Play(); });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path8")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.x - 0.03f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                    DOMoveZ(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.z - 0.03f, 0.5f);
                });
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y - 0.004f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Zc工1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zc工2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zc工3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path9")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = false;
                NextStep();
            });
        }
        private void ZStep_7()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
                .DOPath(GetObjBehaviour("目标位置", "Z换枪头3Path1")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {

                GetObjBehaviour("交互物体", "Z枪头3").GetComponent<MeshRenderer>().enabled = true;
                GetObjBehaviour("交互物体", "yiyeguan_109").GetComponent<MeshRenderer>().enabled = false;
            });

            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path4")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(() => {
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep离心管特写相机位置", 1, null);
               }));
            sequence.AppendCallback(() => {
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                sequence.Pause();

                GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.position.y + 0.005f, 0.5f).OnComplete(() => { sequence.Play(); });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path5")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪3注射器").transform.position.y - 0.003f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Zd三1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zd三2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zd三3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪3").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪3Path6")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(NextStep);
        }
        private void ZStep_8()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
                .DOPath(GetObjBehaviour("目标位置", "Z换枪头1Path3")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {

                GetObjBehaviour("交互物体", "Z枪头1").GetComponent<MeshRenderer>().enabled = true;
                GetObjBehaviour("交互物体", "YYQ-T107").GetComponent<MeshRenderer>().enabled = false;
            });

            sequence.Append(GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveZ(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.z + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.x + 0.03f, 0.5f));
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
               .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path7")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnStart(() => {
                   GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep工作液特写相机位置", 1, null);
               }));
            sequence.AppendCallback(() => {
                GetObjLogic<LookCameraLogic>("Main Camera").MoveLookCamera("目标位置", "ZStep_1相机位置", 1, null);
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y + 0.005f, 0.5f).OnComplete(() => { sequence.Play(); });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
              .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path10")
              .transform
              .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(() => {
                sequence.Pause();
                GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                DOMoveX(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.x - 0.03f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Z工作液盖子").transform.
                    DOMoveZ(GetObjBehaviour("交互物体", "Z工作液盖子").transform.position.z - 0.03f, 0.5f);
                });
                GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.DOMoveY(GetObjBehaviour("交互物体", "Z移液枪1注射器").transform.position.y - 0.002f, 0.5f).OnComplete(() => {
                    GetObjBehaviour("交互物体", "Zd工1").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zd工2").GetComponent<MeshRenderer>().enabled = true;
                    GetObjBehaviour("交互物体", "Zd工3").GetComponent<MeshRenderer>().enabled = true;
                    sequence.Play();
                });
            });
            sequence.Append(GetObjBehaviour("交互物体", "Z移液枪1").transform
             .DOPath(GetObjBehaviour("目标位置", "Z移液枪1Path11")
             .transform
             .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2));
            sequence.AppendCallback(NextStep);
        }
        private void ZStep_9()
        {
            GetObjBehaviour("交互物体", "Z96孔板盖子").transform
               .DOPath(GetObjBehaviour("目标位置", "Z96孔板盖子Path2")
               .transform
               .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 1).OnComplete(() =>
               {
                   WaitTimeDoFunc(2, () =>
                   {
                       GetUILogic<WarningPanelLogic>("WarningPanel").SetWarningThenDoFunc("2分钟后", "提示",
                        "完成", () =>
                            {
                                GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "ZStep_2相机位置");
                                NextStep();
                            });
                   });

               });
        }
        private void ZStep_10()
        {
            GetObjBehaviour("交互物体", "Z96孔板1").transform
                .DOPath(GetObjBehaviour("目标位置", "Z96孔板1Path")
                .transform
                .GetComponentsInChildren<Transform>().Select(x => x.position).ToArray(), 2).OnComplete(()=> {
                    WaitTimeDoFunc(1F, () => {
                        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "ZStep_3相机位置");
                        NextStep();
                    });
                });
        }

        private void RStep_1()
        {
    //        string[] names = { "斑马鱼成鱼结构认知", "斑马鱼脑解剖", "酶活性的测定" };
    //        GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Initialize(names);
    //        GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Show();
    //        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
    //.ChangeState(0, StepProgressPanelLogic.StepState.Complete);
    //        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
    //.ChangeState(1, StepProgressPanelLogic.StepState.Complete);
    //        GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
    //            .ChangeState(2, StepProgressPanelLogic.StepState.Underway);
    //        GetObjLogic<LookCameraLogic>("Main Camera").SetLookCamera("目标位置", "ZStep_3相机位置");


            //GetObjBehaviour("交互物体", "Z电脑").GetComponent<BoxCollider>().enabled = false;
            ChangeButtonImage("板布局", new Vector3(-207, 90, 0), new Vector2(106, 10));
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/1");
            NextStep();
        }

        private void RStep_2()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/2");
            ChangeButtonImage("操作步骤", new Vector3(-207, 80, 0), new Vector2(106, 10));
            NextStep();
        }

        private void RStep_3()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/3");
            ChangeButtonImage("吸光度", new Vector3(-269, 141, 0), new Vector2(20, 29));
            NextStep();
        }

        private void RStep_4()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/4");
            ChangeButtonImage("吸光度输入框", new Vector3(-70, 70, 0), new Vector2(66, 12));
            NextStep();
        }

        private void RStep_5()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/5");
            ChangeButtonImage("吸光度1", new Vector3(-201, 52, 0), new Vector2(99, 8));
            NextStep();
        }

        private void RStep_6()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/6");
            ChangeButtonImage("按钮2", new Vector3(-252, -159, 0), new Vector2(28, 13));
            NextStep();
        }

        private void RStep_7()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/8");
            ChangeButtonImage("温度", new Vector3(-249, -128, 0), new Vector2(31, 9));
            NextStep();
        }

        private void RStep_8()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/10");
            ChangeButtonImage("确定", new Vector3(-214, -152, 0), new Vector2(20, 9));
            NextStep();
        }

        private void RStep_9()
        {
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/11");
            ChangeButtonImage("按钮1", new Vector3(-209, -143, 0), new Vector2(111, 19));
            NextStep();
        }

        private void RStep_10()
        {
            GetUIBehaviour("ZRNAPanel", "Button").Scale = Vector3.zero;
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/7");
            WaitTimeDoFunc(3, () =>
            {
                GetUIBehaviour("ZRNAPanel", "Button").Scale = Vector3.one;
                GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/12");
                ChangeButtonImage("保存", new Vector3(-73, 0, 0), new Vector2(38, 10));
                NextStep();
            });
        }

        private void RStep_11()
        {
            GetUIBehaviour("ZRNAPanel", "Button").Scale = Vector3.zero;
            GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/13");
            WaitTimeDoFunc(3, () =>
            {
                GetUIBehaviour("ZRNAPanel", "Content").ImageSprite = Resources.Load<Sprite>("Image/酶标仪界面/14");
                WaitTimeDoFunc(2, () => {
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel")
                    .ChangeState(2, StepProgressPanelLogic.StepState.Complete);
                    GetUILogic<StepProgressPanelLogic>("StepProgressPanel").Hide();
                    GetUILogic<StepTipLogic>("StepTip").Hide();
                    GetUILogic<ExperimentProgressPanelLogic>("ExperimentProgressPanel").Hide();
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").ChangeExperimentState("斑马鱼成鱼(脑组织酶活性)的测定", MainModulePanelLogic.ExperimentState.Complete);
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").GetUIBehaviours("转录组测序State").ImageSprite=GetUILogic<MainModulePanelLogic>("MainModulePanel").underwaySprite;
                    GetUILogic<MainModulePanelLogic>("MainModulePanel").Show();
                    NextStep();
                    DestroyImmediate(GetComponent<SubchronicToxicityStep>());
                });
            });
        }
        private void ChangeButtonImage(string path, Vector3 pos, Vector2 size)
        {
            GetUIBehaviour("ZRNAPanel", "Button").SetButtonActive(false);
            GetUIBehaviour("ZRNAPanel", "Button").GetComponent<RectTransform>().sizeDelta = size;
            GetUIBehaviour("ZRNAPanel", "Button").LocalPosition = pos;
            GetUIBehaviour("ZRNAPanel", "Button").ChangeImageSprite(Resources.Load<Sprite>("Image/酶标仪切图/" + path));
            Sprite sprite = Resources.Load<Sprite>("Image/酶标仪切图/" + path + "点击");
            GetUIBehaviour("ZRNAPanel", "Button").ChangeButtonClickSprite(sprite,
                sprite, sprite, null);
            GetUIBehaviour("ZRNAPanel", "Button").SetButtonActive(true);
            GetUIBehaviour("ZRNAPanel", "Button").RemoveButtonListener();
        }
        #endregion

    }


}

