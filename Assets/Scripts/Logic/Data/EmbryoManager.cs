﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Com.Rainier.ZC_Frame;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class EmbryoManager : MonoBehaviour
{
    private Dictionary<int, EmbryoData> _embryoDatas=new Dictionary<int, EmbryoData>();
    
    private Dictionary<int, EmbryoData> _growthDatas=new Dictionary<int, EmbryoData>();
    
    private Dictionary<int, EmbryoData> _hatchDatas=new Dictionary<int, EmbryoData>();

    private List<int> _livesIndex=new List<int>();
    private List<int> _normalIndex=new List<int>();
    private List<int> _hatchIndex=new List<int>();

    private Dictionary<string, float[]> _deathRate=new Dictionary<string, float[]>();
    private Dictionary<string, int[]> _malformationRate=new Dictionary<string, int[]>();
    private Dictionary<string, int[]> _hatchRate=new Dictionary<string, int[]>();

    public Dictionary<string, float[]> DeathRate
    {
        get => _deathRate;
    }

    private string _currentChoice;

    
    public string CurrentChoice
    {
        get => _currentChoice;
        set => _currentChoice = value;
    }

    private int _rate;

    public int Rate
    {
        get => _rate;
    }

    public void InitDie()
    {
        float[] data0 = {0, 0, 0, 0, 0};
        float[] data1 = {0, 0, 0, 0, 0};
        float[] data2 = {0, 0, 0.05f, 0.1f, 0.1f};
        float[] data3 = {0, 0, 0.1f, 0.15f, 0.15f};
        float[] data4 = {0, 0.2f, 0.4f, 0.45f, 0.45f};
        float[] data5 = {0.15f, 0.5f, 0.6f, 0.65f, 0.7f};
        float[] data6 = {0.65f, 0.8f, 0.9f, 0.9f, 0.95f};
        float[] data7 = {0.7f, 1, 1, 1, 1};
        _deathRate.Add("Control",data0);
        _deathRate.Add("200μg/L",data1);
        _deathRate.Add("300μg/L",data2);
        _deathRate.Add("400μg/L",data3);
        _deathRate.Add("500μg/L",data4);
        _deathRate.Add("600μg/L",data5);
        _deathRate.Add("700μg/L",data6);
        _deathRate.Add("800μg/L",data7);
        foreach (var item in GetComponentsInChildren<EmbryoData>())
        {
            _embryoDatas.Add(item.transform.GetSiblingIndex(),item);
            item.ChangeState(EmbryoType.胚胎,EmbryoData.EmbryoState.Live,24);
            _livesIndex.Add(item.transform.GetSiblingIndex());
        }

        _rate = 0;
    }

    public void InitGrowth()
    {
        int[] data0 = {0, 0, 0, 0, 0};
        int[] data1 = {0, 0, 3, 7, 9};
        int[] data2 = {0, 0, 7, 15, 16};
        _malformationRate.Add("Control",data0);
        _malformationRate.Add("52μg/L",data1);
        _malformationRate.Add("262μg/L",data2);
        foreach (var item in GetComponentsInChildren<EmbryoData>())
        {
            _growthDatas.Add(item.transform.GetSiblingIndex(),item);
            item.ChangeState(EmbryoType.畸形,EmbryoData.EmbryoState.Normal,24);
            _normalIndex.Add(item.transform.GetSiblingIndex());
        }
        _rate = 0;
    }

    public void InitHatch()
    {
        int[] data0 = {0, 20, 20};
        int[] data1 = {0, 17, 20};
        int[] data2 = {0, 8, 20};
        _hatchRate.Add("Control",data0);
        _hatchRate.Add("52μg/L",data1);
        _hatchRate.Add("262μg/L",data2);
        foreach (var item in GetComponentsInChildren<EmbryoData>())
        {
            _hatchDatas.Add(item.transform.GetSiblingIndex(),item);
            item.ChangeState(EmbryoType.孵化,EmbryoData.EmbryoState.UnHatched,48);
            _hatchIndex.Add(item.transform.GetSiblingIndex());
        }
        _rate = 0;
    }

    /// <summary>
    /// 死亡
    /// </summary>
    /// <param name="type">浓度类型</param>
    /// <param name="index">第几次</param>
    /// <param name="time">时间</param>
    /// <returns></returns>
    public int Config(string type,int index,int time,EmbryoType embryoType)
    {
        for (int i = 0; i < _embryoDatas.Count; i++)
        {
            var temp = _embryoDatas[i];
            if (temp.embryoState == EmbryoData.EmbryoState.Live)
            {
                for (int j = 0; j < temp.transform.childCount; j++)
                {
                    if (temp.transform.GetChild(j).name.Equals(embryoType + time.ToString() + "Live"))
                        temp.transform.GetChild(j).gameObject.SetActive(true);
                    else
                        temp.transform.GetChild(j).gameObject.SetActive(false);
                }
            }
        }
        float rate = _deathRate[type][index] * 20;
        for (int i = 0; i < rate-_rate; i++)
        {
            int item = GetNum(_livesIndex.Count);
            var currentIndex = _livesIndex[item];
            _livesIndex.RemoveAt(item);
            _embryoDatas[currentIndex].ChangeState(embryoType,EmbryoData.EmbryoState.Die,time);
        }
        _rate =(int) rate;
        return _rate;
    }

    
    public int ConfigGrowth(string type,int index,int time,EmbryoType embryoType)
    {
        for (int i = 0; i < _growthDatas.Count; i++)
        {
            var temp = _growthDatas[i];
            if (temp.embryoState == EmbryoData.EmbryoState.Normal)
            {
                for (int j = 0; j < temp.transform.childCount; j++)
                {
                    if (temp.transform.GetChild(j).name.Equals(embryoType + time.ToString() + "Normal"))
                        temp.transform.GetChild(j).gameObject.SetActive(true);
                    else
                        temp.transform.GetChild(j).gameObject.SetActive(false);
                }
            }
        }
        int rate = _malformationRate[type][index];
        for (int i = 0; i < rate-_rate; i++)
        {
            int item = GetNum(_normalIndex.Count);
            Debug.Log("索引"+item);
            Debug.Log("列表长度"+_normalIndex.Count);
            var currentIndex = _normalIndex[item];
            _normalIndex.RemoveAt(item);
            _growthDatas[currentIndex].ChangeState(embryoType,EmbryoData.EmbryoState.Malformation,time);
        }
        _rate = rate;
        return _rate;
    }

    public int ConfigHatch(string type,int index,int time,EmbryoType embryoType)
    {
        for (int i = 0; i < _hatchDatas.Count; i++)
        {
            var temp = _hatchDatas[i];
            if (temp.embryoState == EmbryoData.EmbryoState.UnHatched)
            {
                for (int j = 0; j < temp.transform.childCount; j++)
                {
                    if (temp.transform.GetChild(j).name.Equals(embryoType + time.ToString() + "UnHatched"))
                        temp.transform.GetChild(j).gameObject.SetActive(true);
                    else
                        temp.transform.GetChild(j).gameObject.SetActive(false);
                }
            }
        }
        int rate = _hatchRate[type][index];
        for (int i = 0; i < rate-_rate; i++)
        {
            int item = GetNum(_hatchIndex.Count);
            Debug.Log("索引"+item);
            Debug.Log("列表长度"+_hatchIndex.Count);
            var currentIndex = _hatchIndex[item];
            _hatchIndex.RemoveAt(item);
            _hatchDatas[currentIndex].ChangeState(embryoType,EmbryoData.EmbryoState.Hatch,time);
        }
        _rate = rate;
        return _rate;
    }
    
    private int GetNum(int count)
    {
        return Random.Range(0,count);
    }
    public enum EmbryoType
    {
        胚胎
        ,畸形
        ,孵化
        ,心跳
    }
}
