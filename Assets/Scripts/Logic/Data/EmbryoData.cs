﻿using System;
using System.Collections;
using System.Collections.Generic;
using Com.Rainier.ZC_Frame;
using UnityEngine;

public class EmbryoData : MonoBehaviour
{
    public string ActiveObj
    {
        get
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.activeSelf)
                    return transform.GetChild(i).gameObject.name;
            }
            return String.Empty;
        }
    }

    public EmbryoState embryoState = EmbryoState.Default;
    
    public void ChangeState(EmbryoManager.EmbryoType embryoType,EmbryoState state,int time)
    {
        embryoState = state;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Equals(embryoType+time.ToString() + embryoState))
                transform.GetChild(i).gameObject.SetActive(true);
            else
                transform.GetChild(i).gameObject.SetActive(false);
        }
    }
    public enum EmbryoState
    {
        Default
        ,Live
        ,Die
        ,Malformation
        ,Normal
        ,Hatch
        ,UnHatched
    }
    
}
