﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： CommandHelper
* 创建日期：2021-12-29 09:07:33
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：Command程序帮助工具
******************************************************************************/

using Debug = UnityEngine.Debug;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using UnityEditor;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class CommandHelper  
	{
        /// <summary>
        /// CMD程序
        /// </summary>
        private static string CmdApp
        {
            get { string cmdName = "cmd.exe"; return cmdName; }
        }

        private static List<Action> actions = new List<Action>();

        static CommandHelper()
        {
            if (actions == null)
            {
                actions = new List<Action>();
            }
            else
            {
                actions.Clear();
            }
            EditorApplication.update += OnUpdate;
        }

        private static void OnUpdate()
        {
            lock (new object())
            {
                try
                {
                    if (actions == null || actions.Count <= 0)
                    {
                        return;
                    }
                    actions.ForEach(action =>
                    {
                        if (action != null)
                        {
                            action.Invoke();
                        }
                    });
                    actions.Clear();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    if (actions != null) {
                        actions.Clear();
                    }
                    
                }
            }
        }

        /// <summary>
        /// 执行CMD
        /// </summary>
        /// <param name="cmdStr">CMD命令</param>
        /// <returns></returns>
        public static CommandRequest ProcessCommand(string cmdStr)
        {
            CommandRequest request = new CommandRequest();
            ThreadPool.QueueUserWorkItem(delegate (object state)
            {
                Process process = null;
                try
                {
                    ProcessStartInfo start = new ProcessStartInfo(CmdApp);
                    start.Arguments = "/c ";
                    start.Arguments += ("\"" + cmdStr + "\"");
                    start.CreateNoWindow = false;
                    start.ErrorDialog = true;
                    start.UseShellExecute = false;
                    if (start.UseShellExecute)
                    {
                        start.RedirectStandardOutput = false;
                        start.RedirectStandardError = false;
                        start.RedirectStandardInput = false;
                    }
                    else
                    {
                        start.RedirectStandardOutput = true;
                        start.RedirectStandardError = true;
                        start.RedirectStandardInput = true;
                        start.StandardOutputEncoding = UTF8Encoding.UTF8;
                        start.StandardErrorEncoding = UTF8Encoding.UTF8;
                    }
                    process = Process.Start(start);
                    process.ErrorDataReceived += delegate (object sender, DataReceivedEventArgs e) { Debug.LogError(e.Data); };
                    process.OutputDataReceived += delegate (object sender, DataReceivedEventArgs e) { Debug.LogError(e.Data); };
                    process.Exited += delegate (object sender, EventArgs e) { Debug.LogError(e.ToString()); };
                    bool hasError = false;
                    do
                    {
                        string line = process.StandardOutput.ReadLine();
                        if (line == null)
                        {
                            break;
                        }
                        line = line.Replace("\\", "/");
                        actions.Add(delegate () { request.Log(0, line); });
                    } while (true);
                    while (true)
                    {
                        string error = process.StandardError.ReadLine();
                        if (string.IsNullOrEmpty(error))
                        {
                            break;
                        }
                        hasError = true;
                        actions.Add(delegate () { request.Log(1, error); });
                    }
                    process.Close();
                    if (hasError)
                    {
                        actions.Add(delegate () { request.Error(); });
                    }
                    else
                    {
                        actions.Add(delegate () { request.NotifyDone(); });
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    if (process != null)
                    {
                        process.Close();
                    }
                }
            });
            return request;
        }

    }

	public class CommandRequest 
	{
        public event Action<int, string> onLog;
        public event Action onError;
        public event Action onDone;

        public void Log(int type, string log)
        {
            if (onLog != null)
            {
                onLog.Invoke(type, log);
            }
        }

        public void NotifyDone()
        {
            if (onDone != null)
            {
                onDone.Invoke();
            }
        }

        public void Error()
        {
            if (onError != null)
            {
                onError.Invoke();
            }
        }
    }
}

