﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： CreateAtlas
* 创建日期：2021-12-29 09:27:09
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：创建图集工具
******************************************************************************/

using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TMPro;
using UnityEngine.TextCore;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class CreateAtlas : EditorWindow
	{
        /// <summary>
        /// 图集路径
        /// </summary>
		private readonly string AtlasPngPath = "Assets/Artworks/Image/TexturePacker/Atlas/IconAtlas.png";

        private static CreateAtlas window = null;

        /// <summary>
        /// 图标路径
        /// </summary>
        private string iconFolderPath ;

        /// <summary>
        /// 图集路径
        /// </summary>
        private string iconAtlasPngPath ;

        /// <summary>
        /// 图集json路径
        /// </summary>
        private string iconAtlasJsonFilePath ;

        /// <summary>
        /// 图集sheet路径
        /// </summary>
        private string iconAtlasTpsheetFilePath ;

        private System.DateTime curTime;
        private System.DateTime finishTime;

        private void OnEnable()
        {
            iconFolderPath = Application.dataPath + "/Artworks/Image/TexturePacker/Icon";
            iconAtlasPngPath = Application.dataPath + "/Artworks/Image/TexturePacker/Atlas/IconAtlas.png";
            iconAtlasJsonFilePath = Application.dataPath + "/Artworks/Image/TexturePacker/Atlas/IconAtlas.json";
            iconAtlasTpsheetFilePath = Application.dataPath + "/Artworks/Image/TexturePacker/Atlas/IconAtlas.tpsheet";
        }

        [MenuItem("Tools/图集生成工具")]
        public static void ShowCreateAtlasWindow()
        {
            if (window == null)
                window = EditorWindow.GetWindow(typeof(CreateAtlas)) as CreateAtlas;
            window.titleContent = new GUIContent("TexturePacker");
            window.minSize = new Vector2(200, 100);
            window.maxSize = new Vector2(500, 500);

            window.Show();
        }

        private void OnGUI()
        {
            DrawToolBar();
        }

        private void DrawToolBar()
        {
            if (GUILayout.Button("1.生成Json"))
            {
                JsonTexturePacker();
            }
            if (GUILayout.Button("2.生成Atlas"))
            {
                AtlasTexturePacker();
            }
        }

        /// <summary>
        /// 打Json格式图集
        /// </summary>
        private void JsonTexturePacker()
        {
            curTime = System.DateTime.Now;
            string cmd = TexturePackerCommand.GetJsonPackCommand(iconFolderPath, iconAtlasPngPath, iconAtlasJsonFilePath);
            CommandRequest request = CommandHelper.ProcessCommand(cmd);
            EditorUtility.DisplayProgressBar("批量处理中...", "生成Json图集信息...", 0);
            request.onDone += () =>
            {
                {
                    EditorUtility.DisplayProgressBar("批量处理中...", "开始打图集...", 0);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    CreateUIAtlas();
                }
            };
            request.onError += () =>
            {
                Debug.LogError("Texture Packer 打包失败!!! ");
                EditorUtility.ClearProgressBar();
            };
        }

        /// <summary>
        /// 打Unity格式图集
        /// </summary>
        private void AtlasTexturePacker()
        {
            curTime = System.DateTime.Now;
            string cmd = TexturePackerCommand.GetAtlasPackCommand(iconFolderPath, iconAtlasPngPath, iconAtlasTpsheetFilePath);
            CommandRequest request = CommandHelper.ProcessCommand(cmd);
            EditorUtility.DisplayProgressBar("批量处理中...", "生成Atlas图集信息...", 0);
            request.onDone += () =>
            {
                {
                    EditorUtility.DisplayProgressBar("批量处理中...", "开始打图集...", 0);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    CreateUIAtlas();
                }
            };
            request.onError += () =>
            {
                Debug.LogError("Texture Packer 打包失败!!! ");
                EditorUtility.ClearProgressBar();
            };
        }

        private void CreateUIAtlas()
        {
            TextureSetting(AtlasPngPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            finishTime = System.DateTime.Now;
            Debug.Log("本次打图集总耗时：" + (finishTime - curTime));
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// 设置图片格式，根据项目情况自行修改
        /// </summary>
        /// <param name="path"></param>
        public void TextureSetting(string path)
        {
            if (string.IsNullOrEmpty(path) || !IsTextureFile(path))
            {
                return;
            }
            TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            if (textureImporter == null)
            {
                return;
            }
            textureImporter.textureType = TextureImporterType.Sprite;
            textureImporter.spriteImportMode = SpriteImportMode.Multiple;
            textureImporter.wrapMode = TextureWrapMode.Clamp;
            textureImporter.filterMode = FilterMode.Bilinear;
            textureImporter.isReadable = false;
            textureImporter.maxTextureSize = 2048;
#if UNITY_IOS
            TextureImporterPlatformSettings iPhoneSetting = new TextureImporterPlatformSettings();
            iPhoneSetting.overridden = true;
            iPhoneSetting.format = TextureImporterFormat.ASTC_RGBA_4x4;
            iPhoneSetting.maxTextureSize = 2048;
            iPhoneSetting.name = "iPhone";
            iPhoneSetting.compressionQuality = (int)UnityEngine.TextureCompressionQuality.Normal;
            textureImporter.SetPlatformTextureSettings(iPhoneSetting);
#elif UNITY_ANDROID
            TextureImporterPlatformSettings androidSetting = new TextureImporterPlatformSettings();
            androidSetting.overridden = true;
            androidSetting.maxTextureSize = 2048;
            androidSetting.name = "Android";
            androidSetting.resizeAlgorithm = TextureResizeAlgorithm.Mitchell;
            androidSetting.format = TextureImporterFormat.ETC2_RGBA8;
            androidSetting.compressionQuality = (int)TextureCompressionQuality.Normal;
            androidSetting.androidETC2FallbackOverride = AndroidETC2FallbackOverride.UseBuildSettings;
            textureImporter.SetPlatformTextureSettings(androidSetting);
#elif UNITY_WEBGL ||UNITY_STANDALONE
            TextureImporterPlatformSettings webGLSetting = new TextureImporterPlatformSettings();
            webGLSetting.overridden = true;
            webGLSetting.maxTextureSize = 2048;
            webGLSetting.name = "WebGL";
            webGLSetting.format = TextureImporterFormat.DXT5Crunched;
            webGLSetting.compressionQuality = (int)TextureCompressionQuality.Normal;
            textureImporter.SetPlatformTextureSettings(webGLSetting);
#endif
            AssetDatabase.ImportAsset(path);
            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// 判断是否是图片格式
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool IsTextureFile(string path)
        {
            string texturePath = path.ToLower();
            return texturePath.EndsWith(".psd") || texturePath.EndsWith(".tga") || texturePath.EndsWith(".png") || texturePath.EndsWith(".jpg") || texturePath.EndsWith(".dds") || texturePath.EndsWith(".bmp") || texturePath.EndsWith(".tif") || texturePath.EndsWith(".gif");
        }


    }
}

