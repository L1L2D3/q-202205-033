﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： TexturePackerCommand
* 创建日期：2021-12-29 08:45:03
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：TexturePacker cmd命令
******************************************************************************/

using UnityEngine;
using System.Text.RegularExpressions;
using System.Collections;

namespace Com.Rainier.ZC_Frame
{
	/// <summary>
	/// TexturePacker cmd命令
	/// </summary>
	public class TexturePackerCommand  
	{
		/// <summary>
		/// TexturePacker安装路径（路径有空格时要用""包起来）
		/// </summary>
		private static string texturePacker_Path = @"""C:\Program Files\CodeAndWeb\TexturePacker\bin\TexturePacker""";


		/// <summary>
		/// TP打包Json命令
		/// </summary>
		private static string jsonPackCMDStr = "{TexturePacker_PATH} {ART_IMG_PATH} --max-size 4096 --allow-free-size --no-trim --format json-array --size-constraints POT --shape-padding 2 --border-padding 2 --common-divisor-x 1 --common-divisor-y 1 --disable-rotation --algorithm MaxRects --opt RGBA8888 --scale 1 --scale-mode Smooth --smart-update --sheet {ATLAS_PNG} --data {ATLAS_DATA}";

	    /// <summary>
		/// TP打包图集命令
		/// </summary>
		private static string atlasPackCMDStr = "{TexturePacker_PATH} {ART_IMG_PATH} --max-size 4096 --allow-free-size --no-trim --format unity-texture2d --extrude 0 --size-constraints POT --shape-padding 2 --border-padding 2 --common-divisor-x 1 --common-divisor-y 1 --disable-rotation --algorithm MaxRects --opt RGBA8888 --scale 1 --scale-mode Smooth --smart-update --sheet {ATLAS_PNG} --data {ATLAS_DATA}";

	    /// <summary>
		/// 生成打包图集命令
		/// </summary>
		/// <param name="photoFolderPath">图标文件夹</param>
		/// <param name="atlasPngOutputPath">图集文件夹</param>
		/// <param name="atlasTxtOutputPath">json文件夹</param>
		/// <returns></returns>
		public static string GetAtlasPackCommand(string photoFolderPath, string atlasPngOutputPath, string atlasTxtOutputPath)
		{
			if (string.IsNullOrEmpty(photoFolderPath) || string.IsNullOrEmpty(atlasPngOutputPath) || string.IsNullOrEmpty(atlasTxtOutputPath)) return string.Empty;
			string command = atlasPackCMDStr;
			command = Regex.Replace(command, "{TexturePacker_PATH}", texturePacker_Path);//TexturePacker安装路径
			command = Regex.Replace(command, "{ART_IMG_PATH}", photoFolderPath);//图标文件夹
			command = Regex.Replace(command, "{ATLAS_PNG}", atlasPngOutputPath);//生成图集文件夹
			command = Regex.Replace(command, "{ATLAS_DATA}", atlasTxtOutputPath);//json文件输出路径
			return command;
		}

		/// <summary>
		/// 生成打包json命令
		/// </summary>
		/// <param name="photoFolderPath">图标文件夹</param>
		/// <param name="atlasPngOutputPath">图集文件夹</param>
		/// <param name="atlasTxtOutputPath">json文件夹</param>
		/// <returns></returns>
		public static string GetJsonPackCommand(string photoFolderPath, string atlasPngOutputPath, string atlasTxtOutputPath)
		{
			if (string.IsNullOrEmpty(photoFolderPath) || string.IsNullOrEmpty(atlasPngOutputPath) || string.IsNullOrEmpty(atlasTxtOutputPath)) return string.Empty;
			string commond = jsonPackCMDStr;
			commond = Regex.Replace(commond, "{TexturePacker_PATH}", texturePacker_Path);
			commond = Regex.Replace(commond, "{ART_IMG_PATH}", photoFolderPath);
			commond = Regex.Replace(commond, "{ATLAS_PNG}", atlasPngOutputPath);
			commond = Regex.Replace(commond, "{ATLAS_DATA}", atlasTxtOutputPath);
			return commond;
		}
	}
}

