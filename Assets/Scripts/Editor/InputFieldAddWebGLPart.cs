﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： InputFieldAddWebGLPart
* 创建日期：2021-12-07 09:48:28
* 作者名称：张辰
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：批量添加InputField_WebGL组件
******************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using Com.Rainier.Buskit3D.WebGLInputField;

namespace Com.Rainier.ZC_Frame
{
    /// <summary>
    /// 
    /// </summary>
	public class InputFieldAddWebGLPart : EditorWindow
    {
        [MenuItem("Tools/批量添加InputField_WebGL")]
        public static void AddWebGLPart()
        {
            var allGos = Resources.FindObjectsOfTypeAll(typeof(GameObject));
            var previousSelection = Selection.objects;
            Selection.objects = allGos;
            var selectedTransforms = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);
            Selection.objects = previousSelection;
            int count = 0;
            foreach (var trans in selectedTransforms)
            {
                if (trans.GetComponent<InputField>())
                {
                    InputField tmpInput = trans.GetComponent<InputField>();
                    Undo.RecordObject(tmpInput, tmpInput.gameObject.name);
                    if (!tmpInput.GetComponent<InputField_WebGL>())
                    {
                        tmpInput.gameObject.AddComponent<InputField_WebGL>();
                        EditorUtility.SetDirty(tmpInput);
                        count++;
                    }
                    else
                    {
                        Debug.Log(tmpInput.name+ " 已存在 InputField_WebGL 组件");
                    }
                }
            }
            Debug.Log("InputField_WebGL 组件添加完成，共添加 " + count + " 个");
        }
    }
}

