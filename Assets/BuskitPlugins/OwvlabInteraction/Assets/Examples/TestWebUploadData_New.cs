﻿/*******************************************************************************
* 版权声明：北京润尼尔网络科技有限公司，保留所有版权
* 版本声明：v1.0.0
* 类 名 称： TestWebUploadData_New
* 创建日期：2020-03-24 13:11:52
* 作者名称：王庚
* CLR 版本：4.0.30319.42000
* 修改记录：
* 描述：
******************************************************************************/

using UnityEngine;
using System.Collections;
using Com.Rainier.Buskit.Unity.Architecture.Injector;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using UnityEngine.Video;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace Com.Rainier.Buskit3D.Owvlab
{
	/// <summary>
	/// 测试新平台web端数据交互
	/// </summary>
	public class TestWebUploadData_New : MonoBehaviour
	{

		public InputField scoreText;
		public Button scoreBtn;
		public Button reportBtn1;

		public Texture2D texture;
		public AudioClip audioClip;

		public TextAsset textAsset;
		/// <summary>
		/// Unity Method
		/// </summary>
		void Start()
		{

			//Webgl不需要用户名密码登录，需要将实验资源上传至平台才可测试

			scoreBtn.onClick.AddListener(UpScoreData);

			reportBtn1.onClick.AddListener(UpReprotData1);

			//直接测试报告上传图片和文字，不需要登录,从网页上拿去esid和eid
			//StartCoroutine(UploadPNG());
		}

		/// <summary>
		/// Unity Method
		/// </summary>
		void Update()
		{

		}


		public void UpScoreData()
		{
			OwvlabPlatformNew owvlabWeb = InjectService.Get<OwvlabPlatformNew>();

			int s = int.Parse(scoreText.text);

			owvlabWeb.SendScoreToWeb(s);

		}

		/// <summary>
		/// 登录之后 ,上传实验报告
		/// </summary>
		public void UpReprotData1()
		{
			OwvlabPlatformNew owvlabWeb = InjectService.Get<OwvlabPlatformNew>();

			byte[] bytes = texture.EncodeToPNG();

			WWWForm form = new WWWForm();

			string str = textAsset.text;
			form.AddField("reportData", str);
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");

			owvlabWeb.SendReportToWeb(form);
		}

		/// <summary>
		/// 直接测试报告上传
		/// </summary>
		/// <returns></returns>
		IEnumerator UploadPNG()
		{
			
			byte[] bytes = texture.EncodeToPNG();
			// Create a Web Form
			WWWForm form = new WWWForm();

			string str = textAsset.text;
			form.AddField("reportData", str);
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");
			form.AddBinaryData("picFiles", bytes, "screenShot.png", "image/png");
			// Upload to a cgi script
			using (var w = UnityWebRequest.Post("http://cs.jinhao.owvlab.net/openapi/report/de045b45-f1d8-48bf-a7a1-3631befbbc2e/230004", form))
			{
				yield return w.SendWebRequest();
				if (w.isNetworkError || w.isHttpError)
				{
					print(w.error);
				}
				else
				{
					print("Finished Uploading Screenshot");
				}
				Debug.Log(w.downloadHandler.text);
			}
		}

		public byte[] AudioClipToByte(AudioClip clip)
		{
			float[] data = new float[clip.samples];
			clip.GetData(data, 0);
			int rescaleFactor = 32767; //to convert float to Int16
			byte[] outData = new byte[data.Length * 2];
			for (int i = 0; i < data.Length; i++)
			{
				short temshort = (short)(data[i] * rescaleFactor);
				byte[] temdata = BitConverter.GetBytes(temshort);
				outData[i * 2] = temdata[0];
				outData[i * 2 + 1] = temdata[1];
			}
			return outData;
		}

	}
}

